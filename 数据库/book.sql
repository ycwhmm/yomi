/*
 Navicat Premium Data Transfer

 Source Server         : jeecgboot
 Source Server Type    : MySQL
 Source Server Version : 50737
 Source Host           : localhost:3306
 Source Schema         : book

 Target Server Type    : MySQL
 Target Server Version : 50737
 File Encoding         : 65001

 Date: 28/04/2022 20:32:13
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for bcategory
-- ----------------------------
DROP TABLE IF EXISTS `bcategory`;
CREATE TABLE `bcategory`  (
  `Bcategoryid` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '商品包装供应商类别id',
  `Bcategoryname` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品包装供应商类别名字',
  PRIMARY KEY (`Bcategoryid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品包装供应商类别' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bcategory
-- ----------------------------
INSERT INTO `bcategory` VALUES (1, '礼盒装');
INSERT INTO `bcategory` VALUES (2, '纸盒装');

-- ----------------------------
-- Table structure for bping
-- ----------------------------
DROP TABLE IF EXISTS `bping`;
CREATE TABLE `bping`  (
  `bpingid` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `statusid` int(11) NOT NULL COMMENT '审核状态信息',
  `ordersId` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bpringtime` date NULL DEFAULT NULL,
  `needId` int(11) NULL DEFAULT NULL,
  `logId` int(11) NULL DEFAULT NULL,
  `entrepotid` int(11) NULL DEFAULT NULL,
  `outstorageid` int(11) NULL DEFAULT NULL,
  `commodityid` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`bpingid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 155 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '批量领料申请' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bping
-- ----------------------------
INSERT INTO `bping` VALUES (113, 2, 'XQ202012283', '2020-12-16', 35, 3, 1, 29, 31);
INSERT INTO `bping` VALUES (120, 2, 'XQ202012241', '2020-12-16', 35, 5, 1, 29, 31);
INSERT INTO `bping` VALUES (134, 1, 'XQ20224879', '2022-04-27', 38, 3, 1, 35, 32);
INSERT INTO `bping` VALUES (140, 1, 'XQ202012909', '2022-04-28', 35, 3, 1, 38, 19);
INSERT INTO `bping` VALUES (141, 1, 'XQ202012241', '2020-12-01', 35, 3, 1, 1, 31);
INSERT INTO `bping` VALUES (142, 1, 'XQ202012241', '2020-12-01', 35, 3, 1, 1, 31);
INSERT INTO `bping` VALUES (143, 1, 'XQ202012241', '2020-12-01', 35, 3, 1, 1, 31);
INSERT INTO `bping` VALUES (144, 1, 'XQ202012241', NULL, 35, 3, 1, 1, 31);
INSERT INTO `bping` VALUES (145, 1, 'XQ202012241', NULL, 35, 3, 1, 1, 31);
INSERT INTO `bping` VALUES (146, 1, 'XQ20224203', NULL, 38, 3, 1, 40, 32);
INSERT INTO `bping` VALUES (147, 1, 'XQ20224203', NULL, 38, 3, 1, 40, 32);
INSERT INTO `bping` VALUES (148, 1, 'XQ202244444', NULL, 38, 5, 1, 41, 32);
INSERT INTO `bping` VALUES (149, 1, 'XQ202244444', NULL, 38, 5, 1, 41, 32);
INSERT INTO `bping` VALUES (150, 1, 'XQ202012241', NULL, 35, 3, 1, 1, 31);
INSERT INTO `bping` VALUES (151, 1, 'XQ202012241', NULL, 35, 3, 1, 1, 31);
INSERT INTO `bping` VALUES (152, 1, 'XQ202012241', NULL, 35, 3, 1, 1, 31);
INSERT INTO `bping` VALUES (153, 1, 'XQ202012241', NULL, 35, 3, 1, 1, 31);
INSERT INTO `bping` VALUES (154, 1, 'XQ202012241', NULL, 35, 3, 1, 1, 31);

-- ----------------------------
-- Table structure for bsupplier
-- ----------------------------
DROP TABLE IF EXISTS `bsupplier`;
CREATE TABLE `bsupplier`  (
  `BsupId` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `BsupName` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '供应商名',
  `Bcategoryid` int(11) NOT NULL COMMENT '供应商类别id',
  `Bsprice` int(11) NOT NULL COMMENT '商品包装单价',
  `Bsuloc` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品包装供应商地址',
  `statusid` int(11) NULL DEFAULT NULL COMMENT '审核状态',
  PRIMARY KEY (`BsupId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品包装供应商' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bsupplier
-- ----------------------------
INSERT INTO `bsupplier` VALUES (1, '诚信包装有限公司', 1, 20, '河南省新乡市红旗区', 1);
INSERT INTO `bsupplier` VALUES (2, '迅捷包装有限公司', 2, 10, '河南省开封市龙亭区', 2);
INSERT INTO `bsupplier` VALUES (3, '三A包装有限公司', 1, 15, '河南省郑州市金水区', 2);

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category`  (
  `categoryid` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '商品类别id',
  `categoryname` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品类别名字',
  PRIMARY KEY (`categoryid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品类别' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES (1, '渔');
INSERT INTO `category` VALUES (2, '渔');
INSERT INTO `category` VALUES (3, '渔');

-- ----------------------------
-- Table structure for commodity
-- ----------------------------
DROP TABLE IF EXISTS `commodity`;
CREATE TABLE `commodity`  (
  `commodityid` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `commodityname` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品名',
  `commodityimgstr` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品图片',
  `supId` int(11) NOT NULL COMMENT '商品供应商id(查询出商品类别信息)',
  `coprice` decimal(10, 1) NOT NULL COMMENT '商品单价',
  `statusid` int(11) NULL DEFAULT NULL COMMENT '审核状态',
  PRIMARY KEY (`commodityid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '供应商商品详情表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of commodity
-- ----------------------------
INSERT INTO `commodity` VALUES (19, '小黄鱼', '/files/3ee9dab7-2b6c-4c14-b3de-e27ebb41df3a.gif', 2, 12.0, 1);
INSERT INTO `commodity` VALUES (31, '小黄鱼', '/files/e971e12b-f045-4214-bbc6-2af08320e5ff.gif', 3, 12.0, 1);
INSERT INTO `commodity` VALUES (32, '的是法國的', '/files/e80dc60d-88f6-4032-9a88-0ca22f49bc8f.gif', 2, 42.0, 2);

-- ----------------------------
-- Table structure for dept
-- ----------------------------
DROP TABLE IF EXISTS `dept`;
CREATE TABLE `dept`  (
  `dept_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '部门编号',
  `dept_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门名称',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 33 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dept
-- ----------------------------
INSERT INTO `dept` VALUES (1, '样板负责人');
INSERT INTO `dept` VALUES (2, '包装订单负责人');
INSERT INTO `dept` VALUES (3, '采购订单负责人');
INSERT INTO `dept` VALUES (4, '分拣负责人');
INSERT INTO `dept` VALUES (5, '入库管理员');
INSERT INTO `dept` VALUES (6, '出库管理员');
INSERT INTO `dept` VALUES (7, '批量订单管理员');
INSERT INTO `dept` VALUES (8, '生产订单管理员');
INSERT INTO `dept` VALUES (9, '物流管理员');
INSERT INTO `dept` VALUES (10, '财务管理员');
INSERT INTO `dept` VALUES (11, '分销商业务员');
INSERT INTO `dept` VALUES (21, '123');
INSERT INTO `dept` VALUES (22, '123');
INSERT INTO `dept` VALUES (23, '123');
INSERT INTO `dept` VALUES (24, '123');
INSERT INTO `dept` VALUES (32, '出库负责人');
INSERT INTO `dept` VALUES (30, '123');

-- ----------------------------
-- Table structure for dis_need
-- ----------------------------
DROP TABLE IF EXISTS `dis_need`;
CREATE TABLE `dis_need`  (
  `dis_need_id` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键ID',
  `distributorid` int(11) NOT NULL COMMENT '分销商ID',
  `supId` int(11) NOT NULL COMMENT '商品供应商ID',
  `commodityid` int(11) NOT NULL COMMENT '商品详情ID',
  `Bcategoryid` int(11) UNSIGNED NOT NULL COMMENT '商品包装供应商类别id',
  `BsupId` int(11) UNSIGNED NOT NULL COMMENT '商品包装供应商ID',
  `logId` int(11) UNSIGNED NOT NULL COMMENT '物流公司id',
  `logmodeid` int(11) UNSIGNED NOT NULL COMMENT '物流方式id',
  `statusid` int(11) NOT NULL COMMENT '审核状态信息',
  `advance` decimal(11, 0) NOT NULL COMMENT '定金',
  `Samnumber` int(11) NOT NULL COMMENT '商品数量',
  `satypenumber` int(20) NOT NULL COMMENT '采购商品包装数量',
  `needId` int(11) NOT NULL COMMENT '样板包装',
  `birth` date NOT NULL COMMENT '日期',
  `logparticularsid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`dis_need_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '分销商需求' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dis_need
-- ----------------------------
INSERT INTO `dis_need` VALUES ('XQ202012241', 3, 2, 19, 2, 2, 1, 2, 2, 1000, 23, 23, 35, '2020-12-11', 'WL202012241');
INSERT INTO `dis_need` VALUES ('XQ202012625', 2, 2, 31, 1, 2, 5, 1, 2, 1000, 23, 23, 35, '2020-12-11', 'WL202012625');
INSERT INTO `dis_need` VALUES ('XQ202012666', 2, 2, 31, 1, 2, 3, 2, 2, 21, 12, 21, 35, '2020-12-16', 'WL202012666');
INSERT INTO `dis_need` VALUES ('XQ202012722', 2, 2, 31, 1, 2, 5, 2, 2, 1000, 23, 23, 35, '2020-12-18', 'WL202012722');
INSERT INTO `dis_need` VALUES ('XQ202012740', 2, 2, 31, 1, 2, 3, 2, 2, 1000, 23, 23, 35, '2020-12-12', 'WL202012740');
INSERT INTO `dis_need` VALUES ('XQ202012885', 2, 2, 31, 2, 2, 3, 2, 2, 1000, 23, 23, 35, '2020-12-24', 'WL202012885');
INSERT INTO `dis_need` VALUES ('XQ202012896', 3, 2, 31, 1, 2, 3, 1, 2, 1000, 23, 23, 35, '2020-12-18', 'WL202012896');
INSERT INTO `dis_need` VALUES ('XQ202012897', 3, 2, 31, 1, 2, 3, 3, 2, 1000, 23, 23, 35, '2020-12-12', 'WL202012897');
INSERT INTO `dis_need` VALUES ('XQ202012909', 2, 2, 19, 1, 2, 3, 1, 2, 4, 34, 34, 35, '2020-12-17', 'WL202012909');
INSERT INTO `dis_need` VALUES ('XQ202012919', 2, 2, 31, 1, 2, 3, 3, 2, 12, 12, 12, 35, '2020-12-16', 'WL202012919');
INSERT INTO `dis_need` VALUES ('XQ202012939', 3, 2, 31, 2, 2, 3, 1, 2, 1000, 23, 23, 35, '2020-12-24', 'WL202012939');
INSERT INTO `dis_need` VALUES ('XQ202012957', 2, 2, 31, 1, 2, 3, 2, 2, 1000, 23, 23, 35, '2020-12-18', 'WL202012957');
INSERT INTO `dis_need` VALUES ('XQ20224121', 2, 2, 32, 1, 2, 5, 1, 2, 1, 1, 1, 38, '2022-04-28', 'WL20224121');
INSERT INTO `dis_need` VALUES ('XQ20224203', 2, 2, 32, 1, 2, 3, 1, 2, 1111, 1111, 1111, 38, '2022-04-28', 'WL20224203');
INSERT INTO `dis_need` VALUES ('XQ202244444', 2, 2, 32, 1, 3, 5, 1, 2, 55, 555, 5, 38, '2022-04-28', 'WL20224533');
INSERT INTO `dis_need` VALUES ('XQ20224679', 2, 2, 32, 1, 3, 3, 1, 2, 1, 1, 1, 38, '2022-04-28', 'WL20224679');
INSERT INTO `dis_need` VALUES ('XQ20224879', 2, 2, 32, 1, 2, 3, 3, 2, 45, 435, 45, 38, '2022-04-28', 'WL20224879');
INSERT INTO `dis_need` VALUES ('XQ20224887', 2, 2, 32, 1, 2, 3, 1, 2, 1, 1, 1, 38, '2022-04-28', 'WL20224887');
INSERT INTO `dis_need` VALUES ('XQ20224951', 3, 2, 32, 1, 3, 3, 1, 2, 1, 1, 1, 38, '2022-04-20', 'WL20224951');

-- ----------------------------
-- Table structure for distributor
-- ----------------------------
DROP TABLE IF EXISTS `distributor`;
CREATE TABLE `distributor`  (
  `distributorid` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `distributorname` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分销商名字',
  `distrlegal` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分销商法人',
  `distrcapital` int(11) NOT NULL COMMENT '分销商资金',
  `statusid` int(11) NOT NULL COMMENT '审核状态信息',
  `distloc` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分销商地址',
  PRIMARY KEY (`distributorid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '分销商信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of distributor
-- ----------------------------
INSERT INTO `distributor` VALUES (1, '海大鱼', '张三', 4000, 1, '河南省新乡市红旗区');
INSERT INTO `distributor` VALUES (2, '蟹皇堡', '李四', 5000, 2, '河南省开封市龙亭区');
INSERT INTO `distributor` VALUES (3, '海鲜店', '王五', 5000, 2, '河南省郑州市金水区');

-- ----------------------------
-- Table structure for emp
-- ----------------------------
DROP TABLE IF EXISTS `emp`;
CREATE TABLE `emp`  (
  `emp_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '员工编号',
  `emp_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '员工名称',
  `emp_sex` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '性别',
  `emp_phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '电话',
  `dept_id` int(11) NOT NULL COMMENT '外键:部门编号',
  PRIMARY KEY (`emp_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 202012607 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of emp
-- ----------------------------
INSERT INTO `emp` VALUES (1, '1', '男', '12312312312', 1);
INSERT INTO `emp` VALUES (123123286, '123', '男', '123', 4);
INSERT INTO `emp` VALUES (123123317, '123', '男', '123', 3);
INSERT INTO `emp` VALUES (9, 'bbb', '男', '13569584712', 3);
INSERT INTO `emp` VALUES (22, '出库人', '女', '12312312312', 32);
INSERT INTO `emp` VALUES (123123323, '123', '男', '12312312312', 1);
INSERT INTO `emp` VALUES (202012356, '123', '男', '12312312312', 1);
INSERT INTO `emp` VALUES (202012217, '123', '男', '12312312312', 1);
INSERT INTO `emp` VALUES (202012486, '123', '男', '12312312312', 1);
INSERT INTO `emp` VALUES (123123320, '123', '男', '123', 2);

-- ----------------------------
-- Table structure for entrepot
-- ----------------------------
DROP TABLE IF EXISTS `entrepot`;
CREATE TABLE `entrepot`  (
  `entrepotid` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '仓库id',
  `entrepotname` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '仓库name',
  `entrepotloc` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '仓库地址',
  `statusid` int(11) NOT NULL COMMENT '审核状态信息',
  PRIMARY KEY (`entrepotid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '仓库' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of entrepot
-- ----------------------------
INSERT INTO `entrepot` VALUES (1, '总仓库', '河南省新乡市红旗区', 2);
INSERT INTO `entrepot` VALUES (2, '开封分仓', '河南省开封市龙亭区', 2);
INSERT INTO `entrepot` VALUES (3, '郑州分仓', '和南省郑州市金水区', 2);
INSERT INTO `entrepot` VALUES (8, '小仓库', '1233321', 2);

-- ----------------------------
-- Table structure for finance
-- ----------------------------
DROP TABLE IF EXISTS `finance`;
CREATE TABLE `finance`  (
  `financeid` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '审核状态信息',
  `num` decimal(11, 0) NOT NULL COMMENT '出货id(出货信息)',
  PRIMARY KEY (`financeid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '收货确认' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of finance
-- ----------------------------
INSERT INTO `finance` VALUES (1, '采购', 102737);
INSERT INTO `finance` VALUES (2, '定金', 3264);
INSERT INTO `finance` VALUES (3, '总额', 111601);
INSERT INTO `finance` VALUES (4, '物流', 5600);

-- ----------------------------
-- Table structure for instorage
-- ----------------------------
DROP TABLE IF EXISTS `instorage`;
CREATE TABLE `instorage`  (
  `Instorageid` int(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `statusid` int(11) NOT NULL COMMENT '审核状态',
  `ordersId` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分拣明细单id',
  `ordertime` date NOT NULL COMMENT '入库单据日期',
  `entrepotid` int(11) NOT NULL COMMENT '仓库id',
  `bz` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Instorageid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 65 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '入库明细单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of instorage
-- ----------------------------
INSERT INTO `instorage` VALUES (50, 2, 'XQ202012578', '2020-12-16', 1, '');
INSERT INTO `instorage` VALUES (51, 2, 'XQ202012740', '2020-12-16', 1, 'aaa');
INSERT INTO `instorage` VALUES (52, 2, 'XQ202012722', '2020-12-16', 1, 'aaa');
INSERT INTO `instorage` VALUES (53, 2, 'XQ202012283', '2020-12-16', 1, '');
INSERT INTO `instorage` VALUES (54, 2, 'XQ202012625', '2020-12-17', 1, '');
INSERT INTO `instorage` VALUES (55, 2, 'XQ202012241', '2020-12-17', 1, '');
INSERT INTO `instorage` VALUES (56, 2, 'XQ202012241', '2020-12-17', 1, '');
INSERT INTO `instorage` VALUES (57, 2, 'XQ202012666', '2020-12-17', 1, '');
INSERT INTO `instorage` VALUES (58, 2, 'XQ20224879', '2022-04-27', 1, '');
INSERT INTO `instorage` VALUES (59, 2, 'XQ202012258', '2022-04-28', 1, '');
INSERT INTO `instorage` VALUES (60, 2, 'XQ20224951', '2022-04-28', 1, '');
INSERT INTO `instorage` VALUES (61, 2, 'XQ202012909', '2022-04-28', 1, '');
INSERT INTO `instorage` VALUES (62, 2, 'XQ20224121', '2022-04-28', 1, '');
INSERT INTO `instorage` VALUES (63, 2, 'XQ20224203', '2022-04-28', 1, '');
INSERT INTO `instorage` VALUES (64, 2, 'XQ202244444', '2022-04-28', 1, '');

-- ----------------------------
-- Table structure for logdantype
-- ----------------------------
DROP TABLE IF EXISTS `logdantype`;
CREATE TABLE `logdantype`  (
  `logdantypeid` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `logdantypename` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '物流订单信息',
  PRIMARY KEY (`logdantypeid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '物流订单类型' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of logdantype
-- ----------------------------
INSERT INTO `logdantype` VALUES (1, '采购单');

-- ----------------------------
-- Table structure for logistics
-- ----------------------------
DROP TABLE IF EXISTS `logistics`;
CREATE TABLE `logistics`  (
  `logId` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `logname` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '物流公司名称',
  `logmodeid` int(11) NOT NULL COMMENT '物流方式id',
  `logleg` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '负责人',
  `statusid` int(11) NOT NULL COMMENT '审核状态信息',
  PRIMARY KEY (`logId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '物流公司' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of logistics
-- ----------------------------
INSERT INTO `logistics` VALUES (1, '小a速递', 1, '小a', 1);
INSERT INTO `logistics` VALUES (2, '小b速递', 2, '小b', 1);
INSERT INTO `logistics` VALUES (3, '冷库船速递', 3, '冷船', 2);
INSERT INTO `logistics` VALUES (4, '小d速递', 2, '小d', 1);
INSERT INTO `logistics` VALUES (5, '冷库机速递', 1, '冷寂', 2);
INSERT INTO `logistics` VALUES (7, '123', 2, '123', 1);
INSERT INTO `logistics` VALUES (8, '冷库车速递', 2, '冷车', 2);

-- ----------------------------
-- Table structure for logmode
-- ----------------------------
DROP TABLE IF EXISTS `logmode`;
CREATE TABLE `logmode`  (
  `logmodeid` int(11) NOT NULL AUTO_INCREMENT COMMENT '物流运输id',
  `logmodename` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '运输方式',
  `logmodeprice` decimal(10, 1) NOT NULL COMMENT '运输价格',
  PRIMARY KEY (`logmodeid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '物流公司运输方式' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of logmode
-- ----------------------------
INSERT INTO `logmode` VALUES (1, '空运冷库机', 500.0);
INSERT INTO `logmode` VALUES (2, '陆运冷库车', 200.0);
INSERT INTO `logmode` VALUES (3, '海运冷库船', 200.0);

-- ----------------------------
-- Table structure for logparticulars
-- ----------------------------
DROP TABLE IF EXISTS `logparticulars`;
CREATE TABLE `logparticulars`  (
  `logparticularsid` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键ID',
  `logId` int(11) NOT NULL COMMENT '物流公司名称id',
  `logstatusid` int(11) NOT NULL COMMENT '物流公司运送状态id',
  `logdantypeid` int(11) NOT NULL COMMENT '物流订单信息',
  PRIMARY KEY (`logparticularsid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '物流订单跟踪明细' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of logparticulars
-- ----------------------------
INSERT INTO `logparticulars` VALUES ('WL202012241', 5, 3, 1);
INSERT INTO `logparticulars` VALUES ('WL202012258', 3, 3, 1);
INSERT INTO `logparticulars` VALUES ('WL202012283', 3, 3, 1);
INSERT INTO `logparticulars` VALUES ('WL202012416', 3, 3, 1);
INSERT INTO `logparticulars` VALUES ('WL202012625', 5, 3, 1);
INSERT INTO `logparticulars` VALUES ('WL202012666', 3, 3, 1);
INSERT INTO `logparticulars` VALUES ('WL202012722', 3, 3, 1);
INSERT INTO `logparticulars` VALUES ('WL202012909', 3, 3, 1);
INSERT INTO `logparticulars` VALUES ('WL20224121', 5, 3, 1);
INSERT INTO `logparticulars` VALUES ('WL20224203', 3, 3, 1);
INSERT INTO `logparticulars` VALUES ('WL20224533', 5, 3, 1);
INSERT INTO `logparticulars` VALUES ('WL20224679', 3, 2, 1);
INSERT INTO `logparticulars` VALUES ('WL20224879', 3, 3, 1);
INSERT INTO `logparticulars` VALUES ('WL20224887', 3, 3, 1);
INSERT INTO `logparticulars` VALUES ('WL20224951', 3, 3, 1);

-- ----------------------------
-- Table structure for logstatus
-- ----------------------------
DROP TABLE IF EXISTS `logstatus`;
CREATE TABLE `logstatus`  (
  `logstatusid` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `logstatusname` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态信息',
  PRIMARY KEY (`logstatusid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '物流公司状态表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of logstatus
-- ----------------------------
INSERT INTO `logstatus` VALUES (1, '待揽收');
INSERT INTO `logstatus` VALUES (2, '已发货');
INSERT INTO `logstatus` VALUES (3, '已完成');

-- ----------------------------
-- Table structure for need
-- ----------------------------
DROP TABLE IF EXISTS `need`;
CREATE TABLE `need`  (
  `needId` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `needName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '需求项目名',
  `needimg` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '样板包装图片',
  `needtime` date NOT NULL COMMENT '发单日期',
  `saadminid` int(11) NOT NULL COMMENT '负责人(7设计)',
  `BsupId` int(11) NOT NULL COMMENT '分销商id',
  `statusid` int(11) NOT NULL COMMENT '审核状态',
  PRIMARY KEY (`needId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 39 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '样板包装信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of need
-- ----------------------------
INSERT INTO `need` VALUES (1, '黑色包装', '/files/754615e7-a63c-4424-9364-f69618f0f9b6.gif', '2020-12-04', 123123320, 2, 1);
INSERT INTO `need` VALUES (12, '白色包裝', '/files/a453915f-91a4-4d32-b7ce-d696710585db.gif', '2020-12-04', 123123320, 2, 1);
INSERT INTO `need` VALUES (35, '红色包装', '/files/918d756e-fe8f-456f-94de-6d3947667d5a.gif', '2020-12-08', 123123320, 2, 1);
INSERT INTO `need` VALUES (38, '二萬人', '/files/c6ce5367-9672-4d67-8bda-95ffd1a6bb3d.gif', '2022-04-11', 123123320, 3, 2);

-- ----------------------------
-- Table structure for outprse
-- ----------------------------
DROP TABLE IF EXISTS `outprse`;
CREATE TABLE `outprse`  (
  `outprseid` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `statusid` int(11) NOT NULL COMMENT '审核状态信息',
  `ordersId` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `outprsetime` date NULL DEFAULT NULL,
  `needId` int(11) NULL DEFAULT NULL,
  `logId` int(11) NULL DEFAULT NULL,
  `entrepotid` int(11) NULL DEFAULT NULL,
  `outstorageid` int(11) NULL DEFAULT NULL,
  `wcstatusid` int(11) NULL DEFAULT NULL,
  `commodityid` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`outprseid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 140 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '批量领料申请' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of outprse
-- ----------------------------
INSERT INTO `outprse` VALUES (113, 2, 'XQ202012283', '2020-12-16', 35, 3, 1, 29, 1, 31);
INSERT INTO `outprse` VALUES (120, 2, 'XQ202012241', '2020-12-16', 35, 5, 1, 29, 1, 31);
INSERT INTO `outprse` VALUES (124, 1, '1', '2022-04-30', 1, 1, 1, 1, 1, 1);
INSERT INTO `outprse` VALUES (125, 1, 'XQ20224879', '2022-04-30', 38, 3, 1, 35, 1, NULL);
INSERT INTO `outprse` VALUES (126, 1, 'XQ20224879', '2022-04-30', 38, 3, 1, 35, 1, NULL);
INSERT INTO `outprse` VALUES (127, 1, 'XQ20224879', '2022-04-30', 38, 3, 1, 35, 1, NULL);
INSERT INTO `outprse` VALUES (134, 1, 'XQ20224879', '2022-04-30', 38, 3, 1, 35, 1, 32);
INSERT INTO `outprse` VALUES (135, 1, 'XQ20224879', '2022-04-30', 38, 3, 1, 35, 1, 32);
INSERT INTO `outprse` VALUES (136, 1, 'XQ20224879', '2022-04-30', 38, 3, 1, 35, 1, 32);
INSERT INTO `outprse` VALUES (139, 1, 'XQ20224879', '2022-04-30', 38, 3, 1, 35, 1, 32);

-- ----------------------------
-- Table structure for outstorage
-- ----------------------------
DROP TABLE IF EXISTS `outstorage`;
CREATE TABLE `outstorage`  (
  `outstorageid` int(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `statusid` int(11) NOT NULL COMMENT '审核状态',
  `ordertime` date NOT NULL COMMENT '出库单据日期',
  `ordersId` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品供应商id',
  `entrepotid` int(11) NULL DEFAULT NULL,
  `bz` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`outstorageid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 42 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '出库明细单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of outstorage
-- ----------------------------
INSERT INTO `outstorage` VALUES (1, 2, '2020-12-01', 'XQ202012241', 1, NULL);
INSERT INTO `outstorage` VALUES (34, 2, '2020-12-17', 'XQ202012666', 1, '');
INSERT INTO `outstorage` VALUES (35, 2, '2022-04-27', 'XQ20224879', 1, '');
INSERT INTO `outstorage` VALUES (36, 2, '2022-04-28', 'XQ202012258', 1, '');
INSERT INTO `outstorage` VALUES (37, 2, '2022-04-28', 'XQ20224951', 1, '');
INSERT INTO `outstorage` VALUES (38, 2, '2022-04-28', 'XQ202012909', 1, '');
INSERT INTO `outstorage` VALUES (39, 2, '2022-04-28', 'XQ20224121', 1, '');
INSERT INTO `outstorage` VALUES (40, 2, '2022-04-28', 'XQ20224203', 1, '');
INSERT INTO `outstorage` VALUES (41, 2, '2022-04-28', 'XQ202244444', 1, '');

-- ----------------------------
-- Table structure for prse
-- ----------------------------
DROP TABLE IF EXISTS `prse`;
CREATE TABLE `prse`  (
  `prseid` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `statusid` int(11) NOT NULL COMMENT '样板id',
  `ordersId` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '完成情况',
  `ordertime1` date NULL DEFAULT NULL,
  `needId` int(11) NULL DEFAULT NULL,
  `logmodeid` int(11) NULL DEFAULT NULL,
  `entrepotid` int(11) NULL DEFAULT NULL,
  `outstorageid` int(200) NULL DEFAULT NULL,
  `commodityid` int(11) NULL DEFAULT NULL,
  `dis_need_id` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `logId` int(11) NULL DEFAULT NULL,
  `wcstatusid` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`prseid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 132 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '生产进度' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of prse
-- ----------------------------
INSERT INTO `prse` VALUES (123, 1, 'XQ20224877', '2022-04-28', 38, 3, 1, 35, 32, 'XQ202012241', 3, 1);
INSERT INTO `prse` VALUES (124, 1, 'XQ20224879', '2022-04-30', 38, 3, 1, 35, 32, 'XQ202012241', 3, 1);
INSERT INTO `prse` VALUES (125, 1, 'XQ202012909', NULL, 35, 3, 1, 38, 19, 'XQ202012241', 3, 1);
INSERT INTO `prse` VALUES (126, 1, '1', '2022-04-28', 38, 3, 113, 5, 32, '1', 3, 1);
INSERT INTO `prse` VALUES (127, 1, 'XQ202012909', NULL, 35, 3, 1, 38, 19, '', 3, 1);
INSERT INTO `prse` VALUES (128, 1, 'XQ20224879', NULL, 38, 3, 1, 35, 32, '', 3, 1);
INSERT INTO `prse` VALUES (129, 1, 'XQ20224879', NULL, 38, 3, 1, 35, 32, '', 3, 1);
INSERT INTO `prse` VALUES (130, 1, 'XQ20224879', NULL, 38, 3, 1, 35, 32, '', 3, 1);
INSERT INTO `prse` VALUES (131, 1, 'XQ20224879', NULL, 38, 3, 1, 35, 32, '', 3, 1);

-- ----------------------------
-- Table structure for purchaseorders
-- ----------------------------
DROP TABLE IF EXISTS `purchaseorders`;
CREATE TABLE `purchaseorders`  (
  `ordersId` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键ID',
  `statusid` int(11) NULL DEFAULT NULL COMMENT '审核状态',
  `ordertime` date NULL DEFAULT NULL COMMENT '采购单据日期',
  `commodityid` int(8) NULL DEFAULT NULL COMMENT '商品id',
  `Samnumber` int(20) NULL DEFAULT NULL COMMENT '采购商品数量',
  `BsupId` int(8) NULL DEFAULT NULL COMMENT '商品包装供应商id',
  `satypenumber` int(20) NULL DEFAULT NULL COMMENT '采购商品包装数量',
  `orloc` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '当前地址',
  `arrloc` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '到达地址',
  `logId` int(11) NULL DEFAULT NULL COMMENT '物流公司id',
  `advance` decimal(10, 1) NOT NULL COMMENT '应付金额',
  `emp_id` int(11) NULL DEFAULT NULL,
  `logparticularsid` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ordersId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '采购订单明细' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of purchaseorders
-- ----------------------------
INSERT INTO `purchaseorders` VALUES ('XQ202012241', 1, '2020-12-24', 31, 23, 2, 23, '河南省新乡市红旗区', '河南省郑州市金水区', 3, 506.0, 9, 'WL202012939');
INSERT INTO `purchaseorders` VALUES ('XQ202012258', 2, '2020-12-02', 31, 12, 2, 12, '河南省新乡市红旗区', '河南省开封市龙亭区', 3, 264.0, 9, 'WL202012258');
INSERT INTO `purchaseorders` VALUES ('XQ202012283', 2, '2020-12-18', 31, 23, 2, 23, '河南省新乡市红旗区', '河南省开封市龙亭区', 3, 506.0, 9, 'WL202012283');
INSERT INTO `purchaseorders` VALUES ('XQ202012309', 2, '2020-12-11', 31, 23, 2, 23, '河南省新乡市红旗区', '河南省郑州市金水区', 5, 506.0, 9, 'WL202012241');
INSERT INTO `purchaseorders` VALUES ('XQ202012416', 2, '2020-12-09', 31, 12, 2, 12, '河南省新乡市红旗区', '河南省开封市龙亭区', 3, 264.0, 9, 'WL202012416');
INSERT INTO `purchaseorders` VALUES ('XQ202012625', 2, '2020-12-11', 31, 23, 2, 23, '河南省新乡市红旗区', '河南省开封市龙亭区', 5, 506.0, 9, 'WL202012625');
INSERT INTO `purchaseorders` VALUES ('XQ202012666', 2, '2020-12-16', 31, 12, 2, 21, '河南省新乡市红旗区', '河南省开封市龙亭区', 3, 354.0, 9, 'WL202012666');
INSERT INTO `purchaseorders` VALUES ('XQ202012722', 2, '2020-12-18', 31, 23, 2, 23, '河南省新乡市红旗区', '河南省开封市龙亭区', 5, 506.0, 9, 'WL202012722');
INSERT INTO `purchaseorders` VALUES ('XQ202012740', 2, '2020-12-12', 31, 23, 2, 23, '河南省新乡市红旗区', '河南省开封市龙亭区', 3, 506.0, 9, 'WL202012740');
INSERT INTO `purchaseorders` VALUES ('XQ202012885', 1, '2020-12-24', 31, 23, 2, 23, '河南省新乡市红旗区', '河南省开封市龙亭区', 3, 506.0, 9, 'WL202012885');
INSERT INTO `purchaseorders` VALUES ('XQ202012897', 1, '2020-12-12', 31, 23, 2, 23, '河南省新乡市红旗区', '河南省郑州市金水区', 3, 506.0, 9, 'WL202012897');
INSERT INTO `purchaseorders` VALUES ('XQ202012909', 2, '2020-12-17', 19, 34, 2, 34, '河南省新乡市红旗区', '河南省开封市龙亭区', 3, 748.0, 9, 'WL202012909');
INSERT INTO `purchaseorders` VALUES ('XQ20224121', 2, '2022-04-28', 32, 1, 2, 1, '河南省新乡市红旗区', '河南省开封市龙亭区', 5, 52.0, 9, 'WL20224121');
INSERT INTO `purchaseorders` VALUES ('XQ20224203', 2, '2022-04-28', 32, 1111, 2, 1111, '河南省新乡市红旗区', '河南省开封市龙亭区', 3, 57772.0, 9, 'WL20224203');
INSERT INTO `purchaseorders` VALUES ('XQ202244444', 2, '2022-04-28', 32, 555, 3, 5, '河南省新乡市红旗区', '河南省开封市龙亭区', 5, 23385.0, 9, 'WL20224533');
INSERT INTO `purchaseorders` VALUES ('XQ20224679', 1, '2022-04-28', 32, 1, 3, 1, '河南省新乡市红旗区', '河南省开封市龙亭区', 3, 57.0, 9, 'WL20224679');
INSERT INTO `purchaseorders` VALUES ('XQ20224879', 2, '2022-04-28', 32, 435, 2, 45, '河南省新乡市红旗区', '河南省开封市龙亭区', 3, 18720.0, 9, 'WL20224879');
INSERT INTO `purchaseorders` VALUES ('XQ20224887', 2, '2022-04-28', 32, 1, 2, 1, '河南省新乡市红旗区', '河南省开封市龙亭区', 3, 52.0, 9, 'WL20224887');
INSERT INTO `purchaseorders` VALUES ('XQ20224951', 2, '2022-04-20', 32, 1, 3, 1, '河南省新乡市红旗区', '河南省郑州市金水区', 3, 57.0, 9, 'WL20224951');

-- ----------------------------
-- Table structure for samorder
-- ----------------------------
DROP TABLE IF EXISTS `samorder`;
CREATE TABLE `samorder`  (
  `orderId` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `statusid` int(11) NOT NULL COMMENT '审核状态',
  `needId` int(11) NOT NULL COMMENT '样板包装信息id',
  `distributorid` int(11) NOT NULL COMMENT '分销商id',
  `Bsprice` int(11) NOT NULL COMMENT '样板包装单价',
  `Bsnumber` int(11) NOT NULL COMMENT '分销商需求数量',
  `saadminid` int(11) NOT NULL COMMENT '负责人（包装订单）',
  `begintime` date NOT NULL COMMENT '生产日期',
  `finishtime` date NOT NULL COMMENT '交付日期',
  PRIMARY KEY (`orderId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '样板包装订单详情' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of samorder
-- ----------------------------
INSERT INTO `samorder` VALUES (1, 1, 2, 1, 50, 300, 5, '2020-02-06', '2020-02-15');
INSERT INTO `samorder` VALUES (2, 2, 1, 2, 60, 400, 5, '2019-05-26', '2020-08-15');
INSERT INTO `samorder` VALUES (3, 1, 3, 3, 70, 500, 5, '2019-04-05', '2020-07-15');
INSERT INTO `samorder` VALUES (4, 2, 3, 2, 56, 555, 4, '2020-11-09', '2020-11-24');
INSERT INTO `samorder` VALUES (5, 2, 3, 3, 34, 45, 3, '2020-11-24', '2020-11-24');

-- ----------------------------
-- Table structure for shouhuo
-- ----------------------------
DROP TABLE IF EXISTS `shouhuo`;
CREATE TABLE `shouhuo`  (
  `shid` int(11) NOT NULL,
  `shouhuo` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`shid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of shouhuo
-- ----------------------------
INSERT INTO `shouhuo` VALUES (1, '已收货');
INSERT INTO `shouhuo` VALUES (2, '未收货');

-- ----------------------------
-- Table structure for shqr
-- ----------------------------
DROP TABLE IF EXISTS `shqr`;
CREATE TABLE `shqr`  (
  `shqrid` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `statusid` int(11) NOT NULL COMMENT '审核状态信息',
  `outprseid` int(11) NOT NULL COMMENT '出货id(出货信息)',
  `logparticularsid` int(11) NOT NULL COMMENT '物流公司订单id',
  `account` int(11) NOT NULL COMMENT '应收款',
  PRIMARY KEY (`shqrid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '收货确认' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of shqr
-- ----------------------------
INSERT INTO `shqr` VALUES (1, 1, 1, 1, 1000);
INSERT INTO `shqr` VALUES (2, 2, 1, 2, 2000);
INSERT INTO `shqr` VALUES (3, 1, 2, 1, 3000);

-- ----------------------------
-- Table structure for shqr_copy
-- ----------------------------
DROP TABLE IF EXISTS `shqr_copy`;
CREATE TABLE `shqr_copy`  (
  `shqrid` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `statusid` int(11) NOT NULL COMMENT '审核状态信息',
  `outprseid` int(11) NOT NULL COMMENT '出货id(出货信息)',
  `logparticularsid` int(11) NOT NULL COMMENT '物流公司订单id',
  `account` int(11) NOT NULL COMMENT '应收款',
  PRIMARY KEY (`shqrid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '收货确认' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of shqr_copy
-- ----------------------------
INSERT INTO `shqr_copy` VALUES (1, 1, 1, 1, 1000);
INSERT INTO `shqr_copy` VALUES (2, 2, 1, 2, 2000);
INSERT INTO `shqr_copy` VALUES (3, 1, 2, 1, 3000);

-- ----------------------------
-- Table structure for sortparticulars
-- ----------------------------
DROP TABLE IF EXISTS `sortparticulars`;
CREATE TABLE `sortparticulars`  (
  `sparId` int(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `sortstatusid` int(8) NULL DEFAULT NULL COMMENT '分拣状态id',
  `emp_id` int(11) NOT NULL COMMENT '分拣负责人id',
  `ordersId` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '采购订单id',
  PRIMARY KEY (`sparId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 59 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '分拣明细单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sortparticulars
-- ----------------------------
INSERT INTO `sortparticulars` VALUES (10, 1, 5, '10045');
INSERT INTO `sortparticulars` VALUES (11, 1, 5, '10045');
INSERT INTO `sortparticulars` VALUES (12, 2, 5, '10047');
INSERT INTO `sortparticulars` VALUES (13, 1, 7, '10052');
INSERT INTO `sortparticulars` VALUES (14, 1, 7, '10054');
INSERT INTO `sortparticulars` VALUES (15, 1, 123123286, 'XQ202012309');
INSERT INTO `sortparticulars` VALUES (16, 1, 123123286, 'XQ202012309');
INSERT INTO `sortparticulars` VALUES (17, 1, 123123286, 'XQ202012309');
INSERT INTO `sortparticulars` VALUES (18, 1, 123123286, 'XQ202012309');
INSERT INTO `sortparticulars` VALUES (19, 1, 123123286, 'XQ202012309');
INSERT INTO `sortparticulars` VALUES (20, 1, 123123286, 'XQ202012309');
INSERT INTO `sortparticulars` VALUES (21, 1, 123123286, 'XQ202012309');
INSERT INTO `sortparticulars` VALUES (22, 1, 123123286, 'XQ202012309');
INSERT INTO `sortparticulars` VALUES (23, 1, 123123286, 'XQ202012309');
INSERT INTO `sortparticulars` VALUES (24, 1, 123123286, 'XQ202012309');
INSERT INTO `sortparticulars` VALUES (25, 1, 123123286, 'XQ202012309');
INSERT INTO `sortparticulars` VALUES (26, 1, 123123286, 'XQ202012682');
INSERT INTO `sortparticulars` VALUES (27, 1, 123123286, 'XQ202012188');
INSERT INTO `sortparticulars` VALUES (28, 1, 123123286, 'XQ202012188');
INSERT INTO `sortparticulars` VALUES (29, 1, 123123286, 'XQ202012188');
INSERT INTO `sortparticulars` VALUES (30, 1, 123123286, 'XQ202012355');
INSERT INTO `sortparticulars` VALUES (31, 1, 123123286, 'XQ202012355');
INSERT INTO `sortparticulars` VALUES (32, 1, 123123286, 'XQ202012355');
INSERT INTO `sortparticulars` VALUES (33, 1, 123123286, 'XQ202012355');
INSERT INTO `sortparticulars` VALUES (34, 1, 123123286, 'XQ202012355');
INSERT INTO `sortparticulars` VALUES (35, 1, 123123286, 'XQ202012355');
INSERT INTO `sortparticulars` VALUES (36, 1, 123123286, 'XQ202012355');
INSERT INTO `sortparticulars` VALUES (37, 1, 123123286, 'XQ202012355');
INSERT INTO `sortparticulars` VALUES (38, 1, 123123286, 'XQ202012355');
INSERT INTO `sortparticulars` VALUES (39, 1, 123123286, 'XQ202012355');
INSERT INTO `sortparticulars` VALUES (40, 1, 123123286, 'XQ202012355');
INSERT INTO `sortparticulars` VALUES (41, 1, 123123286, 'XQ202012807');
INSERT INTO `sortparticulars` VALUES (42, 1, 123123286, 'XQ202012578');
INSERT INTO `sortparticulars` VALUES (43, 1, 123123286, 'XQ202012740');
INSERT INTO `sortparticulars` VALUES (44, 1, 123123286, 'XQ202012722');
INSERT INTO `sortparticulars` VALUES (45, 1, 123123286, 'XQ202012283');
INSERT INTO `sortparticulars` VALUES (46, 1, 123123286, 'XQ202012625');
INSERT INTO `sortparticulars` VALUES (47, 1, 123123286, 'XQ202012241');
INSERT INTO `sortparticulars` VALUES (48, 1, 123123286, 'XQ202012241');
INSERT INTO `sortparticulars` VALUES (49, 2, 123123286, 'XQ202012416');
INSERT INTO `sortparticulars` VALUES (50, 1, 123123286, 'XQ202012666');
INSERT INTO `sortparticulars` VALUES (51, 1, 123123286, 'XQ20224879');
INSERT INTO `sortparticulars` VALUES (52, 1, 123123286, 'XQ202012258');
INSERT INTO `sortparticulars` VALUES (53, 1, 123123286, 'XQ20224951');
INSERT INTO `sortparticulars` VALUES (54, 1, 123123286, 'XQ202012909');
INSERT INTO `sortparticulars` VALUES (55, 1, 123123286, 'XQ20224121');
INSERT INTO `sortparticulars` VALUES (56, 1, 123123286, 'XQ20224203');
INSERT INTO `sortparticulars` VALUES (57, 1, 123123286, 'XQ202244444');
INSERT INTO `sortparticulars` VALUES (58, 2, 123123286, 'XQ20224887');

-- ----------------------------
-- Table structure for sortstatus
-- ----------------------------
DROP TABLE IF EXISTS `sortstatus`;
CREATE TABLE `sortstatus`  (
  `sortstatusid` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `sortstatusname` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分拣状态信息',
  PRIMARY KEY (`sortstatusid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '分拣状态表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sortstatus
-- ----------------------------
INSERT INTO `sortstatus` VALUES (1, '已分拣');
INSERT INTO `sortstatus` VALUES (2, '未分拣');
INSERT INTO `sortstatus` VALUES (3, '正在分拣');

-- ----------------------------
-- Table structure for statusl
-- ----------------------------
DROP TABLE IF EXISTS `statusl`;
CREATE TABLE `statusl`  (
  `statusid` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `statusname` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态信息',
  PRIMARY KEY (`statusid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '状态表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of statusl
-- ----------------------------
INSERT INTO `statusl` VALUES (1, '未审核');
INSERT INTO `statusl` VALUES (2, '已审核');

-- ----------------------------
-- Table structure for str_produce
-- ----------------------------
DROP TABLE IF EXISTS `str_produce`;
CREATE TABLE `str_produce`  (
  `entrepotid` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '仓库商品id',
  `entrepotname` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品名字',
  `statusid` int(11) NOT NULL COMMENT '审核状态信息',
  `ordertime` date NOT NULL COMMENT '入库单据日期',
  `categoryid` int(11) NOT NULL COMMENT '商品供应商id',
  `BsupId` int(11) NOT NULL COMMENT '商品包装供应商id',
  `inventory` int(11) NOT NULL COMMENT '库存',
  PRIMARY KEY (`entrepotid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '仓库' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of str_produce
-- ----------------------------
INSERT INTO `str_produce` VALUES (1, '帝王蟹', 1, '2020-02-02', 1, 1, 3000);
INSERT INTO `str_produce` VALUES (2, '大闸蟹', 2, '2020-09-18', 1, 2, 2000);

-- ----------------------------
-- Table structure for supplier
-- ----------------------------
DROP TABLE IF EXISTS `supplier`;
CREATE TABLE `supplier`  (
  `supId` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `supName` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品供应商名',
  `categoryid` int(11) NOT NULL COMMENT '商品类别',
  `statusid` int(11) NULL DEFAULT NULL COMMENT '审核状态',
  `suploc` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品供应商地址',
  PRIMARY KEY (`supId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品供应商' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of supplier
-- ----------------------------
INSERT INTO `supplier` VALUES (1, '北京海纳百鲜商贸有限公司', 2, 1, '北京市');
INSERT INTO `supplier` VALUES (2, '靖江海之恋水产养殖有限公司', 2, 2, '江苏泰州');
INSERT INTO `supplier` VALUES (3, '福建富庚农业发展有限公司', 3, 1, '辽宁省大连市');

-- ----------------------------
-- Table structure for sys_right
-- ----------------------------
DROP TABLE IF EXISTS `sys_right`;
CREATE TABLE `sys_right`  (
  `right_id` int(11) NOT NULL AUTO_INCREMENT,
  `right_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限名称',
  `right_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限链接',
  `level` int(11) NULL DEFAULT NULL COMMENT '权限级别：1,2',
  `parent_id` int(11) NULL DEFAULT NULL COMMENT '父级菜单:0该菜单为一级菜单',
  `statusid` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`right_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 101 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_right
-- ----------------------------
INSERT INTO `sys_right` VALUES (48, '包装商详情', '/bsupplier/page', 2, 21, 2);
INSERT INTO `sys_right` VALUES (13, '人员管理', '#', 1, 0, 2);
INSERT INTO `sys_right` VALUES (14, '包装样板', '/need/page', 2, 21, 2);
INSERT INTO `sys_right` VALUES (15, '采购管理', '#', 1, 0, 2);
INSERT INTO `sys_right` VALUES (16, '出入库管理', '#', 1, 0, 2);
INSERT INTO `sys_right` VALUES (17, '物流管理', '#', 1, 0, 2);
INSERT INTO `sys_right` VALUES (18, '生产批量', '#', 1, 0, 2);
INSERT INTO `sys_right` VALUES (19, '分销商收货', '#', 1, 0, 2);
INSERT INTO `sys_right` VALUES (21, '链管理', '#', 1, 0, 2);
INSERT INTO `sys_right` VALUES (22, '图标字体', '#', 2, 0, 2);
INSERT INTO `sys_right` VALUES (23, '部门管理', '/emp/select', 2, 13, 2);
INSERT INTO `sys_right` VALUES (52, '11', '#', 1, 0, NULL);
INSERT INTO `sys_right` VALUES (25, '供应商商品', '/commodity/selectAll', 2, 21, 2);
INSERT INTO `sys_right` VALUES (26, '分销需求', '/dis_need/page', 2, 1, 2);
INSERT INTO `sys_right` VALUES (28, '采购订单明细', '/indent/selectAll', 2, 15, 2);
INSERT INTO `sys_right` VALUES (29, '分拣明细单', '/Sortp/selectAll', 2, 16, 2);
INSERT INTO `sys_right` VALUES (30, '入库明细单', '/Ins/selectAll', 2, 16, 2);
INSERT INTO `sys_right` VALUES (32, '出库审批', '/out/selectAll1', 2, 16, 2);
INSERT INTO `sys_right` VALUES (50, '出库明细表', '/out/selectAll', 2, 16, 2);
INSERT INTO `sys_right` VALUES (33, '仓库管理', '/entrepot/selectAll', 2, 21, 2);
INSERT INTO `sys_right` VALUES (34, '物流公司', '/logistics/selectAll', 2, 17, 2);
INSERT INTO `sys_right` VALUES (36, '批量领料申请', '/bping/selectAll', 2, 18, 2);
INSERT INTO `sys_right` VALUES (37, '生产进度', '/outprse/glselect', 2, 18, 2);
INSERT INTO `sys_right` VALUES (38, '出货表', 'outprse/selectAll', 2, 18, 2);
INSERT INTO `sys_right` VALUES (39, '确认收货', 'outprse/selectAll9', 2, 19, 2);
INSERT INTO `sys_right` VALUES (45, '权限管理', '/sysrr/selectAll', 2, 21, 2);
INSERT INTO `sys_right` VALUES (1, '样板管理', '#', 1, 0, NULL);
INSERT INTO `sys_right` VALUES (49, '分销商详情', '/Distributor/page', 2, 21, 2);
INSERT INTO `sys_right` VALUES (51, '物流订单明细', '/logparticulars/selectAll', 2, 17, 2);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, 'admin');
INSERT INTO `sys_role` VALUES (2, 'normal');
INSERT INTO `sys_role` VALUES (3, 'cangnormal');
INSERT INTO `sys_role` VALUES (4, '123');

-- ----------------------------
-- Table structure for sys_role_right
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_right`;
CREATE TABLE `sys_role_right`  (
  `role_id` int(11) NOT NULL,
  `right_id` int(11) NOT NULL,
  PRIMARY KEY (`right_id`, `role_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of sys_role_right
-- ----------------------------
INSERT INTO `sys_role_right` VALUES (1, 1);
INSERT INTO `sys_role_right` VALUES (1, 13);
INSERT INTO `sys_role_right` VALUES (2, 13);
INSERT INTO `sys_role_right` VALUES (1, 14);
INSERT INTO `sys_role_right` VALUES (1, 15);
INSERT INTO `sys_role_right` VALUES (2, 15);
INSERT INTO `sys_role_right` VALUES (1, 16);
INSERT INTO `sys_role_right` VALUES (3, 16);
INSERT INTO `sys_role_right` VALUES (1, 17);
INSERT INTO `sys_role_right` VALUES (1, 18);
INSERT INTO `sys_role_right` VALUES (1, 19);
INSERT INTO `sys_role_right` VALUES (1, 21);
INSERT INTO `sys_role_right` VALUES (2, 21);
INSERT INTO `sys_role_right` VALUES (1, 22);
INSERT INTO `sys_role_right` VALUES (2, 22);
INSERT INTO `sys_role_right` VALUES (1, 23);
INSERT INTO `sys_role_right` VALUES (1, 25);
INSERT INTO `sys_role_right` VALUES (1, 26);
INSERT INTO `sys_role_right` VALUES (1, 28);
INSERT INTO `sys_role_right` VALUES (2, 28);
INSERT INTO `sys_role_right` VALUES (1, 29);
INSERT INTO `sys_role_right` VALUES (3, 29);
INSERT INTO `sys_role_right` VALUES (1, 30);
INSERT INTO `sys_role_right` VALUES (3, 30);
INSERT INTO `sys_role_right` VALUES (1, 32);
INSERT INTO `sys_role_right` VALUES (3, 32);
INSERT INTO `sys_role_right` VALUES (1, 33);
INSERT INTO `sys_role_right` VALUES (3, 33);
INSERT INTO `sys_role_right` VALUES (1, 34);
INSERT INTO `sys_role_right` VALUES (1, 36);
INSERT INTO `sys_role_right` VALUES (1, 37);
INSERT INTO `sys_role_right` VALUES (1, 38);
INSERT INTO `sys_role_right` VALUES (1, 39);
INSERT INTO `sys_role_right` VALUES (1, 45);
INSERT INTO `sys_role_right` VALUES (1, 48);
INSERT INTO `sys_role_right` VALUES (1, 49);
INSERT INTO `sys_role_right` VALUES (1, 50);
INSERT INTO `sys_role_right` VALUES (3, 50);
INSERT INTO `sys_role_right` VALUES (1, 51);
INSERT INTO `sys_role_right` VALUES (1, 54);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_psd` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_statusid` int(11) NOT NULL,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 22 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'aaa', '$2a$10$0aQAzaJ9NjA7iZ3GgzOk1eU/cHJ6y2MEUpG9OC4d4i86YwC3.0klO', 2);
INSERT INTO `sys_user` VALUES (2, 'ccc', '$2a$10$vIA8m2BfTBPtOjx/tP3gkOegS9P5bsWB90E45eowxPHMzyD/bVbKa', 1);
INSERT INTO `sys_user` VALUES (21, 'admin', '$2a$10$MWCKoJbe3HxF7ELunltsruxfXtUtF4N.QQ4t5PTGhJbtOVfLA8SLu', 2);
INSERT INTO `sys_user` VALUES (17, 'qwe', '$2a$10$6iGD.ePfbOifBIsdDVxv..a/muublnfACkC.IftVxy7CWhZif3MUO', 1);
INSERT INTO `sys_user` VALUES (20, 'e', '$2a$10$wARVVR20EmDulb5wLD37/uNmmaRCOqjiKpEs1Ih1aZR2BMyIwFoy2', 2);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (2, 2);
INSERT INTO `sys_user_role` VALUES (14, 2);
INSERT INTO `sys_user_role` VALUES (17, 3);
INSERT INTO `sys_user_role` VALUES (20, 3);
INSERT INTO `sys_user_role` VALUES (21, 1);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `age` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'admin', '123456');
INSERT INTO `user` VALUES (2, '123', '123');
INSERT INTO `user` VALUES (3, '123', '123');

-- ----------------------------
-- Table structure for user_status
-- ----------------------------
DROP TABLE IF EXISTS `user_status`;
CREATE TABLE `user_status`  (
  `user_statusid` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `user_statusname` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '审核状态信息',
  PRIMARY KEY (`user_statusid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '审核状态信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_status
-- ----------------------------
INSERT INTO `user_status` VALUES (1, '禁用');
INSERT INTO `user_status` VALUES (2, '未禁用');

-- ----------------------------
-- Table structure for wancheng
-- ----------------------------
DROP TABLE IF EXISTS `wancheng`;
CREATE TABLE `wancheng`  (
  `wcstatusid` int(11) NOT NULL AUTO_INCREMENT,
  `wcstatus` varchar(22) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`wcstatusid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wancheng
-- ----------------------------
INSERT INTO `wancheng` VALUES (1, '已完成');
INSERT INTO `wancheng` VALUES (2, '未完成');

SET FOREIGN_KEY_CHECKS = 1;
