package com.example.Controller;


import com.example.Service.SupplierService;
import com.example.model.dept;
import com.example.model.supplier;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/supplier")
@Slf4j
public class SupplierDaoController {
    @Autowired
    SupplierService supplierService;

    @RequestMapping("/selectAll")
    public String selectAll(Model m,supplier supplier) {
        List<supplier> list = supplierService.selectAll();
        m.addAttribute("supplier",list);
        return "./supplier/supplier-list.html";
    }

}
