package com.example.Controller;


import com.example.Service.empService;
import com.example.model.JsonMessage;
import com.example.model.SysRight;
import com.example.model.dept;
import com.example.model.emp;
import com.example.util.ResponseUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/emp")
@Slf4j
public class EmpController {
    @Autowired
    empService empService;
@RequestMapping("/select")
public String select(Model model){
    List<dept> list = empService.selectdept();
    model.addAttribute("dept",list);
    return "./emp/emp-lists.html";

}
 @RequestMapping("/selectAll")
 @ResponseBody
    public Map<String,Object> selectAll(Model model,
      @RequestParam("page")Integer pageNum,
      @RequestParam("limit") Integer pageSize){
     List<emp> list = empService.selectAll();
     PageHelper.startPage(pageNum,pageSize);
     List<emp> lists = empService.selectAll();
     PageInfo<emp> pageInfo = new PageInfo<>(lists);
     Map<String,Object> map = new HashMap<String,Object>();
     map.put("code",0);
     map.put("msg","");
     map.put("count",list.size());
     map.put("data",pageInfo.getList());
     return map;
 }
    @RequestMapping("/selectByIf")
    public String selectByIf(Model model,emp emp,
    @RequestParam(defaultValue = "1",value = "pageNum") Integer pageNum){
        PageHelper.startPage(pageNum,5);
        List<emp> list = empService.selectByIf(emp);
        PageInfo<emp> pageInfo = new PageInfo<emp>(list);
        model.addAttribute("pageInfo",pageInfo);
        return "redirect:/emp/select";
    }



    @RequestMapping("/selectdept")
    public String selectdept(Model m) {
        List<dept> list = empService.selectdept();
        m.addAttribute("dept",list);
        return "./emp/emp-add.html";
    }

    @RequestMapping("/add")
    public String add(Model model,emp emp) {

        empService.add(emp);

        return "redirect:/emp/select";
    }
    @RequestMapping("/del")
    @ResponseBody
    public String del(Model model,Integer emp_id) {
        empService.del(emp_id);
        return "redirect:/emp/select";
    }
    @RequestMapping("/upd")
    public String upd(Model model,emp emp) {
        empService.upd(emp);
        return "redirect:/emp/select";
    }
    @RequestMapping("/selectById")
    public String selectById(Model model,Integer emp_id) {
     emp emp = empService.selectById(emp_id);
        List<dept> list = empService.selectdept();
        model.addAttribute("dept",list);
       model.addAttribute("emp",emp);
        return "./emp/emp-edit.html";
    }




    @RequestMapping("/glselectByIf")
    public String glselectByIf(Model model,dept dept,
       @RequestParam(defaultValue = "1",value = "pageNum") Integer pageNum){
        PageHelper.startPage(pageNum,5);
        List<dept> list = empService.glselectByIf(dept);
        PageInfo<dept> pageInfo = new PageInfo<dept>(list);
        model.addAttribute("pageInfo",pageInfo);
        return "./dept/dept-list.html";
    }



    @RequestMapping("/glselect")
    public String glselect(Model m,
        @RequestParam(defaultValue = "1",value = "pageNum") Integer pageNum){
            PageHelper.startPage(pageNum,5);
            List<dept> list = empService.selectdept();
            PageInfo<dept> pageInfo = new PageInfo<dept>(list);
           m.addAttribute("pageInfo",pageInfo);

//        List<dept> list = empService.selectdept();
//        m.addAttribute("dept",list);
        return "./dept/dept-list.html";
    }

    @RequestMapping("/gladd")
    public String gladd(Model m,dept dept) {
        empService.gladd(dept);
        return "redirect:/emp/glselect";
    }
    @RequestMapping("/glupd")
    public String glupd(Model m,dept dept) {
        empService.glupd(dept);
        return "redirect:/emp/glselect";
    }
    @RequestMapping("/gldel")
    public String gldel(Model model,Integer dept_id) {
        empService.gldel(dept_id);
        return "redirect:/emp/glselect";
    }
    @RequestMapping("/glselectById")
    public String glselectById(Model model,Integer dept_id) {
         model.addAttribute("dept", empService.glselectById(dept_id));
        return "./dept/dept-edit.html";
    }
    @RequestMapping("/cbatchDel")
    @ResponseBody
    public JsonMessage cbatchDel(@RequestParam("id[]") Integer[] id) throws IOException {
        empService.cbatchDel(id);
        return new JsonMessage(200,null);
    }
    @RequestMapping("/batchDel")
    @ResponseBody
    public JsonMessage batchDel(@RequestParam("id[]") Integer[] id) throws IOException {
        empService.batchDel(id);
        return new JsonMessage(200,null);
    }
    @RequestMapping("/goadd")
    public String goadd(Model model,emp emp) {
        List<dept> list = empService.selectdept();
        model.addAttribute("dept",list);
        return "./emp/piliang.html";
    }
    @RequestMapping("/addpiliang")
    @ResponseBody
        public String addpiliang(@RequestBody List<emp> emps) {
        empService.addpiliang(emps);
      return "redirect:/emp/select";
    }
    /**
     * 下载老师资料 导出 excel 使用我们的模板导出
     *
     */
    @RequestMapping("/excel_down")
    public String excel_down(HttpServletResponse response, HttpServletRequest request)
            throws Exception {
//本地存放模板 模板在
        String webPath = "E:\\O\\yomi\\src\\main\\resources";
//查询老师信息
        List<emp> list = empService.selectAll();
//Excel模板存放的位置
        Workbook wb = fillExcelDataWithTemplate(list, webPath + "/static/aaa/client_down_model.xls");
        ResponseUtil.export(response, wb, "员工.xls");
        return null;
    }

    /**
     * @param templateFileUrl excel模板的路径
     * @return
     */
    public static Workbook fillExcelDataWithTemplate (List <emp> list, String templateFileUrl){
        Workbook wb = null;
        try {
            POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(templateFileUrl));
            wb = new HSSFWorkbook(fs);
// 取得 模板的 第一个sheet 页
            Sheet sheet = wb.getSheetAt(0);
// 拿到sheet页有 多少列
            int cellNums = sheet.getRow(0).getLastCellNum();
// 从第2行 开搞 下标1 就是第2行
            int rowIndex = 1;
            Row row;
            for (emp emp : list) {
                row = sheet.createRow(rowIndex);
                rowIndex++;
//从零开始一行一行传数值
                row.createCell(0).setCellValue(emp.getEmp_id());
                row.createCell(1).setCellValue(emp.getEmp_name());
                row.createCell(2).setCellValue(emp.getEmp_sex());
                row.createCell(3).setCellValue(emp.getEmp_phone());
                row.createCell(4).setCellValue(emp.getDept_id());
                row.createCell(5).setCellValue(emp.getDept().getDept_name());

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return wb;
    }

}
