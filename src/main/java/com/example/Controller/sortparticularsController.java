package com.example.Controller;


import com.example.Service.SupplierService;
import com.example.Service.commodityService;
import com.example.Service.SortparticularsService;
import com.example.model.commodity;
import com.example.model.sortparticulars;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/sortparticulars")
@Slf4j
public class sortparticularsController {
    @Autowired
    commodityService commodityService;
    @Autowired
    SupplierService supplierService;
      @Autowired
      SortparticularsService sortparticularsService;
 @RequestMapping("/selectAll")
    public  String selectAll(Model model){
     List<sortparticulars> list = sortparticularsService.selectAll();
     model.addAttribute("sortparticulars",list);
     return "./sortparticulars/sortparticulars-list.html";
 }
}
