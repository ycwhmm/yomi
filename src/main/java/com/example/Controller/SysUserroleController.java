package com.example.Controller;

import com.example.Service.SysUserRoleService;
import com.example.Service.SysrrService;
import com.example.model.JsonMessage;
import com.example.model.SysRole;
import com.example.model.SysUser;
import com.example.model.SysUserRole;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/user")
public class SysUserroleController {
    @Autowired
    SysUserRoleService sysUserRoleService;
    @Autowired
    com.example.Service.SysrrService SysrrService;

    @RequestMapping("selectAll")
    public String SysRolerightdelrrs(Model model){

        model.addAttribute("sysuser",sysUserRoleService.selectUser());
        model.addAttribute("sysrole",SysrrService.selectAllSysRole());
        return "./sysuserrole/sysuserrole-add.html";
    }
    @RequestMapping("adduserrole")
    public String insertuserrole(Model model, SysUserRole sysUserRole){
        sysUserRoleService.insertuserrole(sysUserRole);
        return "redirect:/sysrr/selectAlluser";
    }
    @RequestMapping("addrole")
    public String addrole(Model model, SysRole sysRole){
        sysUserRoleService.addrole(sysRole);
        return "redirect:/sysrr/selectAlluser";
    }

//    @RequestMapping("selectAlluser")
//    public String selectAlluser(Model model){
//       model.addAttribute("select",sysUserRoleService.selectAlluser());
//        return "redirect:/sysrr/selectAlluser";
//    }

    @RequestMapping("/selectAlluser")
    public String select(Model model){
//        List<dept> list = empService.selectdept();
//        model.addAttribute("dept",list);
        return "./sysuser/sysuser-lists.html";
    }
    @RequestMapping("/selectAllusers")
    @ResponseBody
    public Map<String,Object> selectAll(Model model,
            @RequestParam("page")Integer pageNum,
            @RequestParam("limit") Integer pageSize){
        List<SysUser> list = sysUserRoleService.selectAlluser();
        PageHelper.startPage(pageNum,pageSize);
        List<SysUser> lists = sysUserRoleService.selectAlluser();
        PageInfo<SysUser> pageInfo = new PageInfo<SysUser>(lists);
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("code",0);
        map.put("msg","");
        map.put("count",list.size());
        map.put("data",pageInfo.getList());
        return map;
    }
    @RequestMapping("/update")
    public void update(Model model, @RequestParam("user_statusid")Integer user_statusid,@RequestParam("user_name")String user_name){
     SysUser sysUser = new SysUser();
     sysUser.setUser_name(user_name);
     sysUser.setUser_statusid(user_statusid);
        sysUserRoleService.update(sysUser);
    }
    @RequestMapping("/batchDel")
    @ResponseBody
    public JsonMessage batchDel(Model model, @RequestParam("id[]")Integer[] user_id){
        sysUserRoleService.batchDel(user_id);
        return new JsonMessage(200,null);
    }
}
