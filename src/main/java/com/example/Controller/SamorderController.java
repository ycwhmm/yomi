package com.example.Controller;

import com.example.Service.SamorderService;
import com.example.Service.empService;
import com.example.Service.needService;
import com.example.model.*;
import com.example.util.BaseController;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RequestMapping("/samorder")
@Component
public class SamorderController extends BaseController {
    @Autowired
    SamorderService samorderService;
    @Autowired
    com.example.Service.StatuslService statuslService;
    @Autowired
    empService empService;
    @Autowired
    com.example.Service.needService needService;
    /*@RequestMapping("/selectAll")
    public String selectAll(Model m,Samorder samorder){
        List<Samorder> list = samorderService.selectAll(samorder);
        List<statusl> status1s = statuslService.selectStatus();
        m.addAttribute("status",status1s);
        List<distributor> distributors = statuslService.selectDis();
        m.addAttribute("dis",distributors);
        m.addAttribute("samorder",list);
        return "./samorder/samorder-list.html";
    }*/
    @RequestMapping("/delete")
    public String delete(int orderId){
        samorderService.delete(orderId);
        return "redirect:/samorder/page";
    }
    @RequestMapping("/goAdd")
    public String goAdd(Model m,need need){
        List<statusl> status1s = statuslService.selectStatus();
        m.addAttribute("status",status1s);
        List<need> needs = needService.selectAll(need);
        m.addAttribute("need",needs);
        List<Distributor> distributors = statuslService.selectDis();
        m.addAttribute("dis",distributors);
        List<emp> emps = empService.selectAll();
        m.addAttribute("emp",emps);
        return "./samorder/samorder-add.html";
    }
    @RequestMapping("/insert")
    public String insert(Samorder samorder){


        samorderService.add(samorder);
        return "redirect:/samorder/page";
    }
    @RequestMapping("/goEdit")
    public String goEdit(Model m,int orderId,need need){
        List<statusl> status1s = statuslService.selectStatus();
        m.addAttribute("status",status1s);
        List<need> needs = needService.selectAll(need);
        m.addAttribute("need",needs);
        List<Distributor> distributors = statuslService.selectDis();
        m.addAttribute("dis",distributors);
        Samorder samorder = samorderService.selectById(orderId);
        m.addAttribute("sam",samorder);
        List<emp> emps = empService.selectAll();
        m.addAttribute("emp",emps);
        return "./samorder/samorder-edit.html";
    }
    @RequestMapping("/update")
    public String update(Samorder samorder){
        samorderService.update(samorder);
        return "redirect:/samorder/page";
    }
    @RequestMapping("/batchDel")
    public String batchDel(@RequestParam("ids[]") Integer[] ids) {
        samorderService.batchDel(ids);
        return "redirect:/samorder/page";
    }
    @RequestMapping("/page")
    private String queryA(Samorder samorder, HttpServletRequest request, Model m,need need){
        Integer pageNo=request.getParameter("pageNo")==null?1 : Integer.valueOf(request.getParameter("pageNo"));
        PageInfo<Samorder> pageInfo = samorderService.queryList(pageNo,4,samorder);
        m.addAttribute("samorder",pageInfo.getList());
        m.addAttribute("pages",pageInfo.getPages());
        m.addAttribute("pageNo",pageInfo.getPageNum());
        m.addAttribute("pageSize",pageInfo.getPageSize());
        List<statusl> status1s = statuslService.selectStatus();
        m.addAttribute("status",status1s);
        List<Distributor> distributors = statuslService.selectDis();
        m.addAttribute("dis",distributors);
        List<need> needs = needService.selectAll(need);
        m.addAttribute("need",needs);
        return "./samorder/samorder-list.html";
    }
}
