package com.example.Controller;

import com.example.Service.*;
import com.example.model.*;
import com.example.util.BaseController;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RequestMapping("/indent")
@Component
public class purchaseordersController extends BaseController {
    @Autowired
    purchaseordersService purchaseordersService;
    @Autowired
    StatuslService statuslService;
    @Autowired
    commodityService commodityService;
    @Autowired
    Dis_needService dis_needService;
    @Autowired
    empService empService;
    @Autowired
    financeService financeService;
    @Autowired
    logparticularsService logparticularsService;
    /*@RequestMapping("/selectAll")
    public String selectAll(Model m,purchaseorders purchaseorders){
        List<purchaseorders> list = purchaseordersService.selectAll(purchaseorders);
        List<statusl> status1s = statuslService.selectStatus();
        m.addAttribute("status",status1s);
        List<logistics> logistics = statuslService.selectLog();
        m.addAttribute("log",logistics);
        m.addAttribute("order",list);
        return "./sample/order-list.html";
    }*/
    @RequestMapping("/delete")
    public String delete(String ordersId){
        purchaseordersService.delete(ordersId);
        return "redirect:/indent/selectAll";
    }
    @RequestMapping("/goAdd")
    public String goAdd(Model m,Dis_need dis_need){
        List<statusl> status1s = statuslService.selectStatus();
        m.addAttribute("status",status1s);
        List<logistics> logistics = statuslService.selectLog();
        m.addAttribute("log",logistics);
        List<commodity> commodities = commodityService.selectAll();
        m.addAttribute("com",commodities);
        List<Bsupplier> bsuppliers = statuslService.selectBus();
        m.addAttribute("bsu",bsuppliers);
        List<Dis_need> list = dis_needService.SelectAlls();
        m.addAttribute("dis_need", list);
        /*Dis_need dis_need1 = dis_needService.SelectById(dis_need_id);
        m.addAttribute("disneed",dis_need1);*/
        return "./sample/order-add.html";
    }
    @RequestMapping("/goAdds")
    public String goAdds(Model m,String dis_need_id,Dis_need dis_need){
        List<statusl> status1s = statuslService.selectStatus();
        m.addAttribute("status",status1s);
        List<logistics> logistics = statuslService.selectLog();
        m.addAttribute("log",logistics);
        List<commodity> commodities = commodityService.selectAll();
        m.addAttribute("com",commodities);
        List<Bsupplier> bsuppliers = statuslService.selectBus();
        m.addAttribute("bsu",bsuppliers);
        List<Dis_need> list = dis_needService.SelectAll(dis_need);
        m.addAttribute("dis_need", list);
        Dis_need dis_need1 = dis_needService.SelectById(dis_need_id);
        m.addAttribute("disneed",dis_need1);
        List<emp> emps = empService.cgselectAll();
        m.addAttribute("emp",emps);
        return "./sample/order-adds.html";
    }
    @RequestMapping("/goEdit")
    public String goEdit(Model m,String ordersId){
        List<statusl> status1s = statuslService.selectStatus();
        m.addAttribute("status",status1s);
        List<logistics> logistics = statuslService.selectLog();
        m.addAttribute("log",logistics);
        List<commodity> commodities = commodityService.selectAll();
        m.addAttribute("com",commodities);
        List<Bsupplier> bsuppliers = statuslService.selectBus();
        m.addAttribute("bsu",bsuppliers);
        purchaseorders purchaseorders = purchaseordersService.selectById(ordersId);
        m.addAttribute("pur",purchaseorders);
        return "./sample/order-edit.html";
    }
    @RequestMapping("/update")
    public String update(purchaseorders purchaseorders){
        purchaseordersService.update(purchaseorders);
        return "redirect:/indent/selectAll";
    }
    @RequestMapping("/goSee")
    public String goSee(Model m,String ordersId){
        purchaseorders purchaseorders = purchaseordersService.selectById(ordersId);
        m.addAttribute("pur",purchaseorders);
        List<statusl> status1s = statuslService.selectStatus();
        m.addAttribute("status",status1s);
        List<logistics> logistics = statuslService.selectLog();
        m.addAttribute("log",logistics);
        List<commodity> commodities = commodityService.selectAll();
        m.addAttribute("com",commodities);
        List<Bsupplier> bsuppliers = statuslService.selectBus();
        m.addAttribute("bsu",bsuppliers);
        List<emp> emps = empService.selectAll();
        m.addAttribute("emp",emps);
        return "./sample/order-see.html";
    }
    @RequestMapping("/insert")
    public String insert(Model m,purchaseorders purchaseorders,String ordersId,String logparticularsid){
        purchaseordersService.add(purchaseorders);
        Double advance = purchaseorders.getAdvance();
        Double cgnum = financeService.cgselect();
        cgnum += advance;
        financeService.cgupd(cgnum);
        dis_needService.updateStatus((ordersId));
        logparticularsService.wlogupd(logparticularsid);
        Double wlnum = logparticularsService.selectlogmodeprice(logparticularsid);
        financeService.wlupd(wlnum);
        Double zcgnum = financeService.cgselect();
        Double zdjnum = financeService.djselect();
        Double zonge = financeService.zongselect();
        Double zonges = zcgnum+zdjnum+zonge;
        financeService.zongselect(zonges);
        return "redirect:/indent/selectAll";
    }
    //使用异步传递数组对象到controller层会经过特殊处理，做种参数的名称是ids[],需要@RequestParam手动绑定；
    @RequestMapping("/batchDel")
    public String batchDel(@RequestParam("ids[]")String[] ids) {
        purchaseordersService.batchDel(ids);
        return "redirect:/indent/selectAll";
    }
    //分页查询
//    @RequestMapping("/page")
//    private String queryA(purchaseorders purchaseorders, HttpServletRequest request, Model m){
//        Integer pageNo=request.getParameter("pageNo")==null?1 : Integer.valueOf(request.getParameter("pageNo"));
//        PageInfo<purchaseorders> pageInfo = purchaseordersService.queryList(pageNo,4,purchaseorders);
//        m.addAttribute("order",pageInfo.getList());
//        m.addAttribute("pages",pageInfo.getPages());
//        m.addAttribute("pageNo",pageInfo.getPageNum());
//        m.addAttribute("pageSize",pageInfo.getPageSize());
//        List<statusl> status1s = statuslService.selectStatus();
//        m.addAttribute("status",status1s);
//        List<logistics> logistics = statuslService.selectLog();
//        m.addAttribute("log",logistics);
//        return "./sample/order-list.html";
//    }
    @RequestMapping("/selectAll")
    public  String selectAll(Model model,purchaseorders purchaseorders,
      @RequestParam(defaultValue = "1",value = "pageNum") Integer pageNum) {
        PageHelper.startPage(pageNum, 6);
        List<purchaseorders> list = purchaseordersService.queryList(purchaseorders);
        PageInfo<purchaseorders> pageInfo = new PageInfo<purchaseorders>(list);
        model.addAttribute("pageInfo", pageInfo);
    return "./sample/order-list.html";
    }

    @RequestMapping(value = "/addpiliang")
    @ResponseBody
    public void addpiliang(@RequestBody List<purchaseorders> purchaseorders,Double[] logmodeprice,Double[] advance, String[] ordersId, String[] logparticularsid) {
        purchaseordersService.addpiliang(purchaseorders);
        Double cgnum = financeService.cgselect();
        int sum =0;
        for (Double i:advance) {
            sum+=i;
        }
        cgnum += sum;
        financeService.cgupd(cgnum);
        dis_needService.updateStatu(ordersId);
        logparticularsService.wlogupds(logparticularsid);

        /*Double wlnum = logparticularsService.selectlogmodeprices(logparticularsid);
        financeService.wlupd(wlnum);*/

        Double zcgnum = financeService.cgselect();
        Double zdjnum = financeService.djselect();
       // Double zonge = financeService.zongselect();
        Double wu = financeService.wlselect();
        for (Double a:logmodeprice){
            wu+=a;

        }
        financeService.wlupd(wu);
        Double zonges = zcgnum+zdjnum+wu;
        financeService.zongselect(zonges);
        System.out.println(purchaseorders);
    }
}
