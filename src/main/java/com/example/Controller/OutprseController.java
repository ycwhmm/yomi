package com.example.Controller;


import com.example.model.JsonMessage;
import com.example.model.logistics;
import com.example.model.outprse;
import com.example.model.prse;
import com.example.util.BaseController;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.List;

/**
 * Created by Miracle yaochengwei on 2020/12/1 19:59
 */
/* PageHelper.startPage(pageNum,4);
        List<outprse> list = outprseService.selectAll();
        PageInfo<outprse> pageInfo = new PageInfo<outprse>(list);
        model.addAttribute("pageInfo",pageInfo);
        model.addAttribute("outprse",list);
* */
/*
*
       List<outprse> list = outprseService.selectAll();
       model.addAttribute("outprse",list);
 */
@Controller
@RequestMapping("/outprse")
@Slf4j
public class OutprseController extends BaseController {
    @Autowired
    com.example.Service.outprseService outprseService;

    @RequestMapping("/selectAll")
    public  String selectAll(Model model,
                             @RequestParam(defaultValue = "1",value = "pageNum") Integer pageNum){
        PageHelper.startPage(pageNum,8);
        List<outprse> list = outprseService.selectAll();
        PageInfo<outprse> pageInfo = new PageInfo<outprse>(list);
        model.addAttribute("pageInfo",pageInfo);
        model.addAttribute("outprse",list);
        List<logistics> logistics = outprseService.selectlogistics();
        model.addAttribute("logistics",logistics);
        return "outprse/outprse-list";

    }
    @RequestMapping("/selectAll9")
    public  String selectAll9(Model model,
                              @RequestParam(defaultValue = "1",value = "pageNum") Integer pageNum){
        PageHelper.startPage(pageNum,8);
        List<outprse> list = outprseService.selectAll();
        PageInfo<outprse> pageInfo = new PageInfo<outprse>(list);
        model.addAttribute("pageInfo",pageInfo);
        model.addAttribute("outprse",list);
        List<logistics> logistics = outprseService.selectlogistics();
        model.addAttribute("logistics",logistics);
        return "outprse/outprse-edita";
    }
    @RequestMapping("/glselectById1")
    public  String glselectById1(Model model,Integer prseid){
        prse prse = outprseService.glselectById1(prseid);
        model.addAttribute("prse",prse);
        return "prse/prse-edits";
    }
    @RequestMapping("/glselectById2")
    public  String glselectById2(Model model,Integer prseid){
        prse prse = outprseService.glselectById(prseid);
        model.addAttribute("prse",prse);
        return "prse/prse-add";
    }
    @RequestMapping("/selectprse")
    public String selectprse(Model m){
        List<prse> prses = outprseService.selectprse();
        m.addAttribute("prse",prses);
        // m.addAttribute("prse",list);
        List<logistics> logistics = outprseService.selectlogistics();
        m.addAttribute("logistics",logistics);
        return "/outprse/outprse-add";
    }

    @RequestMapping("/add")
    public String add(Model model, outprse outprse) {

        outprseService.add(outprse);

        return "redirect:/outprse/selectAll";
    }
    @RequestMapping("/del")
    public String del(Model model,Integer outprseid) {
        outprseService.del(outprseid);
        return "redirect:/outprse/selectAll";
    }
    @RequestMapping("/upd")
    public String upd(Model model, outprse outprse) {
        outprseService.upd(outprse);
        return "redirect:/outprse/selectAll";
    }
    @RequestMapping("/selectById")
    public String selectById(Model model,Integer prseid) {
        outprse outprse = outprseService.selectById(prseid);
//        List<prse> list = outprseService.selectprse();
//        model.addAttribute("prse",list);
        model.addAttribute("outprse",outprse);
        List<logistics> logistics = outprseService.selectlogistics();
        model.addAttribute("logistics",logistics);
        return "outprse/outprse-edits";
    }
    //    @RequestMapping("/glselectById")
//    public String glselectById(Model model,Integer prseid) {
//        model.addAttribute("prse", outprseService.glselectById(prseid));
//        return "outprse/outprse-edits";
//    }
    @RequestMapping("/glselectById")
    public String glselectById(Model model,Integer prseid) {
        prse prse = outprseService.glselectById(prseid);
        model.addAttribute("prse",prse);
        return "outprse/outprse-edits";
    }

    @RequestMapping("/selectByIf")
    public String selectByIf(Model model, outprse outprse,
                             @RequestParam(defaultValue = "1",value = "pageNum") Integer pageNum){
        PageHelper.startPage(pageNum,8);
        List<com.example.model.outprse> list = outprseService.selectByIf(outprse);
        PageInfo<com.example.model.outprse> pageInfo = new PageInfo<com.example.model.outprse>(list);
        model.addAttribute("pageInfo",pageInfo);
        model.addAttribute("outprse",list);
        return "outprse/outprse-list";
    }

    @RequestMapping("/glselectByIf")
    public String glselectByIf(Model model, prse prse,
                               @RequestParam(defaultValue = "1",value = "pageNum") Integer pageNum){
        PageHelper.startPage(pageNum,8);
        List<com.example.model.prse> list = outprseService.glselectByIf(prse);
        PageInfo<com.example.model.prse> pageInfo = new PageInfo<com.example.model.prse>(list);
        model.addAttribute("pageInfo",pageInfo);
        model.addAttribute("outprse",list);
        return "prse/prse-list";
    }



    //prse查询全部
    @RequestMapping("/glselect")
    public String glselect(Model m,
                           @RequestParam(defaultValue = "1",value = "pageNum") Integer pageNum){
        PageHelper.startPage(pageNum,8);
        List<prse> list = outprseService.glselect();
        PageInfo<prse> pageInfo = new PageInfo<prse>(list);
        m.addAttribute("pageInfo",pageInfo);
        return "prse/prse-list";
    }
    @RequestMapping("/glselect1")
    public String glselect1(Model m,
                            @RequestParam(defaultValue = "1",value = "pageNum") Integer pageNum){
        PageHelper.startPage(pageNum,8);
        List<prse> list = outprseService.glselect();
        PageInfo<prse> pageInfo = new PageInfo<prse>(list);
        m.addAttribute("pageInfo",pageInfo);
        m.addAttribute("outprse",list);
        return "outprse/outprse-add";
    }
    //添加添加到出货表里面(outprse的添加)
    @RequestMapping("/gladd")
    public String gladd(Model m, prse prse) {
        outprseService.gladd(prse);
        return "redirect:/outprse/selectAll";
    }
    @RequestMapping("/glupd")
    public String glupd(Model m, prse prse) {
        outprseService.glupd(prse);
        return "redirect:/outprse/glselect";
    }
    @RequestMapping("/gldel")
    public String gldel(Model model,Integer prseid) {
        outprseService.gldel(prseid);
        return "redirect:/outprse/glselect";
    }

    @RequestMapping("/prseDel")
    @ResponseBody
    public JsonMessage prseDel(@RequestParam("prseids[]") Integer[] prseids) throws IOException {
        outprseService.prseDel(prseids);
        return new JsonMessage(200,null);
    }
    @RequestMapping("/outprseDel")
    @ResponseBody
    public JsonMessage outprseDel(@RequestParam("outprses[]") Integer[] outprseids) throws IOException {
        outprseService.outprseDel(outprseids);
        return new JsonMessage(200,null);
    }
//    @RequestMapping("/commbatchDel")
//    @ResponseBody
//    public JsonMessage outprseDel(@RequestParam("outprseids[]") Integer[] outprseids) throws IOException {
//        outprseService.outprseDel(outprseids);
//        return new JsonMessage(200,null);
//    }
}
