package com.example.Controller;

import com.example.Service.DistributorService;
import com.example.model.Distributor;
import com.example.model.emp;
import com.example.model.statusl;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/Distributor")
@Slf4j
public class DistributorController {
    @Autowired
    DistributorService distributorService;
//    @RequestMapping("/selectAllDis")
//    public String selectAllDis(Model model, Distributor distributor) {
//        List<Distributor> list = distributorService.SelectAll(distributor);
//        model.addAttribute("dis", list);
//        List<status1>   status1s = distributorService.Selectstatus1();
//        model.addAttribute("status1s",status1s);
//        return "./Distributor/Distributor-list.html";
//    }
    //去新增页面
    @RequestMapping("/goAdd")
    public String goAdd(Distributor distributor,Model model){
        List<statusl>   status1s = distributorService.Selectstatus1();
        model.addAttribute("status1s",status1s);
        return "./Distributor/Distributor-add.html";
    }
    //新增
    @RequestMapping("/DisAdd")
    public String DisAdd(Model model, Distributor distributor) {
        distributorService.addDis(distributor);
        return "redirect:/Distributor/page";
    }
    // 删除
    @RequestMapping("/DisDel")
    public String bsuppDel(Model model, Integer distributorid) {
        distributorService.delDis(distributorid);
        return "redirect:/Distributor/page";
    }
    //批量删除
    @RequestMapping("/batchDel")
    public String batchDel(@RequestParam("ids[]") Integer[] distributorid) {
        distributorService.DelAll(distributorid);
        return "redirect:/Distributor/page";
    }
    //修改
    @RequestMapping("/UpdDis")
    public String UpdDis(Model model, Distributor distributor) {
        distributorService.updDis(distributor);
        return "redirect:/Distributor/page";
    }
    //根据id查询
    @RequestMapping("/selectById")
    public String selectById(Model model, Integer distributorid) {
        Distributor distributor1 = distributorService.SelectByIdDis(distributorid);
        model.addAttribute("dis", distributor1);
        List<statusl>   status1s = distributorService.Selectstatus1();
        model.addAttribute("status1s",status1s);
        return "./Distributor/Distributor-edit.html";
    }
    @RequestMapping("/page")
    private String queryA(Distributor distributor, HttpServletRequest request, Model model){
        Integer pageNo=request.getParameter("pageNo")==null?1 : Integer.valueOf(request.getParameter("pageNo"));
        PageInfo<Distributor> pageInfo = distributorService.queryList(pageNo,3,distributor);
        model.addAttribute("dis",pageInfo.getList());
        model.addAttribute("pages",pageInfo.getPages());
        model.addAttribute("pageNo",pageInfo.getPageNum());
        model.addAttribute("pageSize",pageInfo.getPageSize());
        List<statusl>   status1s = distributorService.Selectstatus1();
        model.addAttribute("status1s",status1s);
        return "./Distributor/Distributor-list.html";
    }
    @RequestMapping("/addpiliang")
    @ResponseBody
    public void addpiliang(@RequestBody List<Distributor> distributors) {
        distributorService.addpiliang(distributors);
        System.out.println(distributors);
    }
    @RequestMapping("/goPiliang")
    public String goPiliang(Distributor distributor,Model model){
        List<statusl>   status1s = distributorService.Selectstatus1();
        model.addAttribute("status1s",status1s);
        return "./Distributor/Distributor-piliang.html";
    }
}
