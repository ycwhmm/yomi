package com.example.Controller;

import com.example.Service.SortparticularsService;
import com.example.Service.logparticularsService;
import com.example.Service.purchaseordersService;
import com.example.model.*;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/Sortp")
@Slf4j
public class SortparticularsDaoController {
    @Autowired
    SortparticularsService sortparticularsService;
    @Autowired
    purchaseordersService purchaseordersService;

    @Autowired
    logparticularsService logparticularsService;
    //查询未分拣全部
    @RequestMapping("selectAll")
    public String selectAll(Model model,@RequestParam(defaultValue = "1",value = "pageNum") Integer pageNum){
        PageHelper.startPage(pageNum,5);//分页
        List<sortparticulars> list = sortparticularsService.selectAll();
        PageInfo<sortparticulars> pageInfo = new PageInfo<sortparticulars>(list);
        model.addAttribute("pageInfo",pageInfo);
        return "./fenjian/fenjian-list.html";
    }
    @RequestMapping("/sortpDel")
    @ResponseBody
    public JsonMessage batchDel(@RequestParam("sparId[]") Integer[] sparId) throws IOException {
        sortparticularsService.batchDel(sparId);
        return new JsonMessage(200,null);
    }
    //根据订单查询
    @RequestMapping("/selectByIf")
    public String selectByIf(Model model ,sortparticulars sortparticulars ){
        List<sortparticulars> list = sortparticularsService.selectByIf(sortparticulars);
        model.addAttribute("Sortp",list);
        return "./fenjian/fenjian-list.html";
    }
    //查看商品单详情
    @RequestMapping("/chakan")
    public String chakan(Model model ,String id ){
        purchaseorders purchaseorders = sortparticularsService.shangpin(id);
        model.addAttribute("pu",purchaseorders);
        return "./fenjian/fenjian-shangpin.html";
    }
    //入库查看
    @RequestMapping("/ruku")
    public String ruku(Model model ,String id){
        purchaseorders purchaseorders = sortparticularsService.shangpin(id);
        model.addAttribute("pu",purchaseorders);
        return "./fenjian/fenjian-shangpin.html";
    }
    //分产库界面
    @RequestMapping("/goadd")
    public String goadd(Model model ,String id){
        sortparticulars sortparticulars = new sortparticulars();
        sortparticulars.setOrdersId(id);
        List<sortparticulars> list1 = sortparticularsService.selectByIf(sortparticulars);
        model.addAttribute("Sortp",list1);
        List<Entrepot> list = sortparticularsService.goadd();
        model.addAttribute("ent",list);
        return "./fenjian/fenjian-wancheng.html";
    }
    //添加到入库
    @RequestMapping("/add")
    public String add(Model model,Integer entrepotid,String ordersId,Integer sparId,String bz1){
        Instorage instorage= new Instorage();
        instorage.setStatusid(1);
        instorage.setOrdertime(new Date());
        instorage.setOrdersId(ordersId);
        instorage.setBz(bz1);
        instorage.setEntrepotid(entrepotid);
        sortparticularsService.add(instorage,sparId);
        return "redirect:/Sortp/selectAll";
    }
    //查看所有商品单
    @RequestMapping("chakanall")
    public String chakanall(Model model ){
        List<purchaseorders> purchaseorders = sortparticularsService.shangpinAll();
        model.addAttribute("pu",purchaseorders);
        return "./fenjian/fenjian-tianjia.html";
    }
    //添加分拣
    @RequestMapping("gochakanadd")
    public String chakanadds(Model model,String id){
        model.addAttribute("s",id);
        List<emp> emps = sortparticularsService.selectemp();
        model.addAttribute("emps",emps);

       purchaseorders  purchaseorders = sortparticularsService.shangpinby(id);
        model.addAttribute("pu",purchaseorders);

        return "./fenjian/goadd.html";
    }

    @RequestMapping("chakanadd")
    public String chakanadd(Model model,sortparticulars sortparticulars,String logparticularsid){
        logparticularsService.ylogupd(logparticularsid);

        sortparticularsService.adds(sortparticulars);
        return "redirect:/Sortp/selectAll";
    }
    @RequestMapping("/selectAll2")
    public String selectAll2(Model model){
        List<sortparticulars> list = sortparticularsService.selectAll2();
        model.addAttribute("Sortp",list);
        return "./fenjian/fenjian-lists.html";
    }
}
