package com.example.Controller;


import com.example.Service.SupplierService;
import com.example.Service.logisticsService;
import com.example.model.commodity;
import com.example.model.logistics;
import com.example.model.logmode;
import com.example.model.supplier;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/logistics")
@Slf4j
public class logisticsController {
    @Autowired
    logisticsService logisticsService;
    @Autowired
    SupplierService supplierService;

 @RequestMapping("/selectAll")
    public  String selectAll(Model model){
     List<logistics> list = logisticsService.selectAll();
     model.addAttribute("logistics",list);
     return "./logistics/logistics-list.html";
 }
    @RequestMapping("/selectlogmode")
    public  String selectlogmode(Model model){
        List<logmode> list = logisticsService.selectlogmode();
        model.addAttribute("logmode",list);
        return "./logistics/logistics-add.html";
    }

    @RequestMapping("/logadd")
    public  String logadd(Model model, logistics logistics){
        logisticsService.logadd(logistics);
        return "redirect:/logistics/selectAll";
    }
    @RequestMapping("/logdel")
    public  String logdel(Model model,Integer logId){
        logisticsService.logdel(logId);
        return "redirect:/logistics/selectAll";
    }

    @RequestMapping("/logupd")
    public  String logupd(Model model,logistics logistics){
        logisticsService.logupd(logistics);
        return "redirect:/logistics/selectAll";
    }
    @RequestMapping("/selectById")
    public  String selectById(Model model,Integer logId){
        logistics logistics = logisticsService.selectById(logId);
        model.addAttribute("logistics",logistics);

        List<logmode> list = logisticsService.selectlogmode();
        model.addAttribute("logmode",list);

        return "./logistics/logistics-edit.html";
    }
    @RequestMapping("/logmodeselectAll")
    public  String logmodeselectAll(Model model){
        List<logmode> list = logisticsService.selectlogmode();
        model.addAttribute("logmode",list);
        return "./logmode/logmode-list.html";
    }

    @RequestMapping("/logmodeadd")
    public  String logmodeadd(Model model, logmode logmode){
        logisticsService.logmodeadd(logmode);
        return "./logmode/logmode-add.html";
    }
    @RequestMapping("/logmodedel")
    public  String logmodedel(Model model,Integer logmodeid){
        logisticsService.logmodedel(logmodeid);
        return "redirect:/logistics/logmodeselectAll";
    }

    @RequestMapping("/logmodeupd")
    public  String logmodeupd(Model model,logmode logmode){
        logisticsService.logmodeupd(logmode);
        return "redirect:/logistics/selectlogmode";
    }
    @RequestMapping("/logmodeselectById")
    public  String logmodeselectById(Model model,Integer logmodeid){
        logmode logmode = logisticsService.logmodeselectById(logmodeid);
        model.addAttribute("logmode",logmode);
        return "./logmode/logmode-edit.html";
    }

}
