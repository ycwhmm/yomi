package com.example.Controller;


import com.example.Service.SupplierService;
import com.example.Service.commodityService;
import com.example.model.JsonMessage;
import com.example.model.commodity;
import com.example.model.supplier;
import com.example.util.FilesUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/commodity")
@Slf4j
public class commodityController {
    @Autowired
    commodityService commodityService;
    @Autowired
    SupplierService supplierService;
@GetMapping
@PostMapping

 @RequestMapping("/selectAll")
    public  String selectAll(Model model,
      //将请求参数绑定到你控制器的方法参数上（接收普通参数的注解）
      @RequestParam(defaultValue = "1",value = "pageNum") Integer pageNum){
     PageHelper.startPage(pageNum,7);
     List<commodity> list = commodityService.selectAll();
     PageInfo<commodity> pageInfo = new PageInfo<commodity>(list);
     model.addAttribute("pageInfo",pageInfo);
     return "./commodity/commodity-list.html";
 }

    @RequestMapping("/selectAllsup")
    public  String selectAllsup(Model model){
        List<supplier> list = supplierService.selectAll();
        model.addAttribute("supplier",list);
        return "./commodity/commodity-add.html";
    }

    @RequestMapping("/commadd")
    public  String commadd(Model model, commodity commodity,  HttpServletRequest request) throws IOException{
        MultipartFile commodityimg =   commodity.getImg();
        String commodityimgstr = FilesUtil.filesUp(commodityimg);
        commodity.setCommodityimgstr(commodityimgstr);
        commodityService.commadd(commodity);
        return "redirect:/commodity/selectAll";
    }
    @RequestMapping("/commdel")
    public  String commdel(Model model,Integer commodityid){
        commodityService.commdel(commodityid);
        return "redirect:/commodity/selectAll";
    }

    @RequestMapping("/commupd")
    public  String commupd(Model model,commodity commodity){
        MultipartFile commodityimg =   commodity.getImg();
        String commodityimgstr = FilesUtil.filesUp(commodityimg);
        commodity.setCommodityimgstr(commodityimgstr);
        commodityService.commupd(commodity);
        return "redirect:/commodity/selectAll";
    }
    @RequestMapping("/status")
    public  String status(Model model){
        commodityService.selectstatus1();
        return "./commodity/commodity-add.html";
    }

    @RequestMapping("/selectById")
    public  String selectById(Model model,Integer commodityid){
        commodity commodity = commodityService.selectById(commodityid);
        model.addAttribute("commodity",commodity);
        List<supplier> list = supplierService.selectAll();
        model.addAttribute("supplier",list);
        return "./commodity/commodity-edit.html";
    }

    @RequestMapping("/commbatchDel")
    @ResponseBody
    public JsonMessage batchDel(@RequestParam("commodityids[]") Integer[] commodityids) throws IOException {
        commodityService.commbatchDel(commodityids);
        return new JsonMessage(200,null);
    }


}
