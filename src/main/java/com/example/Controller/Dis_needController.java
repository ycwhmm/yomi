package com.example.Controller;

import com.example.Service.Dis_needService;
import com.example.Service.financeService;
import com.example.Service.logparticularsService;
import com.example.model.*;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/dis_need")
@Slf4j
public class Dis_needController {
    @Autowired
    Dis_needService dis_needService;
    @Autowired
    com.example.Service.financeService financeService;
    @Autowired
    com.example.Service.logparticularsService logparticularsService;
    // 查询所有
//    @RequestMapping("/selectAllDis_need")
//    public String selectAllDis_need(Model model, Dis_need dis_need) {
//        List<Dis_need> list = dis_needService.SelectAll(dis_need);
//        model.addAttribute("dis_need", list);
//        List<Bcategory> bcategories=dis_needService.SelectBca();
//        model.addAttribute("bcategory", bcategories);
//        List<status1>   status1s = dis_needService.Selectstatus1();
//        model.addAttribute("status1s",status1s);
//        List<Bsupplier> bsuppliers = dis_needService.SelectBsu();
//        model.addAttribute("bsuppliers",bsuppliers);
//        List<logistics> logistics = dis_needService.SelectLog();
//        model.addAttribute("logistics",logistics);
//        List<logmode> logmodes = dis_needService.SelectLogMode();
//        model.addAttribute("logmodes",logmodes);
//        List<commodity> commodities = dis_needService.SelectCom();
//        model.addAttribute("commodities",commodities);
//        List<Distributor> distributors = dis_needService.SelectDis();
//        model.addAttribute("distributors",distributors);
//        List<supplier> suppliers = dis_needService.SelectSupp();
//        model.addAttribute("suppliers",suppliers);
//        return "./Dis_need/Dis_need-list.html";
//    }
    //去新增页面
    @RequestMapping("/goAdd")
    public String goAdd(Dis_need dis_need,Model model){
        List<Bcategory> bcategories=dis_needService.SelectBca();
        model.addAttribute("bcategory", bcategories);
        List<statusl>   status1s = dis_needService.Selectstatus1();
        model.addAttribute("status1s",status1s);
        List<Bsupplier> bsuppliers = dis_needService.SelectBsu();
        model.addAttribute("bsuppliers",bsuppliers);
        List<logistics> logistics = dis_needService.SelectLog();
        model.addAttribute("logistics",logistics);
        List<logmode> logmodes = dis_needService.SelectLogMode();
        model.addAttribute("logmodes",logmodes);
        List<commodity> commodities = dis_needService.SelectCom();
        model.addAttribute("commodities",commodities);
        List<Distributor> distributors = dis_needService.SelectDis();
        model.addAttribute("distributors",distributors);
        List<supplier> suppliers = dis_needService.SelectSupp();
        model.addAttribute("suppliers",suppliers);
        List<need> needs = dis_needService.SelectNeed();
        model.addAttribute("needs",needs);
        return "./Dis_need/Dis_need-add.html";
    }
    //新增
    @RequestMapping("/Dis_needAdd")
    public String Dis_needAdd(Model model, Dis_need dis_need,Integer logId,String logparticularsid) {
        // 添加 物流 详细单
        logparticulars logparticulars = new logparticulars();
        logparticulars.setLogparticularsid(logparticularsid);
        logparticulars.setLogId(logId);
        logparticularsService.add(logparticulars);
        Double advance = dis_need.getAdvance();
        Double num = financeService.djselect();
        num += advance;
        financeService.djupd(num);
        dis_needService.AddDis_need(dis_need);
        return "redirect:/dis_need/page";
    }
    // 删除
    @RequestMapping("/Dis_neddDel")
    public String Dis_neddDel(Model model, String dis_need_id) {
        dis_needService.DelDis_need(dis_need_id);
        return "redirect:/dis_need/page";
    }
    //批量删除
    @RequestMapping("/batchDel")
    public String batchDel(@RequestParam("ids[]") String[] dis_need_ids) {
        dis_needService.DelAll(dis_need_ids);
        return "redirect:/dis_need/page";
    }
    //修改
    @RequestMapping("/Dis_needUpd")
    public String Dis_needUpd(Model model, Dis_need dis_need) {
        dis_needService.UpdDis_need(dis_need);
        return "redirect:/dis_need/page";
    }
    //根据id查询
    @RequestMapping("/selectById")
    public String selectById(Model model, String dis_need_id) {
        Dis_need dis_need = dis_needService.SelectById(dis_need_id);
        model.addAttribute("dis_need", dis_need);
        List<Bcategory> bcategories=dis_needService.SelectBca();
        model.addAttribute("bcategory", bcategories);
        List<statusl>   status1s = dis_needService.Selectstatus1();
        model.addAttribute("status1s",status1s);
        List<Bsupplier> bsuppliers = dis_needService.SelectBsu();
        model.addAttribute("bsuppliers",bsuppliers);
        List<logistics> logistics = dis_needService.SelectLog();
        model.addAttribute("logistics",logistics);
        List<logmode> logmodes = dis_needService.SelectLogMode();
        model.addAttribute("logmodes",logmodes);
        List<commodity> commodities = dis_needService.SelectCom();
        model.addAttribute("commodities",commodities);
        List<Distributor> distributors = dis_needService.SelectDis();
        model.addAttribute("distributors",distributors);
        List<supplier> suppliers = dis_needService.SelectSupp();
        model.addAttribute("suppliers",suppliers);
        List<need> needs = dis_needService.SelectNeed();
        model.addAttribute("needs",needs);
        return "./Dis_need/Dis_need-edit.html";
    }

    @RequestMapping("/selectDetails")
    public String selectDetails(Model model, String dis_need_id) {
        Dis_need dis_need = dis_needService.SelectById(dis_need_id);
        model.addAttribute("dis_need", dis_need);
        List<Bcategory> bcategories=dis_needService.SelectBca();
        model.addAttribute("bcategory", bcategories);
        List<statusl>   status1s = dis_needService.Selectstatus1();
        model.addAttribute("status1s",status1s);
        List<Bsupplier> bsuppliers = dis_needService.SelectBsu();
        model.addAttribute("bsuppliers",bsuppliers);
        List<logistics> logistics = dis_needService.SelectLog();
        model.addAttribute("logistics",logistics);
        List<logmode> logmodes = dis_needService.SelectLogMode();
        model.addAttribute("logmodes",logmodes);
        List<commodity> commodities = dis_needService.SelectCom();
        model.addAttribute("commodities",commodities);
        List<Distributor> distributors = dis_needService.SelectDis();
        model.addAttribute("distributors",distributors);
        List<supplier> suppliers = dis_needService.SelectSupp();
        model.addAttribute("suppliers",suppliers);
        return "./Dis_need/Dis_need-details.html";
    }
    //分页查询
    @RequestMapping("/page")
    private String queryA(Dis_need dis_need, HttpServletRequest request, Model model){
        Integer pageNo=request.getParameter("pageNo")==null?1 : Integer.valueOf(request.getParameter("pageNo"));
        PageInfo<Dis_need> pageInfo = dis_needService.queryList(pageNo,3,dis_need);
        model.addAttribute("dis_need",pageInfo.getList());
        model.addAttribute("pages",pageInfo.getPages());
        model.addAttribute("pageNo",pageInfo.getPageNum());
        model.addAttribute("pageSize",pageInfo.getPageSize());
        List<Bcategory> bcategories=dis_needService.SelectBca();
        model.addAttribute("bcategory", bcategories);
        List<statusl>   status1s = dis_needService.Selectstatus1();
        model.addAttribute("status1s",status1s);
        List<Bsupplier> bsuppliers = dis_needService.SelectBsu();
        model.addAttribute("bsuppliers",bsuppliers);
        List<logistics> logistics = dis_needService.SelectLog();
        model.addAttribute("logistics",logistics);
        List<logmode> logmodes = dis_needService.SelectLogMode();
        model.addAttribute("logmodes",logmodes);
        List<commodity> commodities = dis_needService.SelectCom();
        model.addAttribute("commodities",commodities);
        List<Distributor> distributors = dis_needService.SelectDis();
        model.addAttribute("distributors",distributors);
        List<supplier> suppliers = dis_needService.SelectSupp();
        model.addAttribute("suppliers",suppliers);
        List<need> needs = dis_needService.SelectNeed();
        model.addAttribute("needs",needs);
        return "./Dis_need/Dis_need-list.html";
    }
    @RequestMapping(value = "/addpiliang")
    @ResponseBody
    public void addpiliang(@RequestBody List<Dis_need> dis_needs,
                           String[] logparticularsid,Integer[] logId,Double[] advance) {
           dis_needService.addpiliang(dis_needs);

        System.out.println(logparticularsid);
        System.out.println(logId);

//        Map<String, Object> map = new HashMap<>();
//        map.put("logparticularsid", logparticularsid);
//        map.put("logId", logId);
         Integer n1 = logId[0];
        logparticularsService.adds(logparticularsid,n1);

        Double num = financeService.djselect();
        for (Double a:advance){
            num+=a;
        }
        financeService.djupd(num);
        System.out.println(dis_needs);
      }
    @RequestMapping("/goPiliang")
    public String goPiliang(Dis_need dis_need,Model model){
        List<Bcategory> bcategories=dis_needService.SelectBca();
        model.addAttribute("bcategory", bcategories);
        List<statusl>   status1s = dis_needService.Selectstatus1();
        model.addAttribute("status1s",status1s);
        List<Bsupplier> bsuppliers = dis_needService.SelectBsu();
        model.addAttribute("bsuppliers",bsuppliers);
        List<logistics> logistics = dis_needService.SelectLog();
        model.addAttribute("logistics",logistics);
        List<logmode> logmodes = dis_needService.SelectLogMode();
        model.addAttribute("logmodes",logmodes);
        List<commodity> commodities = dis_needService.SelectCom();
        model.addAttribute("commodities",commodities);
        List<Distributor> distributors = dis_needService.SelectDis();
        model.addAttribute("distributors",distributors);
        List<supplier> suppliers = dis_needService.SelectSupp();
        model.addAttribute("suppliers",suppliers);
        List<need> needs = dis_needService.SelectNeed();
        model.addAttribute("needs",needs);
        return "./Dis_need/Dis_need-piliang.html";
    }
}
