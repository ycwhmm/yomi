package com.example.Controller;

import com.example.Service.logparticularsService;
import com.example.Service.logisticsService;
import com.example.model.JsonMessage;
import com.example.model.commodity;
import com.example.model.logparticulars;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/logparticulars")
public class logparticularsController {

       @Autowired
    logparticularsService logparticularsService;

    @RequestMapping("/selectAll")
    public  String selectAll(Model model ,
        @RequestParam(defaultValue = "1",value = "pageNum") Integer pageNum){
        PageHelper.startPage(pageNum,8);
        List<logparticulars> list = logparticularsService.selectAll();
        PageInfo<logparticulars> pageInfo = new PageInfo<logparticulars>(list);
        model.addAttribute("pageInfo",pageInfo);

        return "./logparticulars/logparticulars-list.html";
    }
    @RequestMapping("/batchDel")
    @ResponseBody
    public JsonMessage batchDel(@RequestParam("id[]") String[] id) throws IOException {
        logparticularsService.batchDel(id);
        return new JsonMessage(200,null);
    }

}
