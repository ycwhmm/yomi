package com.example.Controller;

import com.example.Service.BpingService;
import com.example.model.JsonMessage;
import com.example.model.bping;
import com.example.util.BaseController;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.List;

//组里的项目123131441414141444
/**
 * Created by Miracle yaochengwei on 2020/11/24 8:53
 */

/**
 * Restcontroller =@ResponseBody-+@controller
 */
@Controller
@RequestMapping("/bping")
public class BpingController extends BaseController {
    @Autowired
    BpingService bpingService;

    @RequestMapping("/selectAll")
    public  String selectAll(Model model,
//        List<bping> list = bpingService.selectAll();
//        model.addAttribute("bping",list);
                             @RequestParam(defaultValue = "1",value = "pageNum") Integer pageNum){
        PageHelper.startPage(pageNum,6);
        List<bping> list = bpingService.selectAll();
        PageInfo<bping> pageInfo = new PageInfo<bping>(list);
        model.addAttribute("pageInfo",pageInfo);
        model.addAttribute("bping",list);
        return "bping/bping-list";
    }
    @RequestMapping("/selectAll1")
    public  String selectAll1(Model model,
//        List<bping> list = bpingService.selectAll();
//        model.addAttribute("bping",list);
                              @RequestParam(defaultValue = "1",value = "pageNum") Integer pageNum){
        PageHelper.startPage(pageNum,6);
        List<bping> list = bpingService.selectAll();
        PageInfo<bping> pageInfo = new PageInfo<bping>(list);
        model.addAttribute("pageInfo",pageInfo);
        model.addAttribute("bping",list);
        return "prse/prse-add";
    }

//    @RequestMapping("/selectAll2")
//    public  String selectAll2(Model model,
////        List<bping> list = bpingService.selectAll();
////        model.addAttribute("bping",list);
//                              @RequestParam(defaultValue = "1",value = "pageNum") Integer pageNum){
//        PageHelper.startPage(pageNum,6);
//        List<bping> list = bpingService.selectAll();
//        PageInfo<bping> pageInfo = new PageInfo<bping>(list);
//        model.addAttribute("pageInfo",pageInfo);
//        model.addAttribute("bping",list);
//        return "outprse/outprse-add";
//    }

    //    @RequestMapping("/selectAAll")
//    public  String selectAll(Model model){
//        List<Outstorage> list = bpingService.selectAAll();
//        model.addAttribute("Outstorage",list);
//        return "./bping/bping-add.html";
//    }
    @RequestMapping("/selectByIf")
    public String selectByIf(Model model, bping bping,
                             @RequestParam(defaultValue = "1",value = "pageNum") Integer pageNum
    ){
        PageHelper.startPage(pageNum,6);
        List<com.example.model.bping> list = bpingService.selectByIf(bping);
        PageInfo<com.example.model.bping> pageInfo = new PageInfo<com.example.model.bping>(list);
        model.addAttribute("pageInfo",pageInfo);
        //model.addAttribute("pageInfo", bpingService.selectByIf(bping));
        //model
        model.addAttribute("bping",list);
        return "./bping/bping-list.html";
    }

    @RequestMapping("/bpingadd")
    public  String bpingadd(Model model, bping bping){
        bpingService.bpingadd(bping);

        return "redirect:/outprse/glselect";


    }
    @RequestMapping("/bpingdel")
    public  String bpingdel(Model model,Integer bpingid){
        bpingService.bpingdel(bpingid);
        return "redirect:/bping/selectAll";
    }
    @RequestMapping("/bpingupd")
    public  String bpingupd(Model model, bping bping){
        bpingService.bpingupd(bping);
        return "redirect:/bping/selectAll";
    }
    @RequestMapping("/selectById")
    public  String selectById(Model model,Integer bpingid){
        bping bping = bpingService.selectById(bpingid);
        model.addAttribute("bping",bping);
        return "prse/prse-edits";
    }

//    @RequestMapping("/selectById1")
//    public  String selectById1(Model model,Integer bpingid){
//        bping bping = bpingService.selectById(bpingid);
//        model.addAttribute("bping",bping);
//        return "prse/prse-edits";
//    }
    @RequestMapping("/status")
    public  String status(Model model){
        bpingService.selectstatusl();
        return "bping/bping-add";
    }
    @RequestMapping("/bpingDel")
    @ResponseBody
    public JsonMessage bpingDel(@RequestParam("bpingids[]") Integer[] bpingids) throws IOException {
        bpingService.bpingDel(bpingids);
        return new JsonMessage(200,null);
    }

}

