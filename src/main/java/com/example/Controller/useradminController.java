package com.example.Controller;


import com.example.Service.SysRightService;
import com.example.Service.SysrrService;
import com.example.model.SysRight;
import com.example.model.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class useradminController {
    @Autowired
    SysRightService sysRightService;
    @Autowired
    SysrrService sysrrService;

    @RequestMapping("/useradminlogin")
    public String showMenu(Model model) throws Exception{
        //获取security中的用户信息
        User principal = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        //用户名
        String username = principal.getUsername();
        //根据用户名查找对应的权限
        List<SysRight> oneRights = sysRightService.selectByUserName(username);

        model.addAttribute("username",username);
        //放入model，传递到页面
        model.addAttribute("oneRights",oneRights);
        return "index";
    }
    @RequestMapping("/logout")
    public String logout(Model model) throws Exception{
        return "./deng/deng.html";
    }
    @RequestMapping("/register")
    public String registerhtml(Model model) throws Exception{
        return "./deng/deng-register.html";
    }

    @RequestMapping("/useradminregister")
    public String register(Model model, SysUser sysUser) throws Exception {

            sysrrService.Sysuseradd(sysUser);
            return "./deng/deng.html";


    }

}
