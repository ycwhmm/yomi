package com.example.Controller;


import com.example.Service.empService;
import com.example.Service.needService;
import com.example.Service.StatuslService;
import com.example.model.*;
import com.example.util.BaseController;
import com.example.util.FilesUtil;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Component
@RequestMapping("/need")
public class needController extends BaseController {
    @Autowired
    needService needService;
    @Autowired
    StatuslService statuslService;
   /* @RequestMapping("/selectAll")
    public String selectAll(Model m, need need){
        List<need> needs = needService.selectAll(need);
        m.addAttribute("need",needs);
        List<statusl> status1s = statuslService.selectStatus();
        m.addAttribute("status",status1s);
        List<distributor> distributors = statuslService.selectDis();
        m.addAttribute("dis",distributors);
        return "./need/need-list.html";
    }*/
    @RequestMapping("/delete")
    public String delete(int needId){
        needService.delete(needId);
        return "redirect:/need/page";
    }
    @RequestMapping("/goAdd")
    public String goAdd(Model m){
        List<statusl> status1s = statuslService.selectStatus();
        m.addAttribute("status",status1s);
        m.addAttribute("dis",needService.bsselect());
        List<emp> emps = statuslService.selectEmp();
        m.addAttribute("emp",emps);
        return "./need/need-add.html";
    }
    @RequestMapping("/goEdit")
    public String goEdit(Model m,int needId){
        need need = needService.selectById(needId);
        m.addAttribute("need",need);
        List<statusl> status1s = statuslService.selectStatus();
        m.addAttribute("status",status1s);
        m.addAttribute("dis",needService.bsselect());
        List<emp> emps = statuslService.selectEmp();
        m.addAttribute("emp",emps);
        return "./need/need-edit.html";
    }
    @RequestMapping(value = "/update",method = RequestMethod.POST)
    public String update(need need){
        //上传的文件
        MultipartFile img = need.getImg();
        String imgStr = FilesUtil.filesUp(img);
        //将文件路径存入对象，存入到数据库
        need.setNeedimg(imgStr);
        needService.update(need);
        return "redirect:/need/page";
    }
    @RequestMapping(value = "/insert",method = RequestMethod.POST)
    public String insert(need need) {
        //上传的文件
        MultipartFile img = need.getImg();
        String imgStr = FilesUtil.filesUp(img);
        //将文件路径存入对象，存入到数据库
        need.setNeedimg(imgStr);
        needService.add(need);
        return "redirect:/need/page";
    }
    //使用异步传递数组对象到controller层会经过特殊处理，做种参数的名称是ids[],需要@RequestParam手动绑定；
    @RequestMapping("/batchDel")
    public String batchDel(@RequestParam("ids[]") Integer[] ids) {
        needService.batchDel(ids);
        return "redirect:/need/page";
    }
    //分页查询
    @RequestMapping("/page")
    private String queryA(need need, HttpServletRequest request, Model m){
        Integer pageNo=request.getParameter("pageNo")==null?1 : Integer.valueOf(request.getParameter("pageNo"));
        PageInfo<need> pageInfo = needService.queryList(pageNo,4,need);
        m.addAttribute("need",pageInfo.getList());
        m.addAttribute("pages",pageInfo.getPages());
        m.addAttribute("pageNo",pageInfo.getPageNum());
        m.addAttribute("pageSize",pageInfo.getPageSize());
        List<statusl> status1s = statuslService.selectStatus();
        m.addAttribute("status",status1s);

        return "./need/need-list.html";
    }
    @RequestMapping("/goaddDuo")
    public String goaddDuo(Model m){
        List<statusl> status1s = statuslService.selectStatus();
        m.addAttribute("status",status1s);
        m.addAttribute("dis",needService.bsselect());
        List<emp> emps = statuslService.selectEmp();
        m.addAttribute("emp",emps);
        return "/need/piliang.html";
    }
    @RequestMapping("/addpiliang")
    @ResponseBody
    public void addpiliang(@RequestBody List<need> needs) {
        needService.addpiliang(needs);
        System.out.println(needs);
    }
}
