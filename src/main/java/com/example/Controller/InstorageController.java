package com.example.Controller;

import com.example.Service.InstorageService;
import com.example.model.Instorage;
import com.example.model.Outstorage;
import com.example.model.ZongHe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/Ins")
public class InstorageController {
    @Autowired
    InstorageService instorageService;
    //查询入库未审批的订单
    @RequestMapping("selectAll")
    public String select(Model model){
        List<Instorage> list =  instorageService.selectAll();
        model.addAttribute("Ins",list);
        return "./ruku/ruku-list.html";
    }
    //根据订单号查询
    @RequestMapping("select1")
    public String select1(Model model,Instorage instorage){
        List<Instorage> list =  instorageService.select1(instorage);
        model.addAttribute("Ins",list);
        return "./ruku/ruku-list.html";
    }
    //下拉框
    @RequestMapping("/goadd")
    public String goadd(Model model ,Instorage instorage){
        List<Instorage> list = instorageService.select1(instorage);
        model.addAttribute("Ins",list);
        return "./ruku/ruku-chuku.html";
    }
    //添加到出库
    @RequestMapping("/add")
    public String add(Model model ,Instorage instorage){
        List<Instorage> list = instorageService.select1(instorage);

        Outstorage outstorage = new Outstorage();
        outstorage.setStatusid(1);
        outstorage.setOrdertime(new Date());
        outstorage.setBz(instorage.getBz());
        outstorage.setEntrepotid(list.get(0).getEntrepotid());
        outstorage.setOrdersId(list.get(0).getOrdersId());
        instorageService.add(outstorage,list.get(0).getOrdersId());

        model.addAttribute("Ins",list);
        return "./ruku/ruku-chuku.html";
    }
    //查询入库已审批的订单
    @RequestMapping("selectAll2")
    public String select2(Model model){
        List<Instorage> list =  instorageService.selectAll2();
        model.addAttribute("Ins",list);
        return "./ruku/ruku-lists.html";
    }
    //商品数量
    @RequestMapping("selectAll3")
    public String select3(Model model){
        List<ZongHe> list1 =  instorageService.selectAll3();
        model.addAttribute("zh",list1);
        return "./ruku/ruku-huizhong.html";
    }
}
