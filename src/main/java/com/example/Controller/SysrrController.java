package com.example.Controller;

import com.example.Service.SysrrService;
import com.example.Service.SysUserRoleService;
import com.example.model.SysRight;
import com.example.model.SysUser;
import com.example.model.commodity;
import com.example.model.sysrr;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("sysrr")
public class SysrrController {

    @Autowired
    SysrrService SysrrService;
    @Autowired
    SysUserRoleService sysUserRoleService;
//    @RequestMapping("/select")
//    public String select(Model model){
////        model.addAttribute("role",SysrrService.selectAllSysRole());
////        model.addAttribute("user",sysUserRoleService.selectUser());
//
//        return "./sysrr/sysrr-lists.html";
//
//    }
//    @RequestMapping("/selectAll")
//    @ResponseBody
//    public Map<String,Object> selectAll(Model model,
//       @RequestParam("page")Integer pageNum,
//       @RequestParam("limit") Integer pageSize){
//        List<SysRight> list = SysrrService.selectAll();
//        PageHelper.startPage(pageNum,pageSize);
//        List<SysRight> lists = SysrrService.selectAll();
//        PageInfo<SysRight> pageInfo = new PageInfo<>(lists);
//        Map<String,Object> map = new HashMap<String,Object>();
//        map.put("code",0);
//        map.put("msg","");
//        map.put("count",list.size());
//        map.put("data",pageInfo.getList());
//
//        return map;
//    }
//     权限查询
    @RequestMapping("selectAll")
    public String selectAll(Model model,
       @RequestParam(defaultValue = "1",value = "pageNum") Integer pageNum){

        PageHelper.startPage(pageNum,7);
        List<SysRight> list = SysrrService.selectAll();
        PageInfo<SysRight> pageInfo = new PageInfo<SysRight>(list);
        model.addAttribute("pageInfo",pageInfo);
        return "./sysrr/sysrr-list.html";
    }

    @RequestMapping("qselectAllSysRightByIf")
    public String qselectAllSysRightByIf(Model model,SysRight SysRight,
      @RequestParam(defaultValue = "1",value = "pageNum") Integer pageNum
    ){
        PageHelper.startPage(pageNum,7);
        List<SysRight> list = SysrrService.qselectAllSysRightByIf(SysRight);
        PageInfo<SysRight> pageInfo = new PageInfo<SysRight>(list);
        model.addAttribute("pageInfo",pageInfo);
//        model.addAttribute("pageInfo", SysrrService.qselectAllSysRightByIf(SysRight));
        return "./sysrr/sysrr-list.html";
    }
    // 修改
    @RequestMapping("strupd")
    public String strupd(Model model, SysRight sysRight){
//      model.addAttribute("sysRight", SysrrService.srupd(sysRight));
        return "./sysrr/sysrr-list.html";
    }
    //  查询路由
    @RequestMapping("selectAllSysRight")
    public String selectAllSysRight(Model model,
       @RequestParam(defaultValue = "1",value = "pageNum") Integer pageNum){

        PageHelper.startPage(pageNum,7);
        List<SysRight> list = SysrrService.selectAllSysRight();
        PageInfo<SysRight> pageInfo = new PageInfo<SysRight>(list);
        model.addAttribute("pageInfo",pageInfo);
//        model.addAttribute("sysRight",SysrrService.selectAllSysRight());
        return "./sysright/sysright-list.html";
    }
//  查询角色
    @RequestMapping("selectAllSysRole")
    public String selectAllSysRole(Model model

    ){
        model.addAttribute("sysrole",SysrrService.selectAllSysRole());
        return "./sysright/sysright-add.html";
    }

    @RequestMapping("srupd")
    public String srupd(Model model,SysRight sysRight){
        SysrrService.srupd(sysRight);
        return "redirect:/sysrr/selectAllSysRight";
    }

    @RequestMapping("selectAllSysRightByID")
    public String selectAllSysRightByID(Model model,Integer right_id){
        model.addAttribute("sysRight",SysrrService.selectAllSysRightByID(right_id));
        return "./sysright/sysright-edit.html";
    }
    @RequestMapping("selectAllSysRightByIDrole")
    public String selectAllSysRightByIDrole(Model model,Integer right_id){
        model.addAttribute("SysRightByIDrole",SysrrService.selectAllSysRightByID(right_id));
        model.addAttribute("role",SysrrService.selectAllSysRole());
        return "./sysrole/sysrole-add.html";
    }

    @RequestMapping("SysRolerightadd")
    public String SysRolerightadd(Model model, sysrr sysrr){
        SysrrService.SysRolerightadd(sysrr);
         return "redirect:/sysrr/selectAllSysRight";
    }

    @RequestMapping("selectAllSysRightByIf")
    public String selectAllSysRightByIf(Model model,SysRight SysRight,
        @RequestParam(defaultValue = "1",value = "pageNum") Integer pageNum){
        PageHelper.startPage(pageNum,7);
        List<SysRight> list = SysrrService.selectAllSysRightByIf(SysRight);
        PageInfo<SysRight> pageInfo = new PageInfo<SysRight>(list);
        model.addAttribute("pageInfo",pageInfo);
//        model.addAttribute("sysRight",SysrrService.selectAllSysRightByIf(SysRight));
         return "./sysright/sysright-list.html";
    }


    @RequestMapping("SysRolerightdel")
    public String SysRolerightdel(Model model,Integer right_id){
       SysrrService.SysRolerightdel(right_id);
        SysrrService.SysRolerightdelrr(right_id);
        return "redirect:/sysrr/selectAllSysRight";
    }

    @RequestMapping("SysRolerightdelrrs")
    public String SysRolerightdelrrs(Model model,Integer right_id,Integer role_id){

        SysrrService.SysRolerightdelrrs(right_id,role_id);
        return "redirect:/sysrr/selectAlluser";
    }

    @RequestMapping("SysRolerightaddx")
    public String SysRolerightaddx(Model model,SysRight SysRight){
        SysrrService.SysRolerightaddx(SysRight);
        return "redirect:/sysrr/selectAllSysRight";
    }

    @RequestMapping("selectAlluser")
    public String selectAlluser(Model model,
       @RequestParam(defaultValue = "1",value = "pageNum") Integer pageNum
    ){
        PageHelper.startPage(pageNum,7);
        List<SysRight> list = SysrrService.selectAll();
        PageInfo<SysRight> pageInfo = new PageInfo<SysRight>(list);
        model.addAttribute("pageInfo",pageInfo);
        model.addAttribute("sysrole",SysrrService.selectAllSysRole());
//        model.addAttribute("sysrr",  SysrrService.selectAll());
        return "./sysuserrole/sysuserrole-list.html";
    }
}
