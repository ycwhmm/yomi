package com.example.Controller;

import com.example.Service.OutstorageServie;
import com.example.model.Outstorage;
import com.example.model.bping;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/out")
public class OutstorageController {
    @Autowired
    OutstorageServie outstorageServie;

    @RequestMapping("/selectById")
    public  String selectById(Model model,Integer outstorageid){
        Outstorage outstorage = outstorageServie.selectById(outstorageid);
        model.addAttribute("out",outstorage);
        return "bping/bping-edits";
    }
    @RequestMapping("/Outstorageadd")
    public  String Outstorageadd(Model model, bping bping){
        outstorageServie.Outstorageadd(bping);
//添加领料的同时删除  领过料的订单
//        outstorageServie.del(bping.getOrdersId());
        return "redirect:/bping/selectAll";
    }
    //    @RequestMapping("/Outstorageadd1")
//    public  String Outstorageadd1(Model model, Outstorage outstorage){
//        outstorageServie.Outstorageadd(outstorage);
//        return "redirect:/outprse/glselect";
//    }
    @RequestMapping("/selectAll2")
    public String select2(Model model){
        List<Outstorage> outstorages = outstorageServie.selectAll2();
        model.addAttribute("out",outstorages);
        return "./bping/bping-add.html";
    }
    //查询已审批出库订单
    @RequestMapping("/selectAll")
    public String select(Model model,@RequestParam(defaultValue = "1",value = "pageNum") Integer pageNum){
        PageHelper.startPage(pageNum,5);
        List<Outstorage> outstorages = outstorageServie.selectAll();
        PageInfo<Outstorage> pageInfo = new PageInfo<Outstorage>(outstorages);
        model.addAttribute("pageInfo",pageInfo);
        return "./chuku/chuku-list.html";
    }
    //查询未审批出库订单
    @RequestMapping("/selectAll1")
    public String select1(Model model){
        List<Outstorage> outstorages = outstorageServie.selectAll1();
        model.addAttribute("out",outstorages);
        return "./chuku/chuku-shenpi.html";
    }
    @RequestMapping("/select1")
    public String select1(Model model,Outstorage outstorage){
        List<Outstorage> outstorages = outstorageServie.select1(outstorage);
        model.addAttribute("out",outstorages);
        return "./chuku/chuku-list.html";
    }
    @RequestMapping("/select2")
    public String select2(Model model,Outstorage outstorage){
        List<Outstorage> outstorages = outstorageServie.select2(outstorage);
        model.addAttribute("out",outstorages);
        return "./chuku/chuku-shenpi.html";
    }
    //修改状态
    @RequestMapping("/up")
    public String up(Model model,Integer id){

        outstorageServie.up(id);

        return "redirect:/out/selectAll1";
    }
}
