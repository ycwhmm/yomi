package com.example.Controller;


import com.example.Service.BsupplierService;
import com.example.model.Bcategory;
import com.example.model.Bsupplier;
import com.example.model.Distributor;
import com.example.model.statusl;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/bsupplier")
@Slf4j
public class BsupplierController {
    @Autowired
    public BsupplierService bsupplierService;

    // 查询所有
//    @RequestMapping("/selectAllBsu")
//    public String selectAllBsupp(Model model,Bsupplier bsupplier) {
//        List<Bsupplier> list = bsupplierService.SelectAll(bsupplier);
//        model.addAttribute("bsupp", list);
//        List<Bcategory> bcategories=bsupplierService.SelectBca();
//        model.addAttribute("bcategory", bcategories);
//        List<status1>   status1s = bsupplierService.Selectstatus1();
//        model.addAttribute("status1s",status1s);
//        return "./Bsupplier/bsupplier-list.html";
//    }
    //去新增页面
    @RequestMapping("/goAdd")
    public String goAdd(Bsupplier bsupplier,Model model){
        List<Bcategory> bcategories=bsupplierService.SelectBca();
        List<statusl>   status1s = bsupplierService.Selectstatus1();
        model.addAttribute("bcategories",bcategories);
        model.addAttribute("status1s",status1s);
        return "./Bsupplier/Bsupplier-add.html";
    }
    //新增
    @RequestMapping("/bsuppAdd")
    public String busppAdd(Model model, Bsupplier bsupplier) {
        bsupplierService.AddBsupp(bsupplier);
        return "redirect:/bsupplier/page";
    }
    // 删除
    @RequestMapping("/bsuppDel")
    public String bsuppDel(Model model, Integer BsupId) {
        bsupplierService.DelBsupp(BsupId);
        return "redirect:/bsupplier/page";
    }
    //批量删除
    @RequestMapping("/batchDel")
    public String batchDel(@RequestParam("ids[]") Integer[] ids) {
        bsupplierService.DelAll(ids);
        return "redirect:/bsupplier/page";
    }
    //修改
    @RequestMapping("/bsuppUpd")
    public String bsuppUpd(Model model, Bsupplier bsupplier) {
        bsupplierService.UpdBsupp(bsupplier);
        return "redirect:/bsupplier/page";
    }

    //根据id查询
    @RequestMapping("/selectById")
    public String selectById(Model model, Integer BsupId) {
        Bsupplier bsupplier = bsupplierService.SelectById(BsupId);
        model.addAttribute("bsupp", bsupplier);
        List<Bcategory> bcategories=bsupplierService.SelectBca();
        model.addAttribute("bcategory", bcategories);
        List<statusl>   status1s = bsupplierService.Selectstatus1();
        model.addAttribute("status1s",status1s);
        return "./Bsupplier/Bsupplier-edit.html";
    }
    @RequestMapping("/page")
    private String queryA(Bsupplier bsupplier, HttpServletRequest request, Model model){
        Integer pageNo=request.getParameter("pageNo")==null?1 : Integer.valueOf(request.getParameter("pageNo"));
        PageInfo<Bsupplier> pageInfo = bsupplierService.queryList(pageNo,3,bsupplier);
        model.addAttribute("bsupp",pageInfo.getList());
        model.addAttribute("pages",pageInfo.getPages());
        model.addAttribute("pageNo",pageInfo.getPageNum());
        model.addAttribute("pageSize",pageInfo.getPageSize());
        List<Bcategory> bcategories=bsupplierService.SelectBca();
        model.addAttribute("bcategory", bcategories);
        List<statusl>   status1s = bsupplierService.Selectstatus1();
        model.addAttribute("status1s",status1s);
        return "./Bsupplier/Bsupplier-list.html";
    }
    @RequestMapping("/addpiliang")
    public void addpiliang(@RequestBody List<Bsupplier> bsuppliers) {
        bsupplierService.addpiliang(bsuppliers);
        System.out.println(bsuppliers);
    }
    @RequestMapping("/goPiliang")
    public String goPiliang(Bsupplier bsupplier,Model model){
        List<Bcategory> bcategories=bsupplierService.SelectBca();
        List<statusl>   status1s = bsupplierService.Selectstatus1();
        model.addAttribute("bcategories",bcategories);
        model.addAttribute("status1s",status1s);
        return "./Bsupplier/Bsupplier-piliang.html";
    }
}
