package com.example.Controller;


import com.example.Service.SupplierService;
import com.example.Service.entrepotService;
import com.example.model.*;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/entrepot")
@Slf4j
public class entrepotController {
    @Autowired
    entrepotService entrepotService;
    @Autowired
    SupplierService supplierService;

 @RequestMapping("/selectAll")
    public  String selectAll(Model model,
     @RequestParam(defaultValue = "1",value = "pageNum") Integer pageNum){
     PageHelper.startPage(pageNum,3);
     List<entrepots> list = entrepotService.selectAll();
     PageInfo<entrepots> pageInfo = new PageInfo<entrepots>(list);
     model.addAttribute("pageInfo",pageInfo);
     return "./entrepot/entrepot-list.html";
 }

    @RequestMapping("/entradd")
    public String entradd(Model m, entrepots entrepot) {
        entrepotService.entradd(entrepot);
        return "redirect:/entrepot/selectAll";
    }
    @RequestMapping("/entrupd")
    public String entrupd(Model m,entrepots entrepot) {
        entrepotService.entrupd(entrepot);
        return "redirect:/entrepot/selectAll";
    }
    @RequestMapping("/entrdel")
    public String entrdel(Model model,Integer entrepotid) {
        entrepotService.entrdel(entrepotid);
        return "redirect:/entrepot/selectAll";
    }
    @RequestMapping("/entrselectById")
    public String selectById(Model model,Integer entrepotid) {
      entrepots entrepot = entrepotService.selectById(entrepotid);
      model.addAttribute("entrepot",entrepot);
        return "./entrepot/entrepot-edit.html";
    }
    @RequestMapping("/batchDel")
    @ResponseBody
    public JsonMessage batchDel(@RequestParam("id[]") Integer[] entrepotid) throws IOException {
        entrepotService.batchDel(entrepotid);
        return new JsonMessage(200,null);
    }


}
