package com.example.util;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration//必须有，才能作为配置
public class FilesConfigurer implements WebMvcConfigurer {
    @Override//增加资源映射路径
    public void addResourceHandlers(ResourceHandlerRegistry registry){
        //匹配资源路径，资源存放位置
        registry.addResourceHandler("/files/**").addResourceLocations("file:D:\\files\\");
    }
}

