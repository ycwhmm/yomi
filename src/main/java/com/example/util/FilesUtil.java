
package com.example.util;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

public class FilesUtil {
    public static String filesUp(MultipartFile file){
        String path=null;//文件路径
        if (!file.isEmpty()) {
            String fileName = file.getOriginalFilename();  // 文件名
            String suffixName = fileName.substring(fileName.lastIndexOf("."));  // 后缀名
            String newfileName = UUID.randomUUID() + suffixName; // 新文件名
            //存放在D盘
            File newFile = new File("D:\\files\\" + newfileName);//新文件
            if (!newFile.getParentFile().exists()) {
                newFile.getParentFile().mkdirs();
            }
            try {
                file.transferTo(newFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
            path = "/files/" + newfileName;
        }
        return path;
    }

}
