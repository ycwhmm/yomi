package com.example.dao;

import com.example.model.Bcategory;
import com.example.model.emp;
import com.example.model.logparticulars;
import com.example.model.statusl;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface logparticularsDao {
     List<logparticulars> selectAll();
     int add(logparticulars logparticulars);
     int wlogupd(String logparticularsid);
     int ylogupd(String logparticularsid);

     Double  selectlogmodeprice(String logparticularsid);
     int batchDel(String[] id);
     int wlogupds(String[] logparticularsid);
     Double  selectlogmodeprices(String[] logparticularsid);
     void adds(@Param("logparticularsid")String[] logparticularsid,@Param("logId")Integer logId);
     int jselecttype();
     int chuanselecttype();
     int cheselecttype();
}
