package com.example.dao;

import com.example.model.SysRight;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface SysRightDao {
    //子查询中根据name查询所拥有角色id，sys_user ,sys_user_role
    //主查询中根据多个角色id,查询对应的角色，sys_right,sys_role_right
    @Select("SELECT r.* from sys_right r,sys_role_right rr\n" +
            "WHERE r.right_id=rr.right_id\n" +
            "and rr.role_id in (select b.role_id from sys_user a,sys_user_role b where a.user_id=b.user_id and a.user_statusid=2 and a.user_name=#{name})")
    List<SysRight> selectByUserName(String name);
@Select("select * from sys_user where user_name=#{name}")
    List<SysRight> selectName(String name);
}
