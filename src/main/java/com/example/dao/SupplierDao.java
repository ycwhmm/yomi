package com.example.dao;



import com.example.model.category;
import com.example.model.dept;
import com.example.model.emp;
import com.example.model.supplier;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface SupplierDao {
    List<supplier> selectAll();
    List<category> selectcategory();
    int add(supplier supplier);
    int del(Integer supId);
    int upd(supplier supplier);

}
