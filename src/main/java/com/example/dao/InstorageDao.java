package com.example.dao;

import com.example.model.Instorage;
import com.example.model.Outstorage;
import com.example.model.ZongHe;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public interface InstorageDao {
    List<Instorage>selectAll();//查询入库未审批的订单

    List<Instorage> select1(Instorage instorage);//根据订单号查询
    int del(Integer id);//删除
    int add(Outstorage outstorage);//添加到出库
    int up(String id);//修改状态
    List<Instorage>selectAll2();//查询入库已审批的订单
    List<ZongHe>selectAll3();//商品数量

}
