package com.example.dao;

import com.example.model.*;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public interface StatuslDao {
    public List<statusl> selectStatus();
    public List<logistics> selectLog();
    public List<Distributor> selectDis();
    public List<Bsupplier> selectBus();
    public List<emp> selectEmp();

}
