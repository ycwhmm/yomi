package com.example.dao;

import com.example.model.*;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public interface SortparticularsDao {
    List<sortparticulars>selectAll();//查询未分拣全部
    int batchDel(Integer[] sparId);
    List<sortparticulars> selectByIf(sortparticulars Sortparticulars);//根据订单查询
    purchaseorders shangpin(String id);//查询分销单的详情
    int add(Instorage instorage);//分拣完成添加到入库
    List<Entrepot> goadd();//仓库下拉框
    int del(Integer sparId);

    List<purchaseorders> shangpinAll ();//商品单全部
    int adds(sortparticulars sortparticulars);//添加分拣
    List<emp>selectemp();//员工下拉框
    int up1(String id);//修改商品表状态
    int up2(Integer id);//修改分拣状态
    purchaseorders shangpinby(String id);//查询商品单
    List<sortparticulars>selectAll2();//分拣完成的全部
}
