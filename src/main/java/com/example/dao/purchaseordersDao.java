package com.example.dao;

import com.example.model.purchaseorders;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public interface purchaseordersDao {
    public List<purchaseorders> selectAll(purchaseorders purchaseorders);
    public List<purchaseorders> selectAll1();
    public int delete(String ordersId);
    public int add(purchaseorders purchaseorders);
    public purchaseorders selectById(String ordersId);
    public int update(purchaseorders purchaseorders);
    public int batchDel(String[] ids);
    public void addpiliang (List<purchaseorders> purchaseorders);
}
