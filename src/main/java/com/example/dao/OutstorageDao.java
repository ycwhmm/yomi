package com.example.dao;

import com.example.model.Instorage;
import com.example.model.Outstorage;
import com.example.model.bping;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public interface OutstorageDao {
    List<Outstorage>selectAll();//查询已审批的订单
    List<Outstorage> select1(Outstorage outstorage);//根据订单号查询已审批订单
    List<Outstorage>selectAll1();//查询未审批的订单
    List<Outstorage> select2(Outstorage outstorage);//根据订单号查询未审批订单
    List<Outstorage>selectAll2();//查询已审批完成的订单
    int up(Integer id);//修改状态
    Outstorage selectById(Integer outstorageid);
    int Outstorageadd(bping bping);
    int del(String ordersId);

}
