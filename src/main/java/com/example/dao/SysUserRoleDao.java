package com.example.dao;

import com.example.model.SysRole;
import com.example.model.SysUser;
import com.example.model.SysUserRole;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface SysUserRoleDao {
    @Select("select * from sys_user_role where user_id = #{user_id}")
    List<SysUserRole> listByUserId(Integer userId);
    @Select("select * from sys_user")
    List<SysUser> selectUser();
    @Insert(" insert into sys_user_role(role_id,user_id)\n" +
            "          values(#{role_id},#{user_id}) ")
    int insertuserrole(SysUserRole sysUserRole);
    @Insert("insert into sys_role(role_name) values(#{role_name})")
    int addrole(SysRole sysRole);

    List<SysUser> selectAlluser();
    int update(SysUser sysUser);
    int batchDel(Integer[] id);
}
