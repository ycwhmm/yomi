package com.example.dao;

import com.example.model.bping;
import com.example.model.statusl;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Miracle yaochengwei on 2020/11/24 8:57
 *      List<commodity> selectAll();
 *     int commadd(commodity commodity);
 *     int commdel(Integer commodityid);
 *     int commupd(commodity commodity);
 *     List<statusl> selectstatusl();
 *     commodity selectById(Integer commodityid);
 */
@Component
public interface BpingDao {
    List<bping> selectAll();
    int bpingadd(bping bping);
    int bpingdel(Integer bpingid);
    int bpingupd(bping bping);
    List<statusl> selectstatusl();
    bping selectById(Integer bpingid);
    public List<bping> selectByIf(bping bping);
    int bpingDel(Integer[] bpingids);//批量删除
    List<bping> selectByIf();
//    List<Outstorage> selectAAll();
}
