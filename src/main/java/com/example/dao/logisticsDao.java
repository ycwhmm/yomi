package com.example.dao;


import com.example.model.commodity;
import com.example.model.entrepots;
import com.example.model.logistics;
import com.example.model.logmode;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface logisticsDao {
    List<logistics> selectAll();
    List<logmode> selectlogmode();
    int logadd(logistics logistics);
    int logdel(Integer logId);
    int logupd(logistics logistics);
    logistics selectById(Integer logId);

    int logmodeadd(logmode logmode);
    int logmodedel(Integer logmodeid);
    int logmodeupd(logmode logmode);
    logmode logmodeselectById(Integer logmodeid);

}
