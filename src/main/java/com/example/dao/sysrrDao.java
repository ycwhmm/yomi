package com.example.dao;

import com.example.model.SysRight;
import com.example.model.SysRole;
import com.example.model.SysUser;
import com.example.model.sysrr;

import java.util.List;

public interface sysrrDao {
    List<SysRight> selectAll();
    int srupd(SysRight sysRight);
// 查询 路由表
    List<SysRight> selectAllSysRight();
    List<SysRole> selectAllSysRole();
    SysRight selectAllSysRightByID(Integer right_id);
    List<SysRight> selectAllSysRightByIf(SysRight SysRight);
    List<SysRight> qselectAllSysRightByIf(SysRight SysRight);
    List<SysRight> userselectAllSysRightByIf(SysRight SysRight);
//    添加 权限
    int SysRolerightadd(sysrr sysrr);
    int SysRolerightdel(Integer right_id);
    int SysRolerightdelrr(Integer right_id);
    int SysRolerightdelrrs(Integer right_id,Integer role_id);
    int SysRolerightaddx(SysRight SysRight);

    int Sysuseradd(SysUser sysUser);
}
