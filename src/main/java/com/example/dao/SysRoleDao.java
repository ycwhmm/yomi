package com.example.dao;

import com.example.model.SysRole;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

@Component
public interface SysRoleDao {
    @Select("select * from sys_role where role_id =#{role_id}")
    SysRole selectById(Integer role_id);
}
