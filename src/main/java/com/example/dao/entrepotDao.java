package com.example.dao;


import com.example.model.commodity;
import com.example.model.emp;
import com.example.model.entrepots;
import com.example.model.statusl;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface entrepotDao {
    List<entrepots> selectAll();
    int entradd(entrepots entrepot);
    int entrdel(Integer entrepotid);
    int entrupd(entrepots entrepot);
    entrepots selectById(Integer entrepotid);
    int batchDel(Integer[] entrepotid);//批量删除

}
