package com.example.dao;

import com.example.model.logistics;
import com.example.model.outprse;
import com.example.model.prse;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Miracle yaochengwei on 2020/12/1 10:47
 */
@Component
public interface OutprseDao {
    List<outprse> selectAll();
    List<prse> selectprse();
    List<prse> selectprse1();
    List<logistics> selectlogistics();
    int add(outprse outprse);
    int del(Integer outprseid);
    int upd(outprse outprse);
    outprse selectById(Integer outprseid);//根据id查询
    public List<outprse> selectByIf(outprse outprse);//模糊查询
    int outprseDel(Integer[] outprseids);//批量删除

    public List<prse> glselectByIf(prse prse);
    int gladd(prse prse);
    int gldel(Integer prseid);
    int glupd(prse prse);
    prse glselectById(Integer prseid);
    prse glselectById1(Integer prseid);
    int prseDel(Integer[] prseids);//批量删除

    List<prse> glselect();
}
