package com.example.dao;

import com.example.model.Bsupplier;
import com.example.model.need;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public interface needDao {
    public List<need> selectAll(need need);
    public int delete(int needId);
    public need selectById(int needId);
    public int add(need need);
    public int update(need need);
    public int batchDel(Integer[] ids);
    public List<Bsupplier> bsselect();
    public void addpiliang (List<need> needs);
}
