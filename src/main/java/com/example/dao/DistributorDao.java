package com.example.dao;

import com.example.model.Distributor;
import com.example.model.statusl;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface DistributorDao {
    //查询所有(模糊查询)
    public List<Distributor> SelectAll(Distributor distributor);
    //添加
    public int addDis(Distributor distributor);
    //删除
    public int delDis(Integer distributorid);
    //修改
    public int updDis(Distributor distributor);
    //查询审核状态表
    public List<statusl> Selectstatus1();
    //根据id查询
    public Distributor SelectByIdDis(Integer distributorid);
    //批量删除
    public int DelAll(Integer[] distributorids);
    //批量添加
    public void addpiliang(List<Distributor> distributors);

}
