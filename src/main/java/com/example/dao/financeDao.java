package com.example.dao;

import com.example.model.finance;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public interface financeDao {
    List<finance> selectAll();
    int cgupd(Double num);
    Double cgselect();
    int djupd(Double num);
    Double djselect();

    int zongupd(Double num);
    Double zongselect();
    int wlupd(Double num);
    Double wlselect();
}
