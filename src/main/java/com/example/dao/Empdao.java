package com.example.dao;



import com.example.model.dept;
import com.example.model.emp;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface Empdao {
  List<emp> selectAll();
  List<dept> selectdept();
  int add(emp emp);
  int del(Integer emp_id);
  int upd(emp emp);
  emp selectById(Integer emp_id);
  int batchDel(Integer[] emp_id);
  public List<emp> selectByIf(emp emp);


  public List<dept> glselectByIf(dept dept);
  int gladd(dept dept);
  int gldel(Integer dept_id);
  int glupd(dept dept);
  dept glselectById(Integer dept_id);
  int cbatchDel(Integer[] dept_id);//批量删除

  List<emp> cgselectAll();
  void addpiliang (List<emp> emps);
//  addpiliang(emp emp);
}
