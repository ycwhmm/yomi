package com.example.dao;


import com.example.model.SysUser;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

@Component
public interface SysUserDao {
    @Select("select * from sys_user where user_name = #{user_name}")
    SysUser selectByName(String user_name);
}
