package com.example.dao;

import com.example.model.Samorder;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public interface SamorderDao {
    public List<Samorder> selectAll(Samorder samorder);
    public int delete(int orderId);
    public int add(Samorder samorder);
    public Samorder selectById(int orderId);
    public int update(Samorder samorder);
    public int batchDel(Integer[] ids);
}
