package com.example.dao;



import com.example.model.*;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface commodityDao {
    List<commodity> selectAll();
    int commadd(commodity commodity);
    int commdel(Integer commodityid);
    int commupd(commodity commodity);
    List<statusl> selectstatus1();
    commodity selectById(Integer commodityid);
     int commbatchDel(Integer[] commodityids);//批量删除
}
