package com.example.Service;

import com.example.dao.logparticularsDao;
import com.example.model.logparticulars;
import com.example.model.need;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class logparticularsImp implements logparticularsService{
    @Autowired
    logparticularsDao logparticularsDao;
    @Override
    public List<logparticulars> selectAll() {
        return logparticularsDao.selectAll();
    }

    @Override
    public int add(logparticulars logparticulars) {
        return logparticularsDao.add(logparticulars);
    }

    @Override
    public int wlogupd(String logparticularsid) {
        return logparticularsDao.wlogupd(logparticularsid);
    }

    @Override
    public int ylogupd(String logparticularsid) {
        return logparticularsDao.ylogupd(logparticularsid);
    }

    @Override
    public Double selectlogmodeprice(String logparticularsid) {
        return logparticularsDao.selectlogmodeprice(logparticularsid);
    }

    @Override
    public int batchDel(String[] id) {
        return logparticularsDao.batchDel(id);
    }

    @Override
    public int wlogupds(String[] logparticularsid) {
        return logparticularsDao.wlogupds(logparticularsid);
    }

    @Override
    public Double selectlogmodeprices(String[] logparticularsid) {
        return logparticularsDao.selectlogmodeprices(logparticularsid);
    }

    @Override
    public void adds(String[] logparticularsid, Integer logId) {
        logparticularsDao.adds(logparticularsid,logId);
    }

    @Override
    public int jselecttype() {
        return logparticularsDao.jselecttype();
    }

    @Override
    public int chuanselecttype() {
        return logparticularsDao.chuanselecttype();
    }

    @Override
    public int cheselecttype() {
        return logparticularsDao.cheselecttype();
    }


}
