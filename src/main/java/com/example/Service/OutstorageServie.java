package com.example.Service;

import com.example.model.Instorage;
import com.example.model.Outstorage;
import com.example.model.bping;

import java.util.List;

public interface OutstorageServie {
    List<Outstorage> selectAll();
    List<Outstorage> select1(Outstorage outstorage);
    List<Outstorage>selectAll1();
    List<Outstorage>selectAll2();
    List<Outstorage> select2(Outstorage outstorage);
    int up(Integer id);
    Outstorage selectById(Integer outstorageid);
    int Outstorageadd(bping bping);
    int del(String ordersId);
}
