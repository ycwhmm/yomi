package com.example.Service;

import com.example.model.SysRole;

public interface SysRoleService {
    SysRole selectById(Integer role_id);

}
