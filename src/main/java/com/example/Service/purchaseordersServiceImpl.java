package com.example.Service;

import com.example.dao.purchaseordersDao;
import com.example.model.purchaseorders;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public class purchaseordersServiceImpl implements purchaseordersService {
    @Autowired
    purchaseordersDao purchaseordersDao;

    @Override
    public List<purchaseorders> selectAll1() {
        return purchaseordersDao.selectAll1();
    }

    @Override
    public int delete(String ordersId) {
        return purchaseordersDao.delete(ordersId);
    }

    @Override
    public int add(purchaseorders purchaseorders) {
        return purchaseordersDao.add(purchaseorders);
    }

    @Override
    public purchaseorders selectById(String ordersId) {
        return purchaseordersDao.selectById(ordersId);
    }

    @Override
    public int update(purchaseorders purchaseorders) {
        return purchaseordersDao.update(purchaseorders);
    }

    @Override
    public int batchDel(String[] ids) {
        return purchaseordersDao.batchDel(ids);
    }

    @Override
    public List<purchaseorders> queryList(purchaseorders purchaseorders) {
        //使用pagehelper实现分页
//        PageHelper.startPage(pageNum,pageSize);//pagenum 当前第几页,pagesize 每页显示多少
//        //获得后台的值集合，用模糊查询的方法
//        List<purchaseorders> list= purchaseordersDao.selectAll(purchaseorders);
//        PageInfo<purchaseorders> pi=new PageInfo<>(list,pageSize);
        return  purchaseordersDao.selectAll(purchaseorders);
    }

    @Override
    public void addpiliang(List<purchaseorders> purchaseorders) {
        purchaseordersDao.addpiliang(purchaseorders);
    }
}
