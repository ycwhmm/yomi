package com.example.Service;

import com.example.model.Bsupplier;
import com.example.model.need;
import com.example.model.purchaseorders;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface needService {
    public List<need> selectAll(need need);
    public int delete(int needId);
    public need selectById(int needId);
    public int add(need need);
    public int update(need need);
    public int batchDel(Integer[] ids);
    //分页
    PageInfo<need> queryList(Integer pageNum, Integer pageSize, need need);
    public List<Bsupplier> bsselect();
    public void addpiliang (List<need> needs);

}
