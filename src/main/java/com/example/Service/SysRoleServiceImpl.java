package com.example.Service;

import com.example.dao.SysRoleDao;
import com.example.model.SysRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SysRoleServiceImpl implements SysRoleService {
    @Autowired
    SysRoleDao sysRoleDao;
    @Override
    public SysRole selectById(Integer role_id) {
        return sysRoleDao.selectById(role_id);
    }
}
