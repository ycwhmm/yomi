package com.example.Service;

import com.example.dao.OutprseDao;
import com.example.model.logistics;
import com.example.model.outprse;
import com.example.model.prse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Miracle yaochengwei on 2020/12/1 10:51
 */
@Component
public class outprseServiceImpl implements outprseService {
    @Autowired
    OutprseDao outprseDao;
    @Override
    public List<outprse> selectAll() {
        return outprseDao.selectAll();
    }

    @Override
    public List<prse> selectprse() {
        return outprseDao.selectprse();
    }

    @Override
    public List<prse> selectprse1() {
        return outprseDao.selectprse1();
    }

    @Override
    public int add(outprse outprse) {
        return outprseDao.add(outprse);
    }

    @Override
    public int del(Integer outprseid) {
        return outprseDao.del(outprseid);
    }

    @Override
    public int upd(outprse outprse) {
        return outprseDao.upd(outprse);
    }

    @Override
    public outprse selectById(Integer outprseid) {
        return outprseDao.selectById(outprseid);
    }

    @Override
    public List<outprse> selectByIf(outprse outprse) {
        return outprseDao.selectByIf(outprse);
    }

    @Override
    public int outprseDel(Integer[] outprseids) {
        return outprseDao.outprseDel(outprseids);
    }

    @Override
    public List<prse> glselectByIf(prse prse) {
        return outprseDao.glselectByIf(prse);
    }

    @Override
    public int gladd(prse prse) {
        return outprseDao.gladd(prse);
    }

    @Override
    public int gldel(Integer prseid) {
        return outprseDao.gldel(prseid);
    }

    @Override
    public int glupd(prse prse) {
        return outprseDao.glupd(prse);
    }

    @Override
    public prse glselectById(Integer prseid) {
        return outprseDao.glselectById(prseid);
    }

    @Override
    public int prseDel(Integer[] prseids) {
        return outprseDao.prseDel(prseids);
    }

    @Override
    public prse glselectById1(Integer prseid) {
        return outprseDao.glselectById1(prseid);
    }

    @Override
    public List<logistics> selectlogistics() {
        return outprseDao.selectlogistics();
    }

    @Override
    public List<prse> glselect() {
        return outprseDao.glselect();
    }
}
