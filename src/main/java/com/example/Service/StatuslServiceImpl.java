package com.example.Service;

import com.example.dao.StatuslDao;
import com.example.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public class StatuslServiceImpl implements StatuslService {
    @Autowired
    StatuslDao statuslDao;
    @Override
    public List<statusl> selectStatus() {
        return statuslDao.selectStatus();
    }

    @Override
    public List<logistics> selectLog() {
        return statuslDao.selectLog();
    }

    @Override
    public List<Distributor> selectDis() {
        return statuslDao.selectDis();
    }

    @Override
    public List<Bsupplier> selectBus() {
        return statuslDao.selectBus();
    }

    @Override
    public List<emp> selectEmp() {
        return statuslDao.selectEmp();
    }

}
