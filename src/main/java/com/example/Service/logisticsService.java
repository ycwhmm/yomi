package com.example.Service;




import com.example.model.commodity;
import com.example.model.logistics;
import com.example.model.logmode;

import java.util.List;

public interface logisticsService {
    List<logistics> selectAll();
    List<logmode> selectlogmode();
    int logadd(logistics logistics);
    int logdel(Integer logId);
    int logupd(logistics logistics);
    logistics selectById(Integer logId);


    int logmodeadd(logmode logmode);
    int logmodedel(Integer logmodeid);
    int logmodeupd(logmode logmode);
    logmode logmodeselectById(Integer logmodeid);


}
