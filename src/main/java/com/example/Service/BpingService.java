package com.example.Service;

import com.example.model.bping;
import com.example.model.statusl;

import java.util.List;

/**
 *  List<emp> selectAll();
 * Created by Miracle yaochengwei on 2020/11/24 8:56
 */
public interface BpingService {
    List<bping> selectAll();
    int bpingadd(bping bping);
    int bpingdel(Integer bpingid);
    int bpingupd(bping bping);
    List<statusl> selectstatusl();
    bping selectById(Integer bpingid);
    public List<bping> selectByIf(bping bping);
    int bpingDel(Integer[] bpingids);//批量删除
    //    List<Outstorage> selectAAll();
    List<bping> selectByIf();
}
