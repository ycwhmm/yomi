package com.example.Service;



import com.example.dao.commodityDao;
import com.example.model.commodity;
import com.example.model.dept;
import com.example.model.emp;
import com.example.model.statusl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class commodityServiceIm implements commodityService {
@Autowired
    commodityDao commodityDao;


    @Override
    public List<commodity> selectAll() {
        return commodityDao.selectAll();
    }

    @Override
    public int commadd(commodity commodity) {
        return commodityDao.commadd(commodity);
    }

    @Override
    public int commdel(Integer commodityid) {
        return commodityDao.commdel(commodityid);
    }

    @Override
    public int commupd(commodity commodity) {
        return commodityDao.commupd(commodity);
    }

    @Override
    public List<statusl> selectstatus1() {
        return commodityDao.selectstatus1();
    }

    @Override
    public commodity selectById(Integer commodityid) {
        return commodityDao.selectById(commodityid);
    }

    @Override
    public int commbatchDel(Integer[] commodityids) {
        return commodityDao.commbatchDel(commodityids);
    }
}

