package com.example.Service;

import com.example.dao.SysUserDao;
import com.example.model.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SysUserServiceImpl implements SysUserService {
    @Autowired
    SysUserDao sysUserDao;
    @Override
    public SysUser selectByName(String user_name) {
        return sysUserDao.selectByName(user_name);
    }
}
