package com.example.Service;

import com.example.model.purchaseorders;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public interface purchaseordersService {
    public List<purchaseorders> selectAll1();
    public int delete(String ordersId);
    public int add(purchaseorders purchaseorders);
    public purchaseorders selectById(String ordersId);
    public int update(purchaseorders purchaseorders);
    public int batchDel(String[] ids);
    //分页
    List<purchaseorders> queryList(purchaseorders purchaseorders);
    public void addpiliang (List<purchaseorders> purchaseorders);
}
