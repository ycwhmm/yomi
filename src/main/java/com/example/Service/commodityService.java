package com.example.Service;




import com.example.model.commodity;
import com.example.model.dept;
import com.example.model.emp;

import com.example.model.statusl;

import java.util.List;

public interface commodityService {
    List<commodity> selectAll();
    int commadd(commodity commodity);
    int commdel(Integer commodityid);
    int commupd(commodity commodity);
    List<statusl> selectstatus1();
    commodity selectById(Integer commodityid);
    int commbatchDel(Integer[] commodityids);
}
