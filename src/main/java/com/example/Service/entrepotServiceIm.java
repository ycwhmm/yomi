package com.example.Service;



import com.example.dao.entrepotDao;
import com.example.model.commodity;
import com.example.model.entrepots;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class entrepotServiceIm implements entrepotService {
@Autowired
entrepotDao entrepotDao;


    @Override
    public List<entrepots> selectAll() {
        return entrepotDao.selectAll();
    }

    @Override
    public int entradd(entrepots entrepot) {
        return entrepotDao.entradd(entrepot);
    }

    @Override
    public int entrdel(Integer entrepotid) {
        return entrepotDao.entrdel(entrepotid);
    }

    @Override
    public int entrupd(entrepots entrepot) {
        return entrepotDao.entrupd(entrepot);
    }

    @Override
    public entrepots selectById(Integer entrepotid) {
        return entrepotDao.selectById(entrepotid);
    }

    @Override
    public int batchDel(Integer[] entrepotid) {
        return entrepotDao.batchDel(entrepotid);
    }

}

