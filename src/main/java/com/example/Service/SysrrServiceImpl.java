package com.example.Service;

import com.example.dao.sysrrDao;
import com.example.model.SysRight;
import com.example.model.SysRole;
import com.example.model.SysUser;
import com.example.model.sysrr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SysrrServiceImpl implements SysrrService {
    @Autowired
    sysrrDao sysrrDao;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public List<SysRight> selectAll() {
        return sysrrDao.selectAll();
    }

    @Override
    public int srupd(SysRight sysRight) {
        return sysrrDao.srupd(sysRight);
    }

    @Override
    public List<SysRight> selectAllSysRight() {
        return sysrrDao.selectAllSysRight();
    }

    @Override
    public List<SysRole> selectAllSysRole() {
        return sysrrDao.selectAllSysRole();
    }

    @Override
    public SysRight selectAllSysRightByID(Integer right_id) {
        return sysrrDao.selectAllSysRightByID(right_id);
    }

    @Override
    public int SysRolerightadd(sysrr sysrr) {
        return sysrrDao.SysRolerightadd(sysrr);
    }

    @Override
    public List<SysRight> selectAllSysRightByIf(SysRight SysRight) {
        return sysrrDao.selectAllSysRightByIf(SysRight);
    }

    @Override
    public List<SysRight> qselectAllSysRightByIf(SysRight SysRight) {
        return sysrrDao.qselectAllSysRightByIf(SysRight);
    }

    @Override
    public int SysRolerightdel(Integer right_id) {
        return sysrrDao.SysRolerightdel(right_id);
    }

    @Override
    public int SysRolerightdelrr(Integer right_id) {
        return sysrrDao.SysRolerightdelrr(right_id);
    }

    @Override
    public int SysRolerightdelrrs(Integer right_id, Integer role_id) {
        return sysrrDao.SysRolerightdelrrs(right_id,role_id);
    }

    @Override
    public int SysRolerightaddx(SysRight SysRight) {
        return sysrrDao.SysRolerightaddx(SysRight);
    }

    @Override
    public List<SysRight> userselectAllSysRightByIf(SysRight SysRight) {
        return sysrrDao.userselectAllSysRightByIf(SysRight);
    }

    @Override
    public SysUser Sysuseradd(SysUser sysUser) {

        if (sysUser.getUser_name() != null && sysUser.getUser_psd() != null) {
            sysUser.setUser_psd(passwordEncoder.encode(sysUser.getUser_psd()));
            sysrrDao.Sysuseradd(sysUser);
        } else
            sysUser = null;
        return sysUser;

         }
    }

