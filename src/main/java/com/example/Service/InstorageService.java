package com.example.Service;

import com.example.model.Instorage;
import com.example.model.Outstorage;
import com.example.model.ZongHe;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface InstorageService {
    List<Instorage> selectAll();

    List<Instorage> select1(Instorage instorage);
    int add(Outstorage outstorage, String id);
    List<Instorage>selectAll2();
    List<ZongHe>selectAll3();
}
