package com.example.Service;




import com.example.model.dept;
import com.example.model.emp;

import java.util.List;

public interface empService {
    List<emp> selectAll();
    List<dept> selectdept();
    int add(emp emp);
    int del(Integer emp_id);
    int upd(emp emp);
    emp selectById(Integer emp_id);
    public List<emp> selectByIf(emp emp);
    int batchDel(Integer[] emp_id);

    public List<dept> glselectByIf(dept dept);
    int gladd(dept dept);
    int gldel(Integer dept_id);
    int glupd(dept dept);
    dept glselectById(Integer dept_id);
    int cbatchDel(Integer[] dept_id);//批量删除

    List<emp> cgselectAll();

    void addpiliang (List<emp> emps);
}
