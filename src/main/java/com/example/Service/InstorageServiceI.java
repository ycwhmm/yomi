package com.example.Service;

import com.example.dao.InstorageDao;
import com.example.model.Instorage;
import com.example.model.Outstorage;
import com.example.model.ZongHe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public class InstorageServiceI implements InstorageService {
    @Autowired
    InstorageDao instorageDao;
    @Override
    public List<Instorage> selectAll() {
        return instorageDao.selectAll();
    }

    @Override
    public List<Instorage> select1(Instorage instorage) {
        return instorageDao.select1(instorage);
    }

    @Override
    public int add(Outstorage outstorage,String id) {
        instorageDao.up(id);
        return instorageDao.add(outstorage);
    }

    @Override
    public List<Instorage> selectAll2() {
        return instorageDao.selectAll2();
    }

    @Override
    public List<ZongHe> selectAll3() {
        return instorageDao.selectAll3();
    }
}
