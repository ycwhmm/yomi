package com.example.Service;

import com.example.model.SysRole;
import com.example.model.SysUser;
import com.example.model.SysUserRole;

import java.util.List;

public interface SysUserRoleService {
    List<SysUserRole> listByUserId(Integer userId);
    List<SysUser> selectUser();
    int insertuserrole(SysUserRole sysUserRole);
    int addrole(SysRole sysRole);

    List<SysUser> selectAlluser();
    int update(SysUser sysUser);
    int batchDel(Integer[] id);
}
