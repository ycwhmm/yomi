package com.example.Service;

import java.util.List;
import com.example.model.logparticulars;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;


public interface logparticularsService {
    List<logparticulars> selectAll();
    int add(logparticulars logparticulars);
    int wlogupd(String logparticularsid);
    int ylogupd(String logparticularsid);
    Double  selectlogmodeprice(String logparticularsid);
    int batchDel(String[] id);
    int wlogupds(String[] logparticularsid);
    Double  selectlogmodeprices(String[] logparticularsid);
    void adds(@Param("logparticularsid")String[] logparticularsid, @Param("logId")Integer logId);
    int jselecttype();
    int chuanselecttype();
    int cheselecttype();
}
