package com.example.Service;

import com.example.dao.SortparticularsDao;
import com.example.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class SortparticularsService implements SortparticularsServiceI {
    @Autowired
    SortparticularsDao sortparticularsDao;
    @Override
    public List<sortparticulars> selectAll() {
        return sortparticularsDao.selectAll();
    }

    @Override
    public int batchDel(Integer[] sparId) {
        return sortparticularsDao.batchDel(sparId);
    }

    @Override
    public List<sortparticulars> selectByIf(sortparticulars sortparticulars) {
        return sortparticularsDao.selectByIf(sortparticulars);
    }

    @Override
    public purchaseorders shangpin(String id) {
        return sortparticularsDao.shangpin(id);
    }

    @Override
    public int add(Instorage instorage, Integer sparId) {
         sortparticularsDao.up2(sparId);
        return sortparticularsDao.add(instorage);
    }

    @Override
    public List<Entrepot> goadd() {

        return sortparticularsDao.goadd();
    }

    @Override
    public List<purchaseorders> shangpinAll() {
        return sortparticularsDao.shangpinAll();
    }

    @Override
    public int adds(sortparticulars sortparticulars) {
        sortparticularsDao.up1(sortparticulars.getOrdersId());
        return sortparticularsDao.adds(sortparticulars);
    }

    @Override
    public List<emp> selectemp() {
        return sortparticularsDao.selectemp();
    }

    @Override
    public purchaseorders shangpinby(String id) {
        return sortparticularsDao.shangpinby(id);
    }

    @Override
    public List<sortparticulars> selectAll2() {
        return sortparticularsDao.selectAll2();
    }
}
