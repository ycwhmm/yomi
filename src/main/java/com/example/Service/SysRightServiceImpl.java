package com.example.Service;

import com.example.dao.SysRightDao;
import com.example.model.SysRight;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SysRightServiceImpl implements SysRightService {
    @Autowired
    private SysRightDao sysRightDao;
    @Override
    public List<SysRight> selectByUserName(String name) {
        //数据库中的列表，包含一级菜单，二级菜单
        List<SysRight> sysRights = sysRightDao.selectByUserName(name);
        //存放一级菜单
        List<SysRight> oneRights = new ArrayList<SysRight>();
        //存放二级菜单
        List<SysRight> twoRights = new ArrayList<SysRight>();
        //将用户拥有的菜单分类
        for (SysRight sysRight:sysRights
        ) {
            if(sysRight.getLevel()==1){
                oneRights.add(sysRight);
            }else{
                twoRights.add(sysRight);
            }
        }
        //一级菜单中添加二级菜单
        for (SysRight oneRight:oneRights
        ) {
            //设置一级菜单中的二级菜单
            oneRight.setTwoRights(new ArrayList<SysRight>());
            //二级菜单中找一级菜单的子类
            for (SysRight twoRight:twoRights
            ) {
                //如果二级菜单的父id和一级菜单的id对应，添加
                if(oneRight.getRight_id()==twoRight.getParent_id()){
                    oneRight.getTwoRights().add(twoRight);
                }
            }
        }
        return oneRights;
    }

    @Override
    public List<SysRight> selectName(String name) {
        return sysRightDao.selectName(name);
    }
}
