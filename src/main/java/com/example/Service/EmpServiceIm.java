package com.example.Service;



import com.example.dao.Empdao;
import com.example.model.dept;
import com.example.model.emp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class EmpServiceIm implements empService {
@Autowired
Empdao empdao;

    @Override
    public List<emp> selectAll() {
        return empdao.selectAll();
    }

    @Override
    public List<dept> selectdept() {
        return empdao.selectdept();
    }

    @Override
    public int add(emp emp) {
        return empdao.add(emp);
    }

    @Override
    public int del(Integer emp_id) {
        return empdao.del(emp_id);
    }

    @Override
    public int upd(emp emp) {
        return empdao.upd(emp);
    }

    @Override
    public emp selectById(Integer emp_id) {
        return empdao.selectById(emp_id);
    }

    @Override
    public List<emp> selectByIf(emp emp) {
        return empdao.selectByIf(emp);
    }

    @Override
    public int batchDel(Integer[] emp_id) {
        return empdao.batchDel(emp_id);
    }

    @Override
    public List<dept> glselectByIf(dept dept) {
        return empdao.glselectByIf(dept);
    }

    @Override
    public int gladd(dept dept) {
        return empdao.gladd(dept);
    }

    @Override
    public int gldel(Integer dept_id) {
        return empdao.gldel(dept_id);
    }

    @Override
    public int glupd(dept dept) {
        return empdao.glupd(dept);
    }

    @Override
    public dept glselectById(Integer dept_id) {
        return empdao.glselectById(dept_id);
    }

    @Override
    public int cbatchDel(Integer[] dept_id) {
        return empdao.cbatchDel(dept_id);
    }

    @Override
    public List<emp> cgselectAll() {
        return empdao.cgselectAll();
    }

    @Override
    public void addpiliang(List<emp> emps) {
         empdao.addpiliang(emps);
    }

}

