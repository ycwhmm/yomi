package com.example.Service;



import com.example.dao.SupplierDao;
import com.example.model.supplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SupplierServiceServiceIm implements SupplierService {
@Autowired
SupplierDao  supplierDao;

    @Override
    public List<supplier> selectAll() {
        return supplierDao.selectAll();
    }
}

