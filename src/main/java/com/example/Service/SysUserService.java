package com.example.Service;

import com.example.model.SysUser;


public interface SysUserService {
    SysUser selectByName(String user_name);
}
