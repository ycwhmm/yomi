package com.example.Service;

import com.example.model.Bcategory;
import com.example.model.Bsupplier;
import com.example.model.statusl;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public interface BsupplierService {
    //查询所有
    public List<Bsupplier> SelectAll(Bsupplier bsupplier);
    //新增
    public int AddBsupp(Bsupplier bsupplier);
    //删除
    public int DelBsupp(Integer BsupId);
    //修改
    public int UpdBsupp(Bsupplier bsupplier);
    //查询审核状态表
    public List<statusl> Selectstatus1();
    //查询包装类型表
    public List<Bcategory> SelectBca();
    //根据id查询
    public Bsupplier SelectById(Integer BsupId);
    //批量删除
    public int DelAll(Integer[] BsupIds);
    //分页
    PageInfo<Bsupplier> queryList(Integer pageNum, Integer pageSize, Bsupplier bsupplier);
    //批量添加
    public void addpiliang(List<Bsupplier> bsuppliers);
}
