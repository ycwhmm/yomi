package com.example.Service;



import com.example.dao.logisticsDao;
import com.example.model.logistics;
import com.example.model.logmode;
import com.example.model.sortparticulars;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class logisticsServiceIm implements logisticsService {
@Autowired
    logisticsDao logisticsDao;


    @Override
    public List<logistics> selectAll() {
        return logisticsDao.selectAll();
    }

    @Override
    public List<logmode> selectlogmode() {
        return logisticsDao.selectlogmode();
    }

    @Override
    public int logadd(logistics logistics) {
        return logisticsDao.logadd(logistics);
    }

    @Override
    public int logdel(Integer logId) {
        return logisticsDao.logdel(logId);
    }

    @Override
    public int logupd(logistics logistics) {
        return logisticsDao.logupd(logistics);
    }

    @Override
    public logistics selectById(Integer logId) {
        return logisticsDao.selectById(logId);
    }

    @Override
    public int logmodeadd(logmode logmode) {
        return logisticsDao.logmodeadd(logmode);
    }

    @Override
    public int logmodedel(Integer logmodeid) {
        return logisticsDao.logmodedel(logmodeid);
    }

    @Override
    public int logmodeupd(logmode logmode) {
        return logisticsDao.logmodeupd(logmode);
    }

    @Override
    public logmode logmodeselectById(Integer logmodeid) {
        return logisticsDao.logmodeselectById(logmodeid);
    }
}

