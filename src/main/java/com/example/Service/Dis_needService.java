package com.example.Service;

import com.example.model.*;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface Dis_needService {
    //查询所有
    public List<Dis_need> SelectAll(Dis_need dis_need);
    //新增
    public int AddDis_need(Dis_need dis_need);
    //删除
    public int DelDis_need(String dis_need_id);
    //修改
    public int UpdDis_need(Dis_need dis_need);
    //根据id查询
    public Dis_need SelectById(String dis_need_id);
    //批量删除
    public int DelAll(String[] dis_need_ids);
    public int updateStatus(String dis_need_id);
    public int updateStatu(String[] dis_need_ids);
    //查询审核状态表
    public List<statusl> Selectstatus1();
    //查询包装详情表
    public List<Bcategory> SelectBca();
    //查询商品包装类别表
    public List<Bsupplier> SelectBsu();
    //查询物流公司表
    public List<logistics> SelectLog();
    //查询物流方式表
    public List<logmode> SelectLogMode();
    //查询分销商表
    public List<Distributor> SelectDis();
    //查询商品详情表
    public List<commodity> SelectCom();
    //查询供应商表
    public List<supplier> SelectSupp();
    //分页
    PageInfo<Dis_need> queryList(Integer pageNum, Integer pageSize, Dis_need dis_need);
    //查询样板图片
    public  List<need> SelectNeed();
    public List<Dis_need> SelectAlls();

    //批量添加
    public int addpiliang(List<Dis_need> dis_needs);
}
