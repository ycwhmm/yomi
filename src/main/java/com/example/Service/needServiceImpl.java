package com.example.Service;

import com.example.dao.needDao;
import com.example.model.Bsupplier;
import com.example.model.need;
import com.example.model.purchaseorders;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public class needServiceImpl implements needService {
    @Autowired
    needDao needDao;
    @Override
    public List<need> selectAll(need need) {
        return needDao.selectAll(need);
    }

    @Override
    public int delete(int needId) {
        return needDao.delete(needId);
    }

    @Override
    public need selectById(int needId) {
        return needDao.selectById(needId);
    }

    @Override
    public int add(need need) {
        return needDao.add(need);
    }

    @Override
    public int update(need need) {
        return needDao.update(need);
    }

    @Override
    public int batchDel(Integer[] ids) {
        return needDao.batchDel(ids);
    }

    @Override
    public PageInfo<need> queryList(Integer pageNum, Integer pageSize, need need) {
        //使用pagehelper实现分页
        PageHelper.startPage(pageNum,pageSize);//pagenum 当前第几页,pagesize 每页显示多少
        //获得后台的值集合，用模糊查询的方法
        List<need> list= needDao.selectAll(need);
        PageInfo<need> pi=new PageInfo<>(list,pageSize);
        return pi;
    }

    @Override
    public List<Bsupplier> bsselect() {
        return needDao.bsselect();
    }

    @Override
    public void addpiliang(List<need> needs) {
         needDao.addpiliang(needs);
    }
}
