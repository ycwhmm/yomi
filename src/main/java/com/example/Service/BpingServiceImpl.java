package com.example.Service;

import com.example.dao.BpingDao;
import com.example.model.bping;
import com.example.model.statusl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Miracle yaochengwei on 2020/11/24 8:56
 *  List<emp> selectAll();
 */
//@Service//实现类需要一个service注解
@Component
public class BpingServiceImpl implements BpingService {
    @Autowired
    BpingDao bpingDao;

    @Override
    public List<bping> selectAll() {
        return bpingDao.selectAll();
    }

    @Override
    public int bpingadd(bping bping) {
        return bpingDao.bpingadd(bping);
    }

    @Override
    public int bpingdel(Integer bpingid) {
        return bpingDao.bpingdel(bpingid);
    }

    @Override
    public int bpingupd(bping bping) {
        return bpingDao.bpingupd(bping);
    }

    @Override
    public List<statusl> selectstatusl() {
        return bpingDao.selectstatusl();
    }

    @Override
    public bping selectById(Integer bpingid) {
        return bpingDao.selectById(bpingid);
    }

    @Override
    public List<bping> selectByIf(bping bping) {
        return bpingDao.selectByIf(bping);
    }

    @Override
    public int bpingDel(Integer[] bpingids) {
        return bpingDao.bpingDel(bpingids);
    }

//    @Override
//    public List<Outstorage> selectAAll() {
//        return bpingDao.selectAAll();
//    }

    @Override
    public List<bping> selectByIf() {
        return bpingDao.selectByIf();
    }


}
