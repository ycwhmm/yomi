package com.example.Service;

import com.example.model.SysRight;

import java.util.List;

//权限查询
public interface SysRightService {
    List<SysRight> selectByUserName(String name);
    List<SysRight> selectName(String name);
}
