package com.example.Service;

import com.example.dao.SysUserRoleDao;
import com.example.model.SysRole;
import com.example.model.SysUser;
import com.example.model.SysUserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SysUserRoleServiceImpl implements SysUserRoleService {
    @Autowired
    SysUserRoleDao sysUserRoleDao;
    @Override
    public List<SysUserRole> listByUserId(Integer userId) {
        return sysUserRoleDao.listByUserId(userId);
    }

    @Override
    public List<SysUser> selectUser() {
        return sysUserRoleDao.selectUser();
    }

    @Override
    public int insertuserrole(SysUserRole sysUserRole) {
        return sysUserRoleDao.insertuserrole(sysUserRole);
    }

    @Override
    public int addrole(SysRole sysRole) {
        return sysUserRoleDao.addrole(sysRole);
    }

    @Override
    public List<SysUser> selectAlluser() {
        return sysUserRoleDao.selectAlluser();
    }

    @Override
    public int update(SysUser sysUser) {
        return sysUserRoleDao.update(sysUser);
    }

    @Override
    public int batchDel(Integer[] id) {
        return sysUserRoleDao.batchDel(id);
    }
}
