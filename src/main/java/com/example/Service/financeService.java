package com.example.Service;

import com.example.model.Bcategory;
import com.example.model.Bsupplier;
import com.example.model.finance;
import com.example.model.statusl;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface financeService {
    List<finance> selectAll();
    int cgupd(Double num);
    Double cgselect();
    int djupd(Double num);
    Double djselect();

    int zongselect(Double num);
    Double zongselect();
    int wlupd(Double num);
    Double wlselect();
}
