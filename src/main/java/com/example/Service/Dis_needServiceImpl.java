package com.example.Service;

import com.example.dao.Dis_needDao;
import com.example.model.*;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public class Dis_needServiceImpl implements Dis_needService {
    @Autowired
    Dis_needDao dis_needDao;
    @Override
    public List<Dis_need> SelectAll(Dis_need dis_need) {
        return dis_needDao.SelectAll(dis_need);
    }

    @Override
    public int AddDis_need(Dis_need dis_need) {
        return dis_needDao.AddDis_need(dis_need);
    }

    @Override
    public int DelDis_need(String dis_need_id) {
        return dis_needDao.DelDis_need(dis_need_id);
    }

    @Override
    public int UpdDis_need(Dis_need dis_need) {
        return dis_needDao.UpdDis_need(dis_need);
    }

    @Override
    public Dis_need SelectById(String dis_need_id) {
        return dis_needDao.SelectById(dis_need_id);
    }

    @Override
    public int DelAll(String[] dis_need_ids) {
        return dis_needDao.DelAll(dis_need_ids);
    }

    @Override
    public int updateStatus(String dis_need_id) {
        return dis_needDao.updateStatus(dis_need_id);
    }

    @Override
    public int updateStatu(String[] dis_need_ids) {
        return dis_needDao.updateStatu(dis_need_ids);
    }

    @Override
    public List<statusl> Selectstatus1() {
        return dis_needDao.Selectstatus1();
    }

    @Override
    public List<Bcategory> SelectBca() {
        return dis_needDao.SelectBca();
    }

    @Override
    public List<Bsupplier> SelectBsu() {
        return dis_needDao.SelectBsu();
    }

    @Override
    public List<logistics> SelectLog() {
        return dis_needDao.SelectLog();
    }

    @Override
    public List<logmode> SelectLogMode() {
        return dis_needDao.SelectLogMode();
    }

    @Override
    public List<Distributor> SelectDis() {
        return dis_needDao.SelectDis();
    }

    @Override
    public List<commodity> SelectCom() {
        return dis_needDao.SelectCom();
    }

    @Override
    public List<supplier> SelectSupp() {
        return dis_needDao.SelectSupp();
    }

    @Override
    public PageInfo<Dis_need> queryList(Integer pageNum, Integer pageSize, Dis_need dis_need) {
        //使用pagehelper实现分页
        PageHelper.startPage(pageNum,pageSize);//pagenum 当前第几页,pagesize 每页显示多少
        //获得后台的值集合，用模糊查询的方法
        List<Dis_need> list= dis_needDao.SelectAll(dis_need);
        PageInfo<Dis_need> pi=new PageInfo<>(list,pageSize);
        return pi;
    }

    @Override
    public List<need> SelectNeed() {
        return dis_needDao.SelectNeed();
    }

    @Override
    public List<Dis_need> SelectAlls() {
        return dis_needDao.SelectAlls();
    }

    @Override
    public int addpiliang(List<Dis_need> dis_needs) {
        return dis_needDao.addpiliang(dis_needs);
    }
}
