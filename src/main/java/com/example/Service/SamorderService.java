package com.example.Service;

import com.example.model.Samorder;
import com.example.model.purchaseorders;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public interface SamorderService {
    public List<Samorder> selectAll(Samorder samorder);
    public int delete(int orderId);
    public int add(Samorder samorder);
    public Samorder selectById(int orderId);
    public int update(Samorder samorder);
    public int batchDel(Integer[] ids);
    PageInfo<Samorder> queryList(Integer pageNum, Integer pageSize, Samorder samorder);
}
