package com.example.Service;

import com.example.model.SysRight;
import com.example.model.SysRole;
import com.example.model.SysUser;
import com.example.model.sysrr;

import java.util.List;

public interface SysrrService {
    List<SysRight> selectAll();
    int srupd(SysRight sysRight);
  // 查询 路由
    List<SysRight> selectAllSysRight();
//查询 角色
    List<SysRole> selectAllSysRole();
    SysRight selectAllSysRightByID(Integer right_id);
    int SysRolerightadd(sysrr sysrr);
    List<SysRight> selectAllSysRightByIf(SysRight SysRight);
    List<SysRight> qselectAllSysRightByIf(SysRight SysRight);
    int SysRolerightdel(Integer right_id);
    int SysRolerightdelrr(Integer right_id);
    int SysRolerightdelrrs(Integer right_id,Integer role_id);
    int SysRolerightaddx(SysRight SysRight);
    //用户 模糊查询
    List<SysRight> userselectAllSysRightByIf(SysRight SysRight);
    SysUser Sysuseradd(SysUser sysUser);
}
