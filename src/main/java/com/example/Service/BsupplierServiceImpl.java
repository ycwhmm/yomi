package com.example.Service;

import com.example.dao.BsupplierDao;
import com.example.model.Bcategory;
import com.example.model.Bsupplier;
import com.example.model.statusl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public class BsupplierServiceImpl implements BsupplierService {
    @Autowired
    public BsupplierDao bsupplierDao;
    @Override
    public List<Bsupplier> SelectAll(Bsupplier bsupplier) {
        return bsupplierDao.SelectAll(bsupplier);
    }

    @Override
    public int AddBsupp(Bsupplier bsupplier) {
        return bsupplierDao.AddBsupp(bsupplier);
    }

    @Override
    public int DelBsupp(Integer BsupId) {
        return bsupplierDao.DelBsupp(BsupId);
    }

    @Override
    public int UpdBsupp(Bsupplier bsupplier) {
        return bsupplierDao.UpdBsupp(bsupplier);
    }

    @Override
    public List<statusl> Selectstatus1() {
        return bsupplierDao.Selectstatus1();
    }

    @Override
    public List<Bcategory> SelectBca() {
        return bsupplierDao.SelectBca();
    }

    @Override
    public Bsupplier SelectById(Integer BsupId) {
        return bsupplierDao.SelectById(BsupId);
    }

    @Override
    public int DelAll(Integer[] BsupIds) {
        return bsupplierDao.DelAll(BsupIds);
    }

    @Override
    public PageInfo<Bsupplier> queryList(Integer pageNum, Integer pageSize, Bsupplier bsupplier) {
        //使用pagehelper实现分页
        PageHelper.startPage(pageNum,pageSize);//pagenum 当前第几页,pagesize 每页显示多少
        //获得后台的值集合，用模糊查询的方法
        List<Bsupplier> list= bsupplierDao.SelectAll(bsupplier);
        PageInfo<Bsupplier> pi=new PageInfo<>(list,pageSize);
        return pi;
    }

    @Override
    public void addpiliang(List<Bsupplier> bsuppliers) {
        bsupplierDao.addpiliang(bsuppliers);
    }
}
