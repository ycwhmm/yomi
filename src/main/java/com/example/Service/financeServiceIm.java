package com.example.Service;


import com.example.dao.financeDao;
import com.example.model.entrepots;
import com.example.model.finance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class financeServiceIm implements financeService {
@Autowired
    financeDao financeDao  ;


    @Override
    public List<finance> selectAll() {
        return financeDao.selectAll();
    }

    @Override
    public int cgupd(Double num) {
        return financeDao.cgupd(num);
    }

    @Override
    public Double cgselect() {
        Double num = financeDao.cgselect();
        return num;
    }

    @Override
    public int djupd(Double num) {

        return financeDao.djupd(num);
    }

    @Override
    public Double djselect() {
        Double num = financeDao.djselect();
        return num;
    }

    @Override
    public int zongselect(Double num) {
        return financeDao.zongupd(num);
    }

    @Override
    public Double zongselect() {
        Double num = financeDao.zongselect();
        return num;
    }

    @Override
    public int wlupd(Double num) {
        return financeDao.wlupd(num);
    }

    @Override
    public Double wlselect() {
        return financeDao.wlselect();
    }
}

