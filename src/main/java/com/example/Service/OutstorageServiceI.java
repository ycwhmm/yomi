package com.example.Service;

import com.example.dao.OutstorageDao;
import com.example.model.Instorage;
import com.example.model.Outstorage;
import com.example.model.bping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public class OutstorageServiceI implements OutstorageServie {
    @Autowired
    OutstorageDao outstorageDao;
    @Override
    public List<Outstorage> selectAll() {
        return outstorageDao.selectAll();
    }

    @Override
    public List<Outstorage> select1(Outstorage outstorage ) {
        return outstorageDao.select1(outstorage);
    }

    @Override
    public List<Outstorage> selectAll1() {
        return outstorageDao.selectAll1();
    }

    @Override
    public List<Outstorage> selectAll2()  {
        return outstorageDao.selectAll2();
    }

    @Override
    public List<Outstorage> select2(Outstorage outstorage) {
        return outstorageDao.select2(outstorage);
    }

    @Override
    public int up(Integer id) {
        return outstorageDao.up(id);
    }
    @Override
    public Outstorage selectById(Integer outstorageid) {
        return outstorageDao.selectById(outstorageid);
    }

    @Override
    public int Outstorageadd(bping bping) {
        return outstorageDao.Outstorageadd(bping);
    }

    @Override
    public int del(String ordersId) {
        return outstorageDao.del(ordersId);
    }


}
