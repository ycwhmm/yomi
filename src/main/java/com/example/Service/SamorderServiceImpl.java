package com.example.Service;

import com.example.dao.SamorderDao;
import com.example.model.Samorder;
import com.example.model.purchaseorders;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public class SamorderServiceImpl implements SamorderService {
    @Autowired
    SamorderDao samorderDao;
    @Override
    public List<Samorder> selectAll(Samorder samorder) {
        return samorderDao.selectAll(samorder);
    }

    @Override
    public int delete(int orderId) {
        return samorderDao.delete(orderId);
    }

    @Override
    public int add(Samorder samorder) {
        return samorderDao.add(samorder);
    }

    @Override
    public Samorder selectById(int orderId) {
        return samorderDao.selectById(orderId);
    }

    @Override
    public int update(Samorder samorder) {
        return samorderDao.update(samorder);
    }

    @Override
    public int batchDel(Integer[] ids) {
        return samorderDao.batchDel(ids);
    }

    @Override
    public PageInfo<Samorder> queryList(Integer pageNum, Integer pageSize, Samorder samorder) {
        //使用pagehelper实现分页
        PageHelper.startPage(pageNum,pageSize);//pagenum 当前第几页,pagesize 每页显示多少
        //获得后台的值集合，用模糊查询的方法
        List<Samorder> list= samorderDao.selectAll(samorder);
        PageInfo<Samorder> pi=new PageInfo<>(list,pageSize);
        // PageInfo<Teacher> pageInfo=new PageInfo<>(list,pageSize);
        return pi;
    }
}
