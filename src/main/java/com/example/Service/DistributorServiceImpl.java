package com.example.Service;

import com.example.dao.DistributorDao;
import com.example.model.Distributor;
import com.example.model.statusl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public class DistributorServiceImpl implements DistributorService {
    @Autowired
    public DistributorDao distributorDao;
    @Override
    public List<Distributor> SelectAll(Distributor distributor) {
        return distributorDao.SelectAll(distributor);
    }

    @Override
    public int addDis(Distributor distributor) {
        return distributorDao.addDis(distributor);
    }

    @Override
    public int delDis(Integer distributorid) {
        return distributorDao.delDis(distributorid);
    }

    @Override
    public int updDis(Distributor distributor) {
        return distributorDao.updDis(distributor);
    }

    @Override
    public List<statusl> Selectstatus1() {
        return distributorDao.Selectstatus1();
    }

    @Override
    public Distributor SelectByIdDis(Integer distributorid) {
        return distributorDao.SelectByIdDis(distributorid);
    }

    @Override
    public int DelAll(Integer[] distributorids) {
        return distributorDao.DelAll(distributorids);
    }

    @Override
    public PageInfo<Distributor> queryList(Integer pageNum, Integer pageSize, Distributor distributor) {
        //使用pagehelper实现分页
        PageHelper.startPage(pageNum,pageSize);//pagenum 当前第几页,pagesize 每页显示多少
        //获得后台的值集合，用模糊查询的方法
        List<Distributor> list= distributorDao.SelectAll(distributor);
        PageInfo<Distributor> pi=new PageInfo<>(list,pageSize);
        return pi;
    }

    @Override
    public void addpiliang(List<Distributor> distributors) {
        distributorDao.addpiliang(distributors);
    }
}
