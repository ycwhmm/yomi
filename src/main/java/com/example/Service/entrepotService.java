package com.example.Service;




import com.example.model.commodity;
import com.example.model.entrepots;


import java.util.List;

public interface entrepotService {
    List<entrepots> selectAll();
    int entradd(entrepots entrepot);
    int entrdel(Integer entrepotid);
    int entrupd(entrepots entrepot);
    entrepots selectById(Integer entrepotid);
    int batchDel(Integer[] entrepotid);//批量删除

}
