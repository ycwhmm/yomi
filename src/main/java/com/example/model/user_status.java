package com.example.model;

import lombok.Data;

@Data
public class user_status {
    private Integer user_statusid;
    private String user_statusname;
}
