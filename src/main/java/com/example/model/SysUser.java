package com.example.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysUser {
    private int user_id;
    private String user_name;
    private String user_psd;
    private int user_statusid;
    private user_status user_status;
}
