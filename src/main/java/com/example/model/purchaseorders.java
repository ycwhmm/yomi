package com.example.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.Date;


@lombok.Data
@AllArgsConstructor
@NoArgsConstructor
public class purchaseorders {
    private String ordersId;
    private Integer statusid;//审核状态
    private Date ordertime;//采购单据日期
    private String ordertimeJs;
    private Integer commodityid;//商品id
    @JsonProperty
    private Integer Samnumber;//采购商品数量
    @JsonProperty
    private Integer BsupId;//商品包装供应商id
    private Integer satypenumber;//采购商品包装数量
    private String orloc;//当前地址
    private String logparticularsid;
    private String arrloc;//到达地址
    private Integer logId;//物流公司id
    private Double advance;
    private statusl statusl;
    private commodity commodity;
    private logistics logistics;
    private Integer emp_id;
    private emp emp;
    private dept dept;
    private Distributor distributor;
    private Bsupplier bsupplier;

    private logparticulars logparticulars;
    private logstatus logstatus;

}
