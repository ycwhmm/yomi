package com.example.model;

import lombok.Data;

@Data
public class logstatus {
    private Integer logstatusid;
    private String logstatusname;
}
