package com.example.model;

import lombok.Data;

@Data
public class sortparticulars {
  private Integer  sparId;
    private Integer  sortstatusid ;
    private Integer  emp_id;
    private String  ordersId ;
    private emp  emp ;
    private dept  dept ;
    private sortstatus sortstatus;
    private purchaseorders purchaseorders;
}
