package com.example.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.sql.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class need {
    private Integer needId;//主键ID
    private String needName;//需求项目名
    private String needimg;//样板包装图片
    private MultipartFile img;
    private Date needtime;//发单日期
    private String needtimeJs;
    private Integer saadminid;//负责人(设计)
    @JsonProperty
    private Integer BsupId;//分销商id
    private Integer statusid;//审核状态
    private emp emp;
//    private Distributor distributor;
    private Bsupplier Bsupplier;
    private statusl statusl;
}
