package com.example.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@AllArgsConstructor
public class Entrepot {
    private Integer entrepotid;
    private String entrepotname;
    private String entrepotloc;
    private Integer statusid;
}
