package com.example.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Miracle yaochengwei on 2020/12/16 9:45
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class shouhuo {
    private Integer  shid;
    private String  shouhuo;
}
