package com.example.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class commodity {
    private Integer  commodityid;
    private String  commodityname;
    private MultipartFile img;
    private String commodityimgstr;
    private Integer supId;
    private String  coprice ;
    private Integer statusid;
    private statusl statusl;
    private supplier supplier;
    private category category;
}
