package com.example.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Dis_need {
    private String dis_need_id;
    @JsonProperty("distributorid")
    private Integer distributorid;
    @JsonProperty("supId")
    private Integer supId;
    @JsonProperty("commodityid")
    private Integer commodityid;
    @JsonProperty("Bcategoryid")
    private Integer Bcategoryid;
    @JsonProperty("logparticularsid")
    private String logparticularsid;
    @JsonProperty("BsupId")
    private Integer BsupId;
    @JsonProperty("logId")
    private Integer logId;
    @JsonProperty("logmodeid")
    private Integer logmodeid;
    @JsonProperty("statusid")
    private Integer statusid;
    @JsonProperty("advance")
    private Double advance;
    @JsonProperty("needId")
    private Integer needId;
    @JsonProperty("Samnumber")
    private Integer Samnumber;
    @JsonProperty("satypenumber")
    private Integer satypenumber;
    @JsonProperty("birth")
    private Date birth;

    private Distributor distributor;
    private supplier supplier;
    private commodity commodity;
    private Bcategory bcategory;
    private Bsupplier bsupplier;
    private logistics logistics;
    private logmode logmode;
    private statusl statusl;
    private need need;




}
