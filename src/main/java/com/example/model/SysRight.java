package com.example.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysRight {
    private Integer right_id;
    private String right_name;
    private String right_url;
    private int level;
    private int parent_id;
    private Integer statusid;
    private statusl statusl;

    private sysrr sysrr;
    private SysRight SysRight;
    private SysUserRole SysUserRole;
    private SysRole SysRole;
    private SysUser SysUser;

    private List<SysRight> twoRights;//子菜单

}
