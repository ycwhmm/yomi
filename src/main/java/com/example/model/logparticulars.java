package com.example.model;

import lombok.Data;

import java.util.List;

@Data
public class logparticulars {
     private String logparticularsid;
     private  int logId;
    private  Integer logstatusid;
    private  Integer logdantypeid;

    private String[] logparticularsids;
    private Integer[] logIds;
    // 物流 状态
    private logstatus logstatus;
    // 物流 公司
    private logistics logistics;
    // 物流 运输类型
    private logmode logmode;
    // 物流 单类型
    private logdantype logdantype;
//
    private List<Dis_need> Dis_need;
}
