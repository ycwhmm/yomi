package com.example.model;

import lombok.Data;

@Data
public class logmode {
    private Integer logmodeid;
    private String  logmodename;
    private Double  logmodeprice;

}
