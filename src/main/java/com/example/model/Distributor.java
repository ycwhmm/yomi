package com.example.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Distributor {
    private Integer distributorid;
    private String distributorname;
    private String distrlegal;
    private Integer distrcapital;
    @JsonProperty("statusid")
    private Integer statusid;
    private String distloc;
    private statusl statusl;

}
