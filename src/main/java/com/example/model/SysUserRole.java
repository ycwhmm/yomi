package com.example.model;

import lombok.Data;

@Data

public class SysUserRole {
    private int user_id;
    private int role_id;
}
