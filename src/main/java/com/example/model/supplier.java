package com.example.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class supplier {
    private Integer  supId;
    private String  supName;
    private Integer categoryid;
    private Integer statusid;
    private String  suploc ;
    private category category;
    private statusl statusl;
}
