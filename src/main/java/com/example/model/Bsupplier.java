package com.example.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Bsupplier {
    private Integer BsupId;
    @JsonProperty("BsupName")
    private String BsupName;
    @JsonProperty("Bcategoryid")
    private Integer Bcategoryid;
    @JsonProperty("Bsprice")
    private Integer Bsprice;
    @JsonProperty("Bsuloc")
    private String Bsuloc;
    @JsonProperty("statusid")
    private Integer statusid;
    private Bcategory Bcategory;
    private statusl statusl;
}
