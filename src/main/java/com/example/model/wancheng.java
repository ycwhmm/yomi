package com.example.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Miracle yaochengwei on 2020/12/10 14:00
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class wancheng {
    private Integer wcstatusid;
    private String wcstatus;
}
