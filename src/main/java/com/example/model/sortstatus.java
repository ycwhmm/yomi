package com.example.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data

public class sortstatus {
    private Integer sortstatusid;
    private String sortstatusname;

}
