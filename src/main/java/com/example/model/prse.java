package com.example.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by Miracle yaochengwei on 2020/12/1 10:42
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class prse {
    private Integer prseid;

    private Date ordertime1;
    private outprse outprse;
    private Date outprsetime;
    //    private Date ordertime2;
    private Integer statusid;
    private com.example.model.statusl statusl;
    private String ordersId;

    private com.example.model.need need;
    private com.example.model.logistics logistics;
    private Integer  wcstatusid;
    private wancheng wancheng;
    private purchaseorders purchaseorders;
    private com.example.model.logmode logmode;
    private Entrepot entrepot;
    private Integer entrepotid;
    private Outstorage outstorage;

    private com.example.model.commodity commodity;
    private Dis_need dis_need;

    private Integer needId;
    private Integer logId;

    private Integer logmodeid;
    private Integer outstorageid;
    private  Integer commodityid;
    private String dis_need_id;
    private Integer  shid;
    private shouhuo shouhuo;
    private bping bping;

}
