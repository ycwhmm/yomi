package com.example.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class JsonMessage {
    private int status;//状态码，200代表成功
    private Object object;//携带值的对象

}
