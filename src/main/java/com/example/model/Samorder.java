package com.example.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Samorder {
    private Integer orderId;//主键ID
    private Integer statusid;//审核状态
    private Integer needId;//样板包装信息id
    private Integer distributorid;//分销商id
    private double Bsprice;//样板包装单价
    private Integer Bsnumber;//分销商需求数量
    private Integer saadminid;//负责人（包装订单）
    private Date begintime;//生产日期
    private String begintimeJs;
    private Date finishtime;//交付日期
    private String finishtimeJs;
    private statusl statusl;
    public need need;
    private Distributor distributor;
    private emp emp;
}
