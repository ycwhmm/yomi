package com.example.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Outstorage {
    private Integer outstorageid;
    private Integer statusid;
    private Date ordertime;
    private String  ordertime1;

    private Integer entrepotid;
    private Entrepot entrepot;
    private String ordersId;
    private statusl statusl;
    private String bz;

    private logmode logmode;
    private need need;
    private logistics logistics;
    private wancheng wancheng;
    private commodity commodity;
    private Dis_need dis_need;
    private Integer needId;
    private Integer logId;
    private Integer  wcstatusid;
    private Integer logmodeid;
    private  Integer commodityid;
    private String dis_need_id;
    private purchaseorders purchaseorders;
}
