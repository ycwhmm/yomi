package com.example.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by Miracle yaochengwei on 2020/12/1 10:42
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class outprse {
    private String ordertime2;
    private Integer outprseid;
    private Integer prseid;
    private prse prse;
    /**
     * 仓库商品id
     */
    private Integer entrepotid;

    private Date bpringtime;
    private Date outprsetime;
    private String outprsetime1;
    private Integer statusid;
    private com.example.model.statusl statusl;
    private String ordersId;

    private com.example.model.need need;
    private com.example.model.logistics logistics;
    private wancheng wancheng;
    private com.example.model.logmode logmode;
    private Entrepot entrepot;
    private Outstorage outstorage;

    private com.example.model.commodity commodity;
    private Dis_need dis_need;

    private Integer needId;
    private Integer logId;
    private Integer  wcstatusid;
    private Integer logmodeid;
    private Integer outstorageid;
    private  Integer commodityid;
    private String dis_need_id;
    private purchaseorders purchaseorders;


}
