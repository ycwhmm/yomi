package com.example.model;

import lombok.Data;

@Data
public class emp {
  private Integer emp_id;
  private String  emp_name;
  private String  emp_sex;
  private String  emp_phone;
  private Integer dept_id;
  private dept dept;
}
