package com.example.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity//启用spring security
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    MyUserDetailsService userDetailsService;

    //设定用户和角色
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService)
                .passwordEncoder(new PasswordEncoder() {
                    @Override
                    public String encode(CharSequence rawPassword) {
                        return new BCryptPasswordEncoder().encode(rawPassword);
                    }

                    @Override
                    public boolean matches(CharSequence rawPassword, String encodedPassword) {
                        return new BCryptPasswordEncoder().matches(rawPassword,encodedPassword);
                    }
                });

    }

//    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // formLogin()使用form自定义表单作为登录页面
        http
                .authorizeRequests()
                .antMatchers("/useradminlogin","/deng/deng.html","/useradminregister","/deng/deng-register.html").permitAll()
                .anyRequest().authenticated()
                .and()
                .headers().frameOptions().disable()
                .and()
                .formLogin()
                //指定登录页的路径
                .loginPage("/useradminlogin")
                //指定自定义form表单请求的路径
                .loginProcessingUrl("/useradminlogin")
                .failureUrl("/deng/deng-error.html")
                .defaultSuccessUrl("/useradminlogin")
                //必须允许所有用户访问我们的登录页（例如未验证的用户，否则验证流程就会进入死循环）
                //这个formLogin().permitAll()方法允许所有用户基于表单登录访问/login这个page。
                .permitAll();
        http .csrf().disable();
        http.logout()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/deng/deng.html");
    }
    @Override
    public void configure(WebSecurity web) throws Exception {
        //解决静态资源被拦截的问题
        web.ignoring().antMatchers("/aaa/**");
    }



    @Bean//注入passwordEncoder
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

}
